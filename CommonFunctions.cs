using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Forms;
using UEI;

namespace BusinessObject
{
    public class CommonFunctions
    {
        public string GetSynthesizer(string _data)
        {
            if (_data == null)
                return null;
            if (_data.Contains("."))
                return "n/a";
            if (_data.Length != 8 && _data.Length != 16)
                return "n/a";
            return Synthesizer.getCode(_data);
        }

        public string FormatBinaryData(string _data)
        {
            string value = _data;

            if (value == null)
                return value;

            if (value.Contains("."))
                return value;           // frequency data, not binary

            if (value.Length > 8)
            {
                for (int bt = 8; bt < value.Length; bt += 9)
                    value = value.Insert(bt, " ");
            }
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string StripBinaryFormatting(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                while (value.Contains(" "))
                    value = value.Remove(value.IndexOf(" "), 1);
            }
            return value;
        }

        /// <summary>
        /// Verifies that the value is binary and 
        /// length is in multiples of 8 bits
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ValidateBinaryData(string value)
        {
            foreach (char b in value)
                if (!(b == '1') && !(b == '0'))
                    return null;
            if ((value.Length % 8) > 0)
                return null;
            return value;
        }

        /// <summary>
        /// frequency data must be in decimal 
        /// formatted as dd.dd
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ValidateFrequencyData(string value)
        {
            if (!char.IsDigit(value[0]) || !char.IsDigit(value[1]))
                return null;
            if (!char.IsDigit(value[3]) || !char.IsDigit(value[4]))
                return null;
            if (value.Length != 5)
                value = null; 
            return value;
        }

        public static DialogResult WarningDlg(string Msg, string Hdr)
        {
            return MessageBox.Show(Msg, Hdr, MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning);
        }

        /// <summary>
        /// null string handle id added
        /// also take care of embedded "'" character
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SingleQuote(string str)
        {
            if (str != null)
            {
                str = str.Replace("'", "''");
                return "'" + str + "'";
            }
            else
            {
                return "NULL";
            }
        }

        public static string SingleQuoteNoNull(string str)
        {
            if (str != null)
            {
                str = str.Replace("'", "''");
                return "'" + str + "'";
            }
            else
            {
                return "''";
            }
        }

        /// <summary>
        /// DateTime format 2005-12-08T14:01:44
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string SingleQuote(DateTime time)
        {
            return "'" + time.ToString(Log.DATETIME_FORMAT) + "'";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNumeric(string str)
        {
            double result;
            if (double.TryParse(str, System.Globalization.NumberStyles.Any,
                System.Globalization.NumberFormatInfo.InvariantInfo, 
                out result ) == true)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public static string InvertBinaryString(string data)
        {
            string NotData = "";

            if (!string.IsNullOrEmpty(data))
            {
                foreach (char bit in data)
                    NotData += (bit == '1') ? '0' : '1';
            }

            return NotData;
        }
        /// <summary>
        /// Convert frequency data to a decimal number
        /// </summary>
        /// <param name="data">the frequency</param>
        /// <returns>a decimal</returns>
        public uint FrequencyToDecimal(string data)
        {
            double d = double.Parse(data);
            d = (d * 15.0 / 11.0) * 8.0 - 136.0;
            int i = (int)Math.Round(d);

            if (i < 0)
                throw new Exception("The frequency value is"
                    +" converted to a negative number");

            return (uint)i;
        }
        /// <summary>
        /// convert a decimal number to a binary string
        /// </summary>
        /// <param name="dec"></param>
        /// <returns></returns>
        public string DecimalToBinary(uint dec)
        {
            string bin = "";

            while (dec > 0)
            {
                bin = dec % 2 + bin;
                dec /= 2;
            }

            while (bin.Length < 8)
                bin = "0" + bin;

            if ((bin.Length > 8) && (bin.Length <16))
                while (bin.Length < 16)
                    bin = "0" + bin;

            return bin;
        }

        #region Alternate Codes

        private static List<int> execListPrefixSelector = new List<int>(new int[] {
            000,
            002,
            018,
            031,
            039,
            101,
            106,
            115,
            126,
            135,
            144,
            147,
            191,
            200,
            202,
            231,
            232,
            239,
            276,
            277,
            282,
            286,
            353,
            355,
            382,
            383,
            386,
            388,
            420,
            421,
            426,
            427,
            435,
            451,
            452,
            456,
            480,
            482 });

        public static List<int> GetExecListWithPrefixSelector()
        {
            return execListPrefixSelector;
        }

        public static List<List<int>> MakeExecListWithAlternateEquivalent()
        {
            List<List<int>> result =new List<List<int>>();

            result.Add(new List<int>(new int[] {
                90, 282, 182, 382
            }));
            result.Add(new List<int>(new int[] {
                232, 115, 388
            }));
            result.Add(new List<int>(new int[] {
                202, 39, 222
            }));
            result.Add(new List<int>(new int[] {
                201, 31, 205
            }));
            result.Add(new List<int>(new int[] {
                175, 276
            }));
            result.Add(new List<int>(new int[] {
                28, 156
            }));
            result.Add(new List<int>(new int[] {
                52, 152
            }));
            result.Add(new List<int>(new int[] {
                88, 388
            }));
            result.Add(new List<int>(new int[] {
                95, 126
            }));
            result.Add(new List<int>(new int[] {
                226, 126
            }));
            result.Add(new List<int>(new int[] {
                92, 283
            }));
            result.Add(new List<int>(new int[] {
                93, 283
            }));
            result.Add(new List<int>(new int[] {
                411, 421
            }));
            result.Add(new List<int>(new int[] {
                28, 248, 428, 456
            }));
            result.Add(new List<int>(new int[] {
                371, 481
            }));

            return result;
        }

        #endregion
    }
}
