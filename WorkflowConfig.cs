using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;

namespace BusinessObject
{
    class LockStatusService : LockStatus.LockStatusService
    {
        public LockStatusService()
        {
            this.Url = WorkflowConfig.GetItem("LockStatusURL");
            this.AllowAutoRedirect = true;
            this.Discover();
        }
    }


    public static class WorkflowConfig
    {
        const string _CONFIGFILE = "Workflow.config";
        const string _QUERY = "//configuration/Item/add";
        const string _KEY = "ItemName";
        const string _VALUE = "Value";

        static WorkflowConfig()
        {
        }

        private static void InitForWebService(out XmlNodeList nodeList, out DateTime timeStamp)
        {
            XmlDocument doc = new XmlDocument();
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;

            if (!baseDir.EndsWith(Path.DirectorySeparatorChar.ToString()))
                baseDir += Path.DirectorySeparatorChar.ToString();

            string configFile = baseDir + _CONFIGFILE;

            using (FileStream stream =
                new FileStream(configFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                doc.Load(stream);

                IDictionary<string, string> list = new Dictionary<string, string>();
                nodeList = doc.SelectNodes(_QUERY);

                timeStamp = File.GetCreationTime(configFile);
            }
        }

        public static string GetItem(string key)
        {
            XmlNodeList nodeList;
            DateTime timeStamp;

            InitForWebService(out nodeList, out timeStamp);

            string tmp = "";
            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes[_KEY].Value == key)
                {
                    tmp = node.Attributes[_VALUE].Value;
                    break;
                }
            }
            if (string.IsNullOrEmpty(tmp))
                throw new Exception(string.Format("Workflow.config does not contain an entry for {0}", key));
            return tmp;
        }

        public static DateTime GetTimeStamp()
        {
            XmlNodeList nodeList;
            DateTime timeStamp;

            InitForWebService(out nodeList, out timeStamp);

            return timeStamp;
        }
    }
}
