using System;
using BusinessObject.LockStatus;

namespace BusinessObject
{
    public class LockStatusFunctions
    {
        private readonly LockStatusService _lockService = new LockStatusService();

        public int IssueNewTN(string domain, string user)
        {
            LockStatusCounters(domain, user);
            
            int issuedTn = PreviewNextTnNumber();
            IncrementTnNumber(domain, user);
            UnlockStatusCounters(domain, user);

            _lockService.LockTn(issuedTn, domain, user);

            return issuedTn;
        }

        public int IssueNewRestrictedTN(string domain, string user)
        {
            LockStatusCounters(domain, user);

            int issuedTn = PreviewNextRestrictedTnNumber();
            IncrementRestrictedTnNumber(domain, user);
            UnlockStatusCounters(domain, user);

            _lockService.LockTn(issuedTn, domain, user);

            return issuedTn;
        }

        public bool LockExistingTN(int tnNumber, string domain, string user)
        {
            const int RESTRICTED_TN_FLOOR = 90000;
            const int RESTRICTED_TN_CEILING = 100000;
            bool result = false;

            int lastIssued = PreviewNextTnNumber();
            if (tnNumber >= RESTRICTED_TN_FLOOR && tnNumber < RESTRICTED_TN_CEILING)
            {
                lastIssued = PreviewNextRestrictedTnNumber();
            }

            if (tnNumber < lastIssued)
            {
                int status = _lockService.LockTn(tnNumber, domain, user);
                if (status >= 0)
                    result = true;
                else
                    throw new Exception(_lockService.GetMessage(status));
            }
            return result;
        }

        public void LockStatusCounters(string domain, string user) 
        {
            int result = _lockService.LockStatusCounters(domain, user);
            if (result < 0)
                throw new Exception(_lockService.GetMessage(result));
            return;
        }

        public void UnlockStatusCounters(string domain, string user)
        {
            int result = _lockService.UnlockStatusCounters(domain, user);
            if (result < 0)
                throw new Exception(_lockService.GetMessage(result));
            return;
        }

        public int PreviewNextTnNumber()
        {
            return _lockService.GetNextTnNumber();
        }

        public int PreviewNextRestrictedTnNumber()
        {
            return _lockService.GetNextRestrictedTnNumber();
        }

        public int PreviewNextBoxNumber()
        {
            return _lockService.GetNextBoxNumber();
        }

        public void IncrementTnNumber(string domain, string user)
        {
            int result = _lockService.IncrementTnNumber(domain, user);
            if (result < 0)
                throw new Exception(_lockService.GetMessage(result));
        }

        public void IncrementRestrictedTnNumber(string domain, string user)
        {
            int result = _lockService.IncrementRestrictedTnNumber(domain, user);
            if (result < 0)
                throw new Exception(_lockService.GetMessage(result));
        }

        public void IncrementBoxNumber(string domain, string user)
        {
            int result = _lockService.IncrementBoxNumber(domain, user);
            if (result < 0)
                throw new Exception(_lockService.GetMessage(result));
        }

        public bool IsCheckedOut(int tn) {
            LockStatusService lockStatus = new LockStatusService();
            TnLock lockedTN = lockStatus.GetTnLock(tn);
            return (lockedTN != null);
        }

        public bool IsCheckedOut(string id) {
            LockStatusService lockStatus = new LockStatusService();
            IdLock lockedID = lockStatus.GetIdLock(id);
            return (lockedID != null);
        }

        public StringCollection GetAllCheckedOutIds(string domainName) {
            LockStatusService lockStatusService = new LockStatusService();
            IdLock[] lockedIDs = lockStatusService.GetLockedIDs(domainName);

            StringCollection ids = new StringCollection();
            if (lockedIDs != null) {
                foreach (IdLock lockedID in lockedIDs) {
                    ids.Add(lockedID.ID);                    
                }
            }

            return ids;
        }

        public IntegerCollection GetAllCheckedOutTns(string domainName) {
            LockStatusService lockStatusService = new LockStatusService();
            TnLock[] lockedTNs = lockStatusService.GetLockedTNs(domainName);

            IntegerCollection tns = new IntegerCollection();
            if (lockedTNs != null) {
                foreach (TnLock lockedTn in lockedTNs) {
                    tns.Add(lockedTn.TN);                    
                }
            }

            return tns;
        }
    }
}
