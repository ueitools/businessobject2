using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace BusinessObject
{
    public static class Check
    {

        public static bool IdName(ref string id)
        {
            char mode;
            int idNumber;

            if (string.IsNullOrEmpty(id))
                return false;

            if (!char.IsLetter(id[0]))
                return false;

            if (!int.TryParse(id.Substring(1), out idNumber))
                return false;

            if (idNumber > 9999)
                return false;

            mode = id[0];
            id = string.Format("{0}{1:D4}", mode, idNumber).ToUpper();
            return true;
        }
    }
}
