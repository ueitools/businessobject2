using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class DigitGroupTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(DigitGroupCollection 
            digitGroupList)
        {
            List<string> tableList = new List<string>();

            DigitGroupRow row;
            int order;

            int count = 0;
            string table = "";

            foreach (DigitGroup digitGroup in digitGroupList)
            {
                order = 0;

                foreach (string data in digitGroup.DataList)
                {
                    if (table != "")
                        table += " UNION ALL ";

                    row = new DigitGroupRow();
                    
                    row.Project_Code = digitGroup.Project_Code;
                    row.Code = digitGroup.GroupNumber;
                    row.Order = order;
                    row.Data = data;
                    order++;

                    table += GetLocalField(row);

                    // reset table
                    count++;
                    if (count == _bulkSize)
                    {
                        tableList.Add(table);
                        table = "";
                        count = 0;
                    }
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(DigitGroupRow row)
        {
            string query = "SELECT " +

                row.Project_Code + ", " +
                row.Code + ", " +
                row.Order+ ", " +
                CommonFunctions.SingleQuote(row.Data);

            return query;
        }
    }
}
