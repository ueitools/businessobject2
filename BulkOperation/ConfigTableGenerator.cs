using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class ConfigTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(ConfigCollection configList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (Config config in configList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(config);

                // reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(Config config)
        {
            string query = "SELECT " +

                config.Project_Code + ", " +
                CommonFunctions.SingleQuote(config.Name) + ", " +
                CommonFunctions.SingleQuote(config.Value);

            return query;
        }
    }
}
