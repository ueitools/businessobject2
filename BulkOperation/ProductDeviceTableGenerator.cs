using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class ProductDeviceTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(KeyCollection keyList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (Key key in keyList)
            {
                if (key.DeviceInfo == null)
                    continue;

                if (table != "")
                    table += " UNION ALL ";

                key.DeviceInfo.DeviceKey_RID = key.RID;

                table += GetLocalField(key.DeviceInfo);

                /// reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(DeviceInfo deviceInfo)
        {
            string query = "SELECT " +

                deviceInfo.DeviceKey_RID + ", " +
                deviceInfo.DeviceNumber + ", " +
                CommonFunctions.SingleQuote(deviceInfo.ModeList) + ", " +
                CommonFunctions.SingleQuote(deviceInfo.DefaultID);

            return query;
        }
    }
}
