using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class OutronIntronTableGenerator
    {
        #region private fields
        int _bulkSize = 500;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(int startRID,
            KeyCollection keyList)
        {
            List<string> tableList = new List<string>();

            OutronIntron outronIntron;
            int count = 0,order;
            int curRID = startRID;
            string table = "";

            foreach (Key key in keyList)
            {
                foreach (PDLInfo pdlInfo in key.PDLInfoList)
                {
                    order = 0;
                    foreach (string intron in pdlInfo.IntronList)
                    {
                        if (table != "")
                            table += " UNION ALL ";

                        outronIntron = new OutronIntron();
                        outronIntron.KeyOutron_RID = curRID;
                        outronIntron.Order = order;
                        outronIntron.Intron = intron;

                        table += GetLocalField(outronIntron);
                        order++;

                        /// reset table
                        count++;
                        if (count == _bulkSize)
                        {
                            tableList.Add(table);
                            table = "";
                            count = 0;
                        }
                    }
                    curRID++;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(int startRID,
            PDLInfoCollection pdlInfoList)
        {
            List<string> tableList = new List<string>();

            OutronIntron outronIntron;
            int count = 0, order;
            int curRID = startRID;
            string table = "";

            foreach (PDLInfo pdlInfo in pdlInfoList)
            {
                order = 0;
                foreach (string intron in pdlInfo.IntronList)
                {
                    if (table != "")
                        table += " UNION ALL ";

                    outronIntron = new OutronIntron();
                    outronIntron.KeyOutron_RID = curRID;
                    outronIntron.Order = order;
                    outronIntron.Intron = intron;

                    table += GetLocalField(outronIntron);
                    order++;

                    /// reset table
                    count++;
                    if (count == _bulkSize)
                    {
                        tableList.Add(table);
                        table = "";
                        count = 0;
                    }
                }
                curRID++;
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outronIntronList"></param>
        /// <returns></returns>
        public List<string> GetLocalTable(OutronIntronCollection
            outronIntronList)
        {
            List<string> tableList = new List<string>();
            string table = "";
            int count = 0;

            foreach (OutronIntron outronIntron in outronIntronList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(outronIntron);

                /// reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        private string GetLocalField(OutronIntron outronIntron)
        {
            string query = "SELECT " +

                outronIntron.KeyOutron_RID + ", " +
                outronIntron.Order + ", " +
                CommonFunctions.SingleQuote(outronIntron.Intron);

            return query;
        }
    }
}
