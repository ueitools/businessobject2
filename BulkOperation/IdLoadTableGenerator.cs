using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class IdLoadTableGenerator
    {
        #region private fields
        int _bulkSize = 600;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(IDLoadCollection idLoadList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (IDLoad load in idLoadList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(load);

                // reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(IDLoad load)
        {
            string query = "SELECT " +

                load.Project_Code + ", " +
                CommonFunctions.SingleQuote(load.ID) + ", " +
                CommonFunctions.SingleQuote(load.DoUpdate)+ ", " +
                CommonFunctions.SingleQuote(load.FromID);

            return query;
        }
    }
}
