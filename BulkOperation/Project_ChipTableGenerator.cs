using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class Project_ChipTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(ChipCollection chipList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (Chip chip in chipList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(chip);

                // reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(Chip chip)
        {
            string query = "SELECT " +

                chip.Project_Code + ", " +
                chip.Chip_Code;

            return query;
        }
    }
}
