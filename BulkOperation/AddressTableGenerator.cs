using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class AdressTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(AddressCollection addressList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (Address address in addressList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(address);

                // reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(Address address)
        {
            string query = "SELECT " +

                address.Project_Code + ", " +
                CommonFunctions.SingleQuote(address.Version) + ", " +
                (int)address.Type + ", " +
                CommonFunctions.SingleQuote(address.VersionAddress) + ", " +
                CommonFunctions.SingleQuote(address.BeginAddress) + ", " +
                CommonFunctions.SingleQuote(address.EndAddress);

            return query;
        }
    }
}
