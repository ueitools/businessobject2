using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class RelationTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion

        /// <summary>
        /// GetIDRID insert proxuy id into function table if not exists
        /// </summary>
        /// <param name="dridList"></param>
        /// <returns></returns>
        public List<string> GetLocalTable(List<DRID> dridList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            Relation relation = new Relation();
            string table = "";
            foreach (DRID drid in dridList)
            {
                if (table != "")
                    table += " UNION ALL ";

                relation.Parent = drid.DR_RID;
                relation.Child = DAOFactory.Function().GetIDRID(drid.ID);
                table += GetLocalField(relation);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(List<DRTN> drtnList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            Relation relation = new Relation();
            string table = "";
            foreach (DRTN drtn in drtnList)
            {
                if (table != "")
                    table += " UNION ALL ";

                relation.Parent = drtn.DR_RID;
                relation.Child = DAOFactory.Function().GetTNRID(drtn.TN);
                table += GetLocalField(relation);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// GetCountryCode insert country into country_ref table if not exists
        /// </summary>
        /// <param name="countryList"></param>
        /// <returns></returns>
        public List<string> GetLocalTable(CountryCollection countryList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            Relation relation = new Relation();
            string table = "";
            foreach (Country country in countryList)
            {
                if (table != "")
                    table += " UNION ALL ";

                relation.Parent = country.DR_RID;
                relation.Child = DAOFactory.Misc().GetCountryCode(country);
                table += GetLocalField(relation);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(DeviceTypeCollection deviceTypeList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            Relation relation = new Relation();
            string table = "";
            foreach (DeviceType deviceType in deviceTypeList)
            {
                if (table != "")
                    table += " UNION ALL ";

                relation.Parent = deviceType.DR_RID;
                relation.Child = 
                    DAOFactory.Misc().GetDeviceTypeCode(deviceType);
                table += GetLocalField(relation);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(OpInfoCollection opInfoList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            Relation relation = new Relation();
            string table = "";
            foreach (OpInfo opInfo in opInfoList)
            {
                if (table != "")
                    table += " UNION ALL ";

                relation.Parent = opInfo.DR_RID;
                relation.Child =
                    DAOFactory.Misc().GetOpInfoCode(opInfo);
                table += GetLocalField(relation);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// Parent, Child
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        private string GetLocalField(Relation relation)
        {
            string query = "SELECT " +

                relation.Parent + ", " +
                relation.Child;

            return query;
        }
    }

    class DRID
    {
        int _dr_rid;
        string _id;

        public int DR_RID
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }
    }

    class DRTN
    {
        int _dr_rid;
        int _tn;

        public int DR_RID
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }
    }
}
