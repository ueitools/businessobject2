using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class AlternateCodeTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion

        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(AlternateIDCollection altIDList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            string table = "";

            foreach (AlternateID id in altIDList)
            {
                if (table != "")
                    table += " UNION ALL ";

                table += GetLocalField(id);

                // reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private string GetLocalField(AlternateID id)
        {
            string query = "SELECT " +
                id.Project_Code + ", " +
                CommonFunctions.SingleQuote(id.InternalID) + ", " +
                CommonFunctions.SingleQuote(id.ExternalID) + ", " +
                id.NumCriticalOutron + ", " +
                id.MatchCount;

            return query;
        }
    }
}
