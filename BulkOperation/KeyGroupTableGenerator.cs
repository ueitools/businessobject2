using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class KeyGroupTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(KeyCollection keyList)
        {
            List<string> tableList = new List<string>();

            int count = 0,order;
            string table = "";
            Key keyFound;
            KeyGroup group;

            foreach (Key key in keyList)
            {
                if (key.DeviceInfo == null)
                    continue;

                order = 0;   
                foreach (string keyLabel in key.DeviceInfo.KeyLabelList)
                {
                    if (table != "")
                        table += " UNION ALL ";

                    keyFound = keyList.Find(keyLabel);

                    if (keyFound == null)
                    {
                        throw new ApplicationException(String.Format(
                            "Key with label {0} can't be found in key List",
                            keyLabel));
                    }
                    group = new KeyGroup();
                    group.Device_RID = key.RID;
                    group.Order = order;
                    group.Key_RID = keyFound.RID;
                    order++;

                    table += GetLocalField(group);

                    /// reset table
                    count++;
                    if (count == _bulkSize)
                    {
                        tableList.Add(table);
                        table = "";
                        count = 0;
                    }
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(KeyCollection keyList, 
            List<int> orderList)
        {
            List<string> tableList = new List<string>();

            int count = 0, order;
            string table = "";
            Key keyFound;
            KeyGroup group;
            int i = 0;
            foreach (Key key in keyList)
            {
                if (key.DeviceInfo == null)
                    continue;

                order = orderList[i++];

                foreach (string keyLabel in key.DeviceInfo.KeyLabelList)
                {
                    if (table != "")
                        table += " UNION ALL ";

                    keyFound = keyList.Find(keyLabel);

                    if (keyFound == null)
                    {
                        throw new ApplicationException(String.Format(
                            "Key with label {0} can't be found in key List",
                            keyLabel));
                    }
                    group = new KeyGroup();
                    group.Device_RID = key.RID;
                    group.Order = order;
                    group.Key_RID = keyFound.RID;
                    order++;

                    table += GetLocalField(group);

                    /// reset table
                    count++;
                    if (count == _bulkSize)
                    {
                        tableList.Add(table);
                        table = "";
                        count = 0;
                    }
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(KeyGroup group)
        {
            string query = "SELECT " +

                group.Device_RID + ", " +
                group.Order + ", " +
                group.Key_RID;

            return query;
        }
    }
}
