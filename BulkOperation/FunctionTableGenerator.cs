using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class FunctionTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion

        public List<string> GetLocalTable(string id, int startRID,
            FunctionCollection functionList)
        {
            foreach (Function function in functionList)
                function.ID = id;

            return GetLocalTable(startRID, functionList);
        }

        public List<string> GetLocalTable(int tn, int startRID,
            FunctionCollection functionList)
        {
            foreach (Function function in functionList)
                function.TN = tn;

            return GetLocalTable(startRID, functionList);
        }
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        //// 
        private List<string> GetLocalTable(int startRID, 
            FunctionCollection functionList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";
            foreach (Function function in functionList)
            {
                if (table != "")
                    table += " UNION ALL ";

                function.RID = curRID;
                table += GetLocalField(function);
                curRID += 1;

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        private string GetLocalField(Function function)
        {
            string strTN = "NULL";
            if (function.TN != 0)
                strTN = String.Format("{0}", function.TN);

            string query = "SELECT " +

                function.RID + ", " +
                strTN + ", " +
                CommonFunctions.SingleQuote(function.ID) + ", " +
                CommonFunctions.SingleQuote(function.IsConnection) + ", " +
                CommonFunctions.SingleQuote(function.Label) + ", " +

                CommonFunctions.SingleQuoteNoNull(function.Intron) + ", " +
                CommonFunctions.SingleQuoteNoNull(function.IntronPriority) + ", " +
                CommonFunctions.SingleQuote(function.Data) + ", " +
                CommonFunctions.SingleQuote(function.Comment) + ", " +
                CommonFunctions.SingleQuoteNoNull(function.LegacyLabel) + ", " +

                CommonFunctions.SingleQuote(function.Filename);

            return query;
        }
    }
}
