using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class LogTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion

        public List<string> GetLocalTable(string id, int startRID,
            LogCollection logList)
        {
            foreach (Log log in logList)
                log.ID = id;

            return GetLocalTable(startRID, logList);
        }

        public List<string> GetLocalTable(int tn, int startRID,
            LogCollection logList)
        {
            foreach (Log log in logList)
                log.TN = tn;

            return GetLocalTable(startRID, logList);
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        //// 
        private List<string> GetLocalTable(int startRID,
            LogCollection logList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";
            foreach (Log log in logList)
            {
                if (table != "")
                    table += " UNION ALL ";

                log.RID = curRID;
                table += GetLocalField(log);
                curRID += 1;

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, Time, Operator, 
        /// Content
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        private string GetLocalField(Log log)
        {
            string strTN = "NULL";
            if (log.TN != 0)
                strTN = String.Format("{0}", log.TN);

            string query = "SELECT " +

                log.RID + ", " +
                strTN + ", " +
                CommonFunctions.SingleQuote(log.ID) + ", " +
                CommonFunctions.SingleQuote(log.Time.ToUniversalTime()) + ", " +
                CommonFunctions.SingleQuote(log.Operator) + ", " +
                CommonFunctions.SingleQuote(log.Content);
                
            return query;
        }
    }
}
