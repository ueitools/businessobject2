using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class KeyOutronTableGenerator
    {
        #region private fields
        int _bulkSize = 600;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(int startRID,
            KeyCollection keyList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";
            foreach (Key key in keyList)
            {
                key.PDLInfoList.SynchKeyRID(key.RID);

                foreach (PDLInfo pdlInfo in key.PDLInfoList)
                {
                    if (table != "")
                        table += " UNION ALL ";

                    pdlInfo.RID = curRID;
                    table += GetLocalField(pdlInfo);
                    curRID++;

                    /// reset table
                    count++;
                    if (count == _bulkSize)
                    {
                        tableList.Add(table);
                        table = "";
                        count = 0;
                    }
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        public List<string> GetLocalTable(int startRID,
            PDLInfoCollection pdlInfoList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";

            foreach (PDLInfo pdlInfo in pdlInfoList)
            {
                if (table != "")
                    table += " UNION ALL ";

                pdlInfo.RID = curRID;
                table += GetLocalField(pdlInfo);
                curRID++;

                /// reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        private string GetLocalField(PDLInfo pdlInfo)
        {
            string query = "SELECT " +

                pdlInfo.RID + ", " +
                pdlInfo.Key_RID + ", " +
                CommonFunctions.SingleQuote(pdlInfo.Mode) + ", " +
                CommonFunctions.SingleQuote(pdlInfo.Outron) + ", " +
                pdlInfo.Group;

            return query;
        }
    }
}
