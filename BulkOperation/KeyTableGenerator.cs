using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    class KeyTableGenerator
    {
        #region private fields
        int _bulkSize = 400;
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }
        #endregion
        
        /////////////////////////////////////////////////////////////////////
        ////
        //// public
        //// 
        public List<string> GetLocalTable(int startRID,
            KeyCollection keyList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";
            foreach (Key key in keyList)
            {
                if (table != "")
                    table += " UNION ALL ";

                key.RID = curRID;
                table += GetLocalField(key);
                curRID++;

                /// reset table
                count++;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }
            }

            if(table != "")
                tableList.Add(table);

            return tableList;
        }

        /// <summary>
        /// RID, TN, ID, IsConnection, Label, 
        /// Intron, IntronPriority, Data, Comment, LegacyLabel, 
        /// Filename
        /// 
        /// TN is null if value is 0
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetLocalField(Key key)
        {
            string query = "SELECT " +

                key.RID + ", " +
                key.Project_Code + ", " +
                key.Position + ", " +
                CommonFunctions.SingleQuote(key.Label) + ", " +
                CommonFunctions.SingleQuote(key.Description) + ", " +
                key.Scan_Code;

            return query;
        }
    }
}
