using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace BusinessObject
{
    /// <summary>
    /// default bulk size: 100
    /// </summary>
    class DeviceTableGenerator
    {
        #region private fields
        int _bulkSize = 100;
        List<DRID> _dridList = new List<DRID>();
        List<DRTN> _drtnList = new List<DRTN>();
        CountryCollection _countryList = new CountryCollection();
        DeviceTypeCollection _deviceTypeList = new DeviceTypeCollection();
        OpInfoCollection _opInfoList = new OpInfoCollection();
        #endregion

        #region Business Properties
        public int BulkSize
        {
            get { return _bulkSize; }
            set { _bulkSize = value; }
        }

        public List<DRID> DRIDList
        {
            get { return _dridList; }
            set { _dridList = value; }
        }

        public List<DRTN> DRTNList
        {
            get { return _drtnList; }
            set { _drtnList = value; }
        }

        public CountryCollection CountryList
        {
            get { return _countryList; }
            set { _countryList = value; }
        }

        public DeviceTypeCollection DeviceTypeList
        {
            get { return _deviceTypeList; }
            set { _deviceTypeList = value; }
        }

        public OpInfoCollection OpInfoList
        {
            get { return _opInfoList; }
            set { _opInfoList = value; }
        }
        #endregion


        /// <summary>
        /// return batched table list
        /// sublist handling
        /// </summary>
        /// <param name="deviceList"></param>
        /// <returns></returns>
        public List<string> GetLocalTable(int startRID, DeviceCollection deviceList)
        {
            List<string> tableList = new List<string>();

            int count = 0;
            int curRID = startRID;
            string table = "";
            foreach (Device device in deviceList)
            {
                if (table != "")
                    table += " UNION ALL ";

                device.RID = curRID;
                device.Brand_Code =
                    DAOFactory.Misc().GetBrandCode(device.Brand);

                table += GetLocalField(device);
                UpdateSubList(device);

                /// reset table
                count += 1;
                if (count == _bulkSize)
                {
                    tableList.Add(table);
                    table = "";
                    count = 0;
                }

                curRID += 1;
            }

            if (table != "")
                tableList.Add(table);

            return tableList;
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        //// 
        private void UpdateSubList(Device device)
        {
            DRID drid = new DRID();
            drid.DR_RID = device.RID;
            drid.ID = device.ID;
            _dridList.Add(drid);

            if (device.TN != 0)
            {
                DRTN drtn = new DRTN();
                drtn.DR_RID = device.RID;
                drtn.TN = device.TN;
                _drtnList.Add(drtn);
            }

            UpdateDR_RID(device.RID, device.CountryList);
            UpdateDR_RID(device.RID, device.DeviceTypeList);
            UpdateDR_RID(device.RID, device.OpInfoList);

            AppendList(_countryList, device.CountryList);
            AppendList(_deviceTypeList, device.DeviceTypeList);
            AppendList(_opInfoList, device.OpInfoList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentList"></param>
        /// <param name="addList"></param>
        private void AppendList(IList currentList, IList listToAdd)
        {
            foreach (Object item in listToAdd)
                currentList.Add(item);
        }

        private void UpdateDR_RID(int rid, CountryCollection countryList)
        {
            foreach (Country country in countryList)
                country.DR_RID = rid;
        }

        private void UpdateDR_RID(int rid, DeviceTypeCollection deviceTypeList)
        {
            foreach (DeviceType deviceType in deviceTypeList)
                deviceType.DR_RID = rid;
        }

        private void UpdateDR_RID(int rid, OpInfoCollection opInfoList)
        {
            foreach (OpInfo opInfo in opInfoList)
                opInfo.DR_RID = rid;
        }

        /// <summary>
        /// RID, Brand_Code, Manufacturer_Code, UEIProduct_Code, Model, 
        /// Branch, Date, Collector, PurchaseYear, PriceInDigit, 
        /// ProductManual, IsCodebook, IsTarget, IsPartNumber, IsRetail,
        /// Source_RID, Text
        /// 
        /// Manufacturer_Code, UEIProduct_Code are assumed as null
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private string GetLocalField(Device device)
        {
            string query = "SELECT " +

                device.RID + ", " +
                device.Brand_Code + ", " +
                "NULL, " +      ///Manufacturer_Code
                "NULL, " +      ///UEIProduct_Code
                CommonFunctions.SingleQuote(device.Model) + ", " +

                CommonFunctions.SingleQuote(device.Branch) + ", " +
                CommonFunctions.SingleQuote(device.Date) + ", " +
                CommonFunctions.SingleQuote(device.Collector) + ", " +
                CommonFunctions.SingleQuote(device.PurchaseYear) + ", " +
                CommonFunctions.SingleQuote(device.PriceInDigit) + ", " +

                CommonFunctions.SingleQuote(device.ProductManual) + ", " +
                CommonFunctions.SingleQuote(device.IsCodebook) + ", " +
                CommonFunctions.SingleQuote(device.IsTarget) + ", " +
                CommonFunctions.SingleQuote(device.IsPartNumber) + ", " +
                CommonFunctions.SingleQuote(device.IsRetail) + ", " +

                CommonFunctions.SingleQuote(device.Source_RID) + ", " +
                CommonFunctions.SingleQuote(device.Text);

            return query;
        }
    }
}
