using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public static class ProductDBFunctions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ID GetID(string id)
        {
            return GetIDWithOptimization(id, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ID GetSimpleID(string id)
        {
            return GetIDWithOptimization(id, true);
        }

        /// <summary>        
        /// </summary>
        /// <param name="id"></param>
        /// <param name="simpleGet">
        /// The device select id query was slowing down some calls and 
        /// is not needed every time, so it was made optional
        /// </param>
        /// <returns></returns>
        private static ID GetIDWithOptimization(string id, bool simpleGet)
        {
            ID oID = new ID();
            oID.Header = DAOFactory.IDHeader().Select(id);
            oID.FunctionList = DAOFactory.ReadOnly().IDFunctionSelect(id);

            if (simpleGet)
                oID.DeviceList = new DeviceCollection();
            else
               oID.DeviceList = DAOFactory.Device().SelectID(id);

            return oID;
        }

        public static ID GetIDWithModelFilter(string id, string model) {
            ID oID = new ID();
            oID.Header = DAOFactory.IDHeader().Select(id);
            oID.FunctionList = DAOFactory.ReadOnly().IDFunctionModelSelect(id, model);
            oID.DeviceList = new DeviceCollection();

            return oID;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static Project GetProject(string projectName)
        {
            return GetProject(projectName, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static Project GetProject(string projectName, bool getPDLInfo)
        {
            return DAOFactory.Project().Load(projectName, getPDLInfo);
        }

        /// <summary>
        /// Returns a list of all project headers in the project database
        /// </summary>
        /// <returns></returns>
        public static ProjectHeader GetProjectHeader(string projectName)
        {
            return DAOFactory.Project().SelectHeader(projectName);
        }

        /// <summary>
        /// Returns a list of all project headers in the project database
        /// </summary>
        /// <returns></returns>
        public static ProjectHeaderCollection GetAllProjects()
        {
            return DAOFactory.Project().ProjectList();
        }

        /// <summary>
        /// Returns a list of all project headers in table project 
        /// with the executor
        /// </summary>
        /// <param name="executor"></param>
        /// <returns></returns>
        public static ProjectHeaderCollection GetProjectsByExecutor(
            int executor)
        {
            return DAOFactory.Project().ProjectList(executor);
        }
        /// <summary>
        /// Returns a list of all project headers in table project
        /// with the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ProjectHeaderCollection GetProjectsByID(string id)
        {
            return DAOFactory.Project().ProjectList(id);
        }

        public static ProjectHeaderCollection GetProjectsByAddressVersion(string addressVersion)
        {
            return DAOFactory.Project().ProjectListByAddressVersion(addressVersion);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static ProductCollection GetProjectProducts(int project)
        {
            return DAOFactory.Project().ProjectProductList(project);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="chipNumber"></param>
        /// <param name="URC_number"></param>
        /// <param name="revision"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static HashtableCollection GetProjectInfo(string projectName, string chipNumber,
                                        int URC_number, string revision, string description)
        {
            return DAOFactory.Project().LoadInfo(projectName, chipNumber,
                                        URC_number, revision, description);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static DataMapCollection GetDataMapList(
            string projectName, string mode)
        {
            return DAOFactory.Pick().GetDataMap(
                projectName, mode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excutorCodeList"></param>
        /// <param name="platformCode"></param>
        /// <returns></returns>
        public static IList<Image> GetExecutorOBJ(
            IList<int> excutorCodeList, int platformCode)
        {
            return GetExecutorOBJ(excutorCodeList, platformCode, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excutorCodeList"></param>
        /// <param name="platformCode"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static IList<Image> GetExecutorOBJ(
            IList<int> excutorCodeList, int platformCode, string version)
        {
            return DAOFactory.ReadOnly().GetExecutorOBJ(
                excutorCodeList, platformCode, version);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excutorCodeList"></param>
        /// <param name="platformName"></param>
        /// <returns></returns>
        public static IList<Image> GetExecutorUSH(
            IList<int> excutorCodeList, string platformName)
        {
            return GetExecutorUSH(excutorCodeList, platformName, null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="excutorCodeList"></param>
        /// <param name="platformName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static IList<Image> GetExecutorUSH(
            IList<int> excutorCodeList, string platformName, string version)
        {
            return DAOFactory.ReadOnly().GetExecutorUSH(
                excutorCodeList, platformName, version);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="prodcutName"></param>
        /// <returns></returns>
        public static int GetGroupNumber(string id, string prodcutName)
        {
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetNopData(string id)
        {
            return DAOFactory.Pick().GetNopData(id);
        }

        /// <summary>
        /// Flas DoUpdate as 'Y' (available as update)
        /// </summary>
        /// <param name="id"></param>
        public static void FlagIDAsObsolete(string id)
        {
            FirmwareDAO.GetInstance().FlagIDAsObsolete(id);
        }

        public static void FlagExecutorAsObsolete(int executorCode)
        {
            FirmwareDAO.GetInstance().FlagExecutorAsObsolete(executorCode);
        }

        /// <summary>
        /// Update the DoUpdate flag in the table executorLoad
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="executorCode"></param>
        /// <param name="update"></param>
        public static void SetExecutorDoUpdate(int projectCode,
            int executorCode, bool update)
        {
            FirmwareDAO.GetInstance().SetExecutorDoUpdate(projectCode,
                executorCode, update);
        }
        /// <summary>
        /// Update the DoUpdate flag in the table idLoad
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="id"></param>
        /// <param name="update"></param>
        public static void SetIDDoUpdate(int projectCode, 
            string id, bool update)
        {
            FirmwareDAO.GetInstance().SetIDDoUpdate(projectCode,
                id, update);
        }
        /// <summary>
        /// Get executors
        /// </summary>
        /// <param name="projectName"></param>
        public static ExecutorLoadCollection GetAllExecutors()
        {
            return FirmwareDAO.GetInstance().SelectAllExecutors();
        }

        public static StringCollection GetAllID()
        {
            return FirmwareDAO.GetInstance().SelectAllID();
        }

        public static IList<BrandId> GetBrandIdList(string market, string branch, string mode,
            List<string> idLoad)
        {
            List<BrandId> result = new List<BrandId>();
            List<string> partialIdLoad = new List<string>();
            const int batchSize = 1800;

            for (int i = 0; i < idLoad.Count; i += batchSize)
            {
                partialIdLoad.Clear();
                for (int partial = i; partial < Math.Min(i + batchSize, idLoad.Count); partial++)
                {
                    partialIdLoad.Add(idLoad[partial]);
                }
                result.AddRange(DAOFactory.Project().GetBrandIdList(market, branch, mode, partialIdLoad));
            }
            result.Sort(delegate(BrandId id1, BrandId id2) { return id1.Brand.CompareTo(id2.Brand); });
            return result;
        }

        public static IList<string> GetBrandListOptimized(string market, string branch, string mode,
            List<string> idLoad)
        {
            List<string> result = new List<string>();
            const int batchSize = 1800;

            for (int i = 0; i < idLoad.Count; i += batchSize)
            {
                StringBuilder partialIdLoad = new StringBuilder();
                for (int partial = i; partial < Math.Min(i + batchSize, idLoad.Count); partial++)
                {
                    partialIdLoad.Append(idLoad[partial]);
                    partialIdLoad.Append(",");
                }
                if (partialIdLoad.Length > 0)
                    partialIdLoad.Remove(partialIdLoad.Length-1, 1);
                result.AddRange(DAOFactory.Project().GetBrandIdListOptimized(market, branch, mode, partialIdLoad.ToString()));
            }
            result.Sort(delegate(string id1, string id2) { return id1.CompareTo(id2); });
            return result;
        }

        public static ChipCollection GetChipList()
        {
            return DAOFactory.Project().GetChipList();
        }

        /// <summary>
        /// Given a project name, get the chip list
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static ChipCollection GetChipList(string projectName)
        {
            return DAOFactory.Project().SelectChip(projectName);
        }
        /// <summary>
        /// Get the syth_digit 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static string GetSynthDigit(string projectName)
        {
            return DAOFactory.Project().SelectSynthDigit(projectName);
        }

        /// <summary>
        /// Get firmware address list
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static AddressCollection GetAddressList(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectAddress(projectName);
        }
        /// <summary>
        /// Get config list
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static ConfigCollection GetConfigList(string projectName)
        {
            return DAOFactory.Project().SelectConfig(projectName);
        }
        /// <summary>
        /// Get firmware header
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static FirmwareHeader GetFirmwareHeader(
            string projectName)
        {
            return FirmwareDAO.GetInstance().SelectHeader(projectName);
        }

        /// <summary>
        /// get the key list
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static KeyCollection GetKeyList(string projectName)
        {
            return KeyDAO.GetInstance().SelectKey(projectName);
        }

        /// <summary>
        /// Get device list
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static DeviceInfoCollection GetDeviceList(
            string projectName)
        {
            return KeyDAO.GetInstance().SelectDeviceInfo(projectName);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static PDLInfoCollection GetPDLList(
            string projectName)
        {
            return KeyDAO.GetInstance().SelectPDLInfo(projectName,
                true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static PDLInfoCollection GetPDLList(
            string projectName, bool getIntronList)
        {
            return KeyDAO.GetInstance().SelectPDLInfo(projectName,
                getIntronList);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static IDLoadCollection GetIDLoadList(
            string projectName)
        {
            return FirmwareDAO.GetInstance().SelectIDLoad(projectName);
        }

        public static IDLoadCollection GetIDLoadList(int projectCode)
        {
            return FirmwareDAO.GetInstance().SelectIDLoadWithCode(projectCode);
        }


        public static FirmwareHeader GetFirmwareHeaderWithCode(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectHeaderWithCode(projectName);
        }

        public static void InsertIDLoadList(IDLoadCollection idLoadCollection)
        {
            FirmwareDAO.GetInstance().Insert(idLoadCollection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="deviceNumber"></param>
        /// <param name="keyList"></param>
        /// <returns></returns>
        public static StringCollection GetKeyGroup(string projectName,
            int deviceNumber, KeyCollection keyList)
        {
            return KeyDAO.GetInstance().SelectKeyGroup(projectName,
                deviceNumber, keyList);
        }
        /// <summary>
        /// update firmware header
        /// </summary>
        /// <param name="firmware"></param>
        public static void UpdateFirmwareHeader(
            FirmwareHeader firmware)
        {
            FirmwareDAO.GetInstance().Update(firmware);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void UpdateConfig(Config config)
        {
            DAOFactory.Project().Update(config);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        public static void UpdateFirmwareAddress(
            Address address)
        {
            FirmwareDAO.GetInstance().Update(address);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void UpdateKey(Key key)
        {
            KeyDAO.GetInstance().Update(key);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceInfo"></param>
        public static void UpdateDeviceInfo(DeviceInfo deviceInfo)
        {
            KeyDAO.GetInstance().UpdateDeviceInfo(deviceInfo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfo"></param>
        public static void UpdatePDLInfo(PDLInfo pdlInfo)
        {
            KeyDAO.GetInstance().Update(pdlInfo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outronIntron"></param>
        public static void UpdateOutronIntron(OutronIntron outronIntron)
        {
            KeyDAO.GetInstance().Update(outronIntron);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idLoad"></param>
        public static void UpdateIDLoad(IDLoad idLoad)
        {
            FirmwareDAO.GetInstance().Update(idLoad);
        }
        /// <summary>
        /// delete the introns with keyOutron_RID
        /// </summary>
        /// <param name="keyOutron_RID"></param>
        public static void DeleteIntronList(int keyOutron_RID)
        {
            KeyDAO.GetInstance().DeleteIntronList(keyOutron_RID);
        }
        /// <summary>
        /// insert the introns
        /// </summary>
        /// <param name="outronIntronList"></param>
        public static void InsertIntronlist(OutronIntronCollection
            outronIntronList)
        {
            KeyDAO.GetInstance().InsertIntronList(outronIntronList);
        }
        /// <summary>
        /// insert configList
        /// </summary>
        /// <param name="configList"></param>
        public static void InsertConfigs(ConfigCollection configList)
        {
            DAOFactory.Project().Save(configList);
        }
        /// <summary>
        /// insert key list
        /// </summary>
        /// <param name="keyList"></param>
        public static void InsertKeyList(KeyCollection keyList)
        {
            KeyDAO.GetInstance().InsertkeyList(keyList);
        }
        /// <summary>
        /// insert key list and outrons/Introns in it
        /// </summary>
        /// <param name="keyList"></param>
        public static void InsertKeyOurtonIntron(KeyCollection keyList)
        {
            KeyDAO.GetInstance().Insert(keyList);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        public static void InsertKeyGroupWithPositiveOrder(
            KeyCollection keyList)
        {
            KeyDAO.GetInstance().InsertKeyGroupWithPositiveOrder(
                keyList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        public static void InsertKeyGroup(KeyCollection keyList)
        {
            KeyDAO.GetInstance().InsertKeyGroup(keyList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfoList"></param>
        public static void InsertPDLList(PDLInfoCollection pdlInfoList)
        {
            KeyDAO.GetInstance().InsertPDLInfoList(pdlInfoList);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chipList"></param>
        public static void InsertChipList(ChipCollection chipList)
        {
            DAOFactory.Project().Save(chipList);
        }

        public static void InsertKeyProperty(KeyProperty keyProperty)
        {
            KeyDAO.GetInstance().InsertKeyProperty(keyProperty);
        }

        /// <summary>
        /// delete the config
        /// </summary>
        /// <param name="config"></param>
        public static void DeleteConfig(Config config)
        {
            DAOFactory.Project().Delete(config);
        }
        /// <summary>
        /// delete the chip
        /// </summary>
        /// <param name="chip"></param>
        public static void DeleteChip(Chip chip)
        {
            DAOFactory.Project().DeleteChip(chip);
        }

        /// <summary>
        /// delete the product
        /// </summary>
        /// <param name="chipCode"></param>
        public static void DeleteProduct(int chipCode)
        {
            DAOFactory.Project().DeleteProduct(chipCode);
        }

        /// <summary>
        /// delete the key list
        /// </summary>
        /// <param name="keyList"></param>
        public static void DeleteKeyList(KeyCollection keyList)
        {
            KeyDAO.GetInstance().Delete(keyList);
        }
        /// <summary>
        /// delete from keyGroup with Device_RID
        /// </summary>
        /// <param name="Device_RID"></param>
        public static void DeleteKeyGroup(int Device_RID)
        {
            KeyDAO.GetInstance().DeleteKeyGroup(Device_RID);
        }

        /// <summary>
        /// delete from keyGroup with Device_RID and Key_RID
        /// </summary>
        /// <param name="Device_RID"></param>
        public static void DeleteKeyGroup(int Device_RID, int Key_RID)
        {
            KeyDAO.GetInstance().DeleteKeyGroup(Device_RID, Key_RID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfo"></param>
        public static void DeletePDLInfo(PDLInfo pdlInfo)
        {
            KeyDAO.GetInstance().DeletePDLInfo(pdlInfo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfoList"></param>
        public static void DeletePDLInfoList(
            PDLInfoCollection pdlInfoList)
        {
            foreach (PDLInfo pdlInfo in pdlInfoList)
            {
                DeletePDLInfo(pdlInfo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyProperty"></param>
        public static void DeleteKeyProperty(KeyProperty keyProperty)
        {
            KeyDAO.GetInstance().DeleteKeyProperty(keyProperty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static ExecutorLoadCollection GetExecutorLoad(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectExecutorLoad(projectName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static AlternateIDCollection GetAlternatesInLoad(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectAlternatesInLoad(projectName);
        }

        public static AlternateIDCollection GetAlternatesInLoad(int projectCode)
        {
            return FirmwareDAO.GetInstance().SelectAlternatesInLoad(projectCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static AlternateIDCollection GetAlternatesOrderExternal(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectAlternateCodeTableByExternalID(projectName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        public static FunctionCollection GetAllFunc(string conditions)
        {
            return DAOFactory.Function().SelectFunc(conditions);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chip"></param>
        public static void AddNewChip(Chip chip)
        {
            DAOFactory.Project().AddNewChip(chip);
        }

        /// <summary>
        /// 
        /// </summary>
        public static KeyTypeCollection GetKeyTypeList()
        {
            return KeyDAO.GetInstance().SelectKeyTypeList();
        }
        /// <summary>
        /// 
        /// </summary>
        public static KeyTypeCollection GetSRIKeyTypeList()
        {
            return KeyDAO.GetInstance().SelectSRIKeyTypeList();
        }

        public static void InsertPickID(PickIDSnapshot pickID, bool automaticVersion)
        {
            DAOFactory.Pick().InsertPickID(pickID, automaticVersion);
        }

        public static void DeleteTempPickID(PickID pickID, string projectName)
        {
            DAOFactory.Pick().DeleteTempPickID(pickID, projectName);
        }

        public static void DeleteAllPick(string projectName)
        {
            DAOFactory.Pick().DeleteAllPick(projectName);
        }

        public static PickIDSnapshotCollection GetPickIDSnapshotHeader(string projectName)
        {
            return DAOFactory.Pick().GetPickIDSnapshotHeader(projectName);
        }

        public static void GetPickIDSnapshotLists(PickIDSnapshot snapshot)
        {
            DAOFactory.Pick().GetPickIDSnapshotPrefixList(snapshot);
            DAOFactory.Pick().GetPickIDSnapshotDataMapList(snapshot);
            DAOFactory.Pick().GetPickIDSnapshotOriginalIDLog(snapshot);
            DAOFactory.Pick().GetPickIDSnapshotOriginalIDDevice(snapshot);
            DAOFactory.Pick().GetPickIDSnapshotOriginalIDFunction(snapshot);
        }

        public static int GetMaxVersion(string id, int projectCode)
        {
            return DAOFactory.Pick().GetMaxVersion(id, projectCode);
        }

        public static void GetPickIDSnapshotDatamap(PickIDSnapshot snapshot)
        {
            DAOFactory.Pick().GetPickIDSnapshotDataMapList(snapshot);
        }

        public static void AddAlternateCodeCollection(AlternateIDCollection altIDCollection)
        {
            FirmwareDAO.GetInstance().Insert(altIDCollection);
        }

        public static void DeleteAlternateCode(string projectName, string mode)
        {
            FirmwareDAO.GetInstance().DeleteAlternateCode(projectName, mode);
        }

        public static void DeleteAliasCode(int projectCode, string mode)
        {
            FirmwareDAO.GetInstance().DeleteAliasCode(projectCode, mode);
        }

        public static void DeleteEquivalentAlternateCodes(int projectCode)
        {
            FirmwareDAO.GetInstance().DeleteEquivalentAlternates(projectCode);
        }

        public static void InsertAliasCode(int projectCode, string globalCode, string localCode)
        {
            FirmwareDAO.GetInstance().InsertAliasCode(projectCode, globalCode, localCode);
        }

        public static AliasCodeCollection GetAliasCodes(int projectCode)
        {
            return FirmwareDAO.GetInstance().GetAliasCodes(projectCode);
        }

        public static void InsertGoldenImage(string projectName, byte[] image)
        {
            FirmwareDAO.GetInstance().InsertGoldenImage(projectName, image);
        }

        public static byte[] GetGoldenImage(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectGoldenImage(projectName);
        }

        public static GoldenImageCollection GetGoldenImageCollection(string projectName)
        {
            return FirmwareDAO.GetInstance().SelectGoldenImageCollection(projectName);
        }

        public static void CopyPick2Partially(
           string sourceDbName, string sourceId,
           string destDbName, string destId)
        {
            ProjectDAO projectDao = new ProjectDAO();

            projectDao.CopyPick2Partially(sourceDbName, sourceId, destDbName, destId);
        }

        public static byte[] GetZipIRBinary(string projectName, string id)
        {
            MiscDAO miscDao = new MiscDAO();

            return miscDao.GetZipIRBinary(projectName, id);
        }
    }
}
