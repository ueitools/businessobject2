using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace BusinessObject
{
    public class PropertyListComparer<T> : IComparer<T>
    {
        #region Private Fields
        string[] _fields;
        bool _isAsc;
        #endregion

        #region Business Properties and Methods
        public bool IsAscending
        {
            get { return _isAsc; }
            set { _isAsc = value; }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isAsc"></param>
        /// <param name="fields"></param>
        public PropertyListComparer(bool isAsc, params string[] fields)
        {
            IsAscending = isAsc;
            _fields = fields;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fields"></param>
        public PropertyListComparer(params string[] fields)
            : this(true, fields)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            if (_fields == null || _fields.Length == 0)
            {
                throw new ApplicationException(
                    "There are no fields to compare to.");
            }

            object o1 = x;
            object o2 = y;

            ///
            /// swap for Desc
            ///
            if (!IsAscending)
            {
                object temp = o1;
                o1 = o2;
                o2 = temp;
            }

            int comparison = 0;
            for (int i = 0; i < _fields.Length; i++)
            {
                string f = _fields[i];
                IComparable prop1 = GetProperty(f, o1);
                IComparable prop2 = GetProperty(f, o2);
                comparison = prop1.CompareTo(prop2);
                if (comparison != 0)
                    return comparison;
            }

            return comparison;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="field"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        IComparable GetProperty(string field, object obj)
        {
            Type type = obj.GetType();
            PropertyInfo prop = type.GetProperty(field);
            if (prop == null)
            {
                throw new ApplicationException(
                    "Property does not exist on object. Property : "
                    + field + " Object Type : " + type.FullName);
            }

            object value = prop.GetValue(obj, null);
            if (!(value is IComparable))
            {
                throw new ApplicationException(
                    "Cannot compare properties that are not of type " +
                    "IComparable. Property : " + field + " Object Type : " + 
                    type.FullName);
            }

            return (IComparable)value;
        }
    }
}


