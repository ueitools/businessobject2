using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace BusinessObject
{
    [Serializable]
    public class BindingListEx<T> : BindingList<T>
    {
        public BindingListEx()
            : base()
        {
        }

        public BindingListEx(IList<T> list)
            : base(list)
        {
        }

        #region SortSupport
        [NonSerialized]
        protected PropertyDescriptor m_sortProperty;
        protected ListSortDirection m_sortDirection;

        /// <summary>
        /// 
        /// </summary>
        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        protected override ListSortDirection  SortDirectionCore
        {
            get { return m_sortDirection; }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get { return m_sortProperty; }
        }

        protected override void ApplySortCore(PropertyDescriptor prop, 
            ListSortDirection direction)
        {
            m_sortProperty = prop;
            m_sortDirection = direction;

            Sort();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Sort()
        {
            List<T> items = this.Items as List<T>;
            PropertyComparer<T> pc = new 
                PropertyComparer<T>(m_sortProperty, m_sortDirection);

            items.Sort(pc);
        }

        /// <summary>
        /// Multiple Properties Sort 
        /// </summary>
        /// <param name="pls"></param>
        public void Sort(bool isAsc, params string[] fields)
        {
            PropertyListComparer<T> pls = new PropertyListComparer<T>
                (isAsc, fields);

            List<T> items = this.Items as List<T>;
            items.Sort(pls);
        }

        /// <summary>
        /// using List<T>'s IComparable (CompareTo method)
        /// </summary>
        public void SortByDefault()
        {
            List<T> items = this.Items as List<T>;
            items.Sort();
        }

        #endregion

        #region FindSupport
        protected override bool SupportsSearchingCore
        {
            get { return true; }
        }

        protected override int FindCore(PropertyDescriptor prop, object key)
        {
            if(prop == null)
                return -1;

            List<T> items = this.Items as List<T>;
            foreach (T item in items)
            {
                string value = (string)prop.GetValue(item);
                if ((string)key == value)
                    return IndexOf(item);
            }

            return -1;
        }
        #endregion

        #region Comparison
        /// <summary>
        /// need to override if T is value type like integer
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            BindingListEx<T> list = (BindingListEx<T>)obj;
            if (Count != list.Count)
                return false;

            for (int i = 0; i < this.Count; i++)
            {
                if (!Object.Equals(this[i], list[i]))
                    return false;
            }

            return true;
        }
        #endregion

        #region Bag Operation
        /// <summary>
        /// contains will use Equals as default
        /// This is inefficient for large list (sequential comparison)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public IList<T> Difference(IList<T> list)
        {
            Type type = list.GetType();
            IList<T> diffList = Activator.CreateInstance(type)
                as IList<T>;

            foreach (T item in this)
            {
                if (list.Contains(item) == false)
                    diffList.Add(item);
            }

            return diffList;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        public void Append(IList<T> list)
        {
            foreach (T item in list)
                this.Add(item);
        }
    }
}
