using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Xml;
using System.IO;

namespace BusinessObject
{
    /// <summary>
    /// S   Shadow ID, not for display
    /// M   Has been Modified
    /// Q   Ready for Check-In
    /// E   Can't check in, waiting for exec changes
    /// P   Can't check in, Misc. hold flag
    /// </summary>
    public static class StatusFlag
    {
        public const string SHADOW          = "S"; 
        public const string MODIFIED        = "M"; 
        public const string PASSED_QA       = "Q"; 
        public const string EXEC_HOLD       = "E"; 
        public const string PROJECT_HOLD    = "P"; 
    }

    /// <summary>
    /// 
    /// </summary>
    public static class BooleanFlag
    {
        public const string YES = "Y";
        public const string NO = "N";
    }

    public static class Branch
    {
        public const string EUBRANCH = "EU";
        public const string OHBRANCH = "OH";
        public const string CABRANCH = "CA";
        public const string CYBRANCH = "CY"; // CY is Cypress
        public const string TMBRANCH = "TM"; // temporary
        public const string BLBRANCH = "BL"; // Bangalore, India
    }

    /// <summary>
    /// 
    /// </summary>
    public static class DBConnectionString
    {
        public static string LOADREQUEST { get { return WorkflowConfig.GetItem("LoadRequest"); } }
        public static string UEIPUBLIC      { get { return WorkflowConfig.GetItem("UEIPublic"); } }
        public static string UEITEMP        { get { return WorkflowConfig.GetItem("UEITemp"); } }
        public static string UEITEMP_INDIA  { get { return WorkflowConfig.GetItem("UEITemp_India"); } }
        public static string PRODUCT        { get { return WorkflowConfig.GetItem("Product"); } }
        public static string WORKPRODUCT    { get { return WorkflowConfig.GetItem("WorkProduct"); } }
        public static string LOCALPRODUCT   { get { return WorkflowConfig.GetItem("LocalProduct"); } }
        public static string FAST4_1        { get { return WorkflowConfig.GetItem("FAST4_1"); } }
        public static string FAST4_2        { get { return WorkflowConfig.GetItem("FAST4_2"); } }
        public static string FAST4_3        { get { return WorkflowConfig.GetItem("FAST4_3"); } }
        public static string IDRANKING      { get { return WorkflowConfig.GetItem("IdRanking"); } }
        public static string EdId           { get { return WorkflowConfig.GetItem("EdId"); } }        
        public static string ServiceProvider { get { return WorkflowConfig.GetItem("ServiceProvider"); } }
        public static string PRODUCT1       { get { return WorkflowConfig.GetItem("Product1"); } }
        public static string WORKPRODUCT1   { get { return WorkflowConfig.GetItem("WorkProduct1"); } }
        public static string LOCALPRODUCT1  { get { return WorkflowConfig.GetItem("LocalProduct1"); } }
        public static string QuickSet       { get { return WorkflowConfig.GetItem("QuickSet"); } }        
    }
}