using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using BusinessObject.LockStatus;
using UEIException;

namespace BusinessObject
{
    public static class DBFunctions
    {
        public static bool DoesExist(string id)
        {
            return DAOFactory.IDHeader().DoesExist(id);
        }

        public static bool DoesExist(int tn)
        {
            return DAOFactory.TNHeader().DoesExist(tn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IntegerCollection GetTNList(string id)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);

            return DAOFactory.AdHoc().SelectInteger(
                "AdHoc.GetTNList", paramList);
        }

        public static IntegerCollection GetTNList(IList<string> idList)
        {
            IntegerCollection tnList = new IntegerCollection();
            foreach (string id in idList)
            {
                IntegerCollection list = DBFunctions.GetTNList(id);
                foreach (int tn in list)
                {
                    if (tnList.Contains(tn) != true)
                    {
                        tnList.Add(tn);
                    }
                }
            }

            return tnList;
        }

        /// <summary>
        /// 1. Get a list of all IDs in the database except shadow IDs
        /// 2. Trasaction Isolation Level: Read UnCommited to 
        ///    minimize bloking (table wise locking)
        /// </summary>
        /// <returns></returns>
        public static StringCollection NonBlockingGetAllIDList()
        {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC)
                return GetCheckedOutIds();

            StringCollection list = new StringCollection();

            try
            {
                DAOFactory.BeginTransaction(IsolationLevel.ReadUncommitted);
                list = DAOFactory.AdHoc().SelectString(
                    "AdHoc.GetIDList", null);
                DAOFactory.CommitTransaction();
            }
            catch (Exception)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }

            return list;
        }

        public static IntegerCollection NonBlockingGetAllTNList()
        {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC)
                return GetCheckedOutTNs();
            IntegerCollection list = new IntegerCollection();
            try
            {
                DAOFactory.BeginTransaction(IsolationLevel.ReadUncommitted);
                list = DAOFactory.AdHoc().SelectInteger("AdHoc.GetTNList", null);
                DAOFactory.CommitTransaction();
            }
            catch (Exception)
            {
                DAOFactory.RollBackTransaction();
                throw;
            }
            return list;
        }

        public static StringCollection GetAllIDList()
        {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC)
                return GetCheckedOutIds();

            return DAOFactory.AdHoc().SelectString("AdHoc.GetIDList", null);
        }

        public static IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param)
        {
            return AdHocDAO.mapper.QueryForList("AdHoc.GetIDListByRegionCountry", m_param) as IDBrandResultCollection;
        }
        public static StringCollection GetIDList(int tn)
        {

            Hashtable paramList = new Hashtable();
            paramList.Add("TN", tn);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDListByTN", paramList);
            Result = IntersectWithCheckoutDB(Result);

            return Result;
        }

        public static StringCollection GetIDList(string mode)
        {
            if (mode == "")
                return GetAllIDList();
            Hashtable paramList = new Hashtable();
            paramList.Add("Mode", mode);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDList", paramList);
            Result = IntersectWithCheckoutDB(Result);
            return Result;
        }

        /// <summary>
        /// Gets the IDList based on the modes or Id range passed through the IDSearchParameter.
        /// </summary>
        /// <remarks>Created on 12/10/2010 by binil. Executes UEI2_SP_GetModeIDList procedure</remarks>
        /// <param name="searchParam"></param>
        /// <returns></returns>
        public static IList<string> GetIDList(IDSearchParameter searchParam)
        {
            //return DAOFactory.AdHoc().GetIDList(searchParam);
            return DAOFactory.AdHoc().ExecuteStatement("AdHoc.GetModeIDList", searchParam);

        }

        /// <summary>
        /// Gets all the modes in database.
        /// </summary>
        /// <remarks>Added on 18/10/2010 by binil.</remarks>
        /// <returns></returns>
        public static IList<string> GetModelList()
        {
            return DAOFactory.AdHoc().ExecuteStatement("AdHoc.GetModeList", null);
        }

        public static StringCollection GetIDListByExecutor(int executor)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("Executor_Code", executor);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDList", paramList);
            Result = IntersectWithCheckoutDB(Result);
            return Result;
        }

        public static StringCollection GetIDListByExecutor(string mode, int executor)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("Executor_Code", executor);
            paramList.Add("Mode", mode);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDList", paramList);
            Result = IntersectWithCheckoutDB(Result);
            return Result;
        }

        public static StringCollection GetIDListByExecutorList(
            List<int> executorList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ExecutorList", executorList);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDListByExecutorList", paramList);
            Result = IntersectWithCheckoutDB(Result);
            return Result;
        }

        public static StringCollection GetIDListByExecutorList(
            StringCollection modeList, List<int> executorList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ExecutorList", executorList);
            paramList.Add("ModeList", modeList);

            StringCollection Result =
                DAOFactory.AdHoc().SelectString("AdHoc.GetIDListByExecutorList", paramList);
            Result = IntersectWithCheckoutDB(Result);
            return Result;
        }

        public static StringCollection GetIDListByBrandList(
            StringCollection brandList, StringCollection modeList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("BrandList", brandList);
            paramList.Add("ModeList", modeList);

            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetIDListByBrandList", paramList);
        }

        public static HashtableCollection GetBrandIDListByModelCount(
            StringCollection modeList, StringCollection regionList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ModeList", modeList);
            paramList.Add("RegionList", regionList);

            HashtableCollection res = DAOFactory.AdHoc().Select(
                "AdHoc.GetBrandIDListByModelCount", paramList);
            return res;
        }

        public static StringCollection GetIDListByRegionList(
            StringCollection regionList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("RegionList", regionList);

            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetIDListByRegionList", paramList);
        }

        public static StringCollection GetIDListByRegionList(
            StringCollection regionList, StringCollection modeList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("RegionList", regionList);
            paramList.Add("ModeList", modeList);

            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetIDListByRegionList", paramList);
        }

        public static StringCollection GetIDListByBrandList(
            StringCollection brandList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("BrandList", brandList);

            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetIDListByBrandList", paramList);
        }

        /// <summary>
        /// check if more than 1 ID are related to TN 
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public static bool IsTNShared(int tn)
        {
            StringCollection idListForTN = DBFunctions.GetIDList(tn);
            StringCollection allIDList = DBFunctions.GetAllIDList();

            int shareCount = 0;
            foreach (string id in idListForTN)
            {
                if (allIDList.Contains(id))
                    shareCount += 1;
            }

            return (shareCount > 1);
        }

        public static StringCollection GetDeviceGroupList()
        {
            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetDeviceGroup", null);
        }

        public static StringCollection GetModeListForDeviceGroup(
            string deviceGroup)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("DeviceGroup", deviceGroup);

            return DAOFactory.AdHoc().SelectString(
               "AdHoc.GetMode", paramList);
        }

        public static IntegerCollection GetAvailableCodeList(
            string deviceGroup)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("DeviceGroup", deviceGroup);

            return DAOFactory.AdHoc().SelectInteger(
                "AdHoc.GetAvailableCode", paramList);
        }

        public static StringCollection GetRestrictedIDList()
        {
            return DAOFactory.AdHoc().SelectString(
                "AdHoc.GetRestrictedIDList", null);
        }
        public static BrandCollection GetAllBrandList()
        {
            return DAOFactory.Misc().GetAllBrandList();
        }

        public static OpInfoCollection GetAllOpInfoList()
        {
            return DAOFactory.Misc().GetAllOpInfoList();
        }

        public static DeviceTypeCollection GetAllDeviceTypeList()
        {
            return DAOFactory.Misc().GetAllDeviceTypeList();
        }

        public static CountryCollection GetAllCountryList()
        {
            return DAOFactory.Misc().GetAllCountryList();
        }

        //Added By Satyadeep on March 29 2012
        public static HashtableCollection GetAllBrands(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllBrands(param);
        }

        //Added By Satyadeep on Sep 02 2010
        //Gives List of All Region List
        public static CountryCollection GetAllRegionList()
        {
            return DAOFactory.Misc().GetAllRegionList();
        }
        public static CountryCollection GetLocationList()
        {
            return DAOFactory.Misc().GetLocationList();
        }
        //Added By Satyadeep on April 04 2012
        //Gives List of All Sub Regions
        public static CountryCollection GetAllSubRegionList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllSubRegionList(param);
        }
        //Added by Satyadeep on Sep 02 2010
        //Gives List of Country List based on Regions        
        public static CountryCollection GetAllCountryList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllCountryList(param);
        }
        //add method here to get complete device type.Added by binil on may 09 ,2012
        public static DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetCompleteDevices(param);
        }

        //Added by Satyadeep on Sep 02 2010
        //gives List of all device types based on Region and Country
        public static DeviceTypeCollection GetAllDeviceTypeList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllDeviceTypeList(param);
        }
        //Added by Satyadeep on 19/04/2012
        //gives list of Sub Devices for selected devices
        public static DeviceTypeCollection GetAllSubDeviceTypeList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllSubDeviceTypeList(param);
        }
        //Added by Satyadeep on 19/04/2012
        //Gives list of Components for selected devices and sub devices
        public static DeviceTypeCollection GetAllComponentList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllComponentList(param);
        }
        //Added By Satyadeep on Sep 02 2010
        //Gives List of DataSources
        public static StringCollection GetAllDataSources()
        {
            return DAOFactory.Misc().GetDataSourceLocation();
        }



        public static Country Redirect(Country country)
        {
            int code = DAOFactory.Misc().GetCountryCode(country);
            return DAOFactory.Misc().GetCountry(code);
        }

        public static HashtableCollection GetAllModeNames(string mode, string language)
        {
            Hashtable param = new Hashtable();

            switch (language)
            {
                case "French":
                    param.Add("SelectTable", "mode_lst_french");
                    break;
                case "German":
                    param.Add("SelectTable", "mode_lst_german");
                    break;
                default:
                    param.Add("SelectTable", "mode_lst_english");
                    break;
            }
            if (mode != null)
                param.Add("mode", mode);
            return DAOFactory.AdHoc().Select("AdHoc.GetAllModeNames", param);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public static NumericValueLabelPairCollection
            GetNumericValueForLabel(StringCollection labelList)
        {
            return DAOFactory.ReadOnly().GetNumericValueForLabel(labelList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        public static StringCollection GetLabelListForID(StringCollection idList)
        {
            return DAOFactory.Function().SelectLabelForFunc(idList);
        }

        public static DeviceCollection GetDeviceCollectionWithBrandModelExact(
            string mode, int? deviceTypeCode, string brand, string model, List<string> market, string branch)
        {
            DeviceCollection result = DAOFactory.DeviceListDAO().SelectDeviceListByBrandModelExact(
                mode, deviceTypeCode, brand, model, market, branch);

            if (result == null)
                return new DeviceCollection();
            else
                return result;
        }

        public static DeviceCollection GetDeviceCollectionWithBrand(
            string mode, int? deviceTypeCode, string brand, List<string> market, string branch)
        {
            DeviceCollection result = DAOFactory.DeviceListDAO().SelectDeviceListByBrandModelExact(
                mode, deviceTypeCode, brand, null, market, branch);

            if (result == null)
                return new DeviceCollection();
            else
                return result;
        }

        private static StringCollection GetCheckedOutIds()
        {
            string Domain = GetDBDomain();
            StringCollection list = new StringCollection();
            LockStatusService lockService = new LockStatusService();
            IdLock[] AllLocks = lockService.GetLockedIDs(Domain);
            foreach (IdLock l in AllLocks)
                list.Add(l.ID);
            return list;
        }

        private static IntegerCollection GetCheckedOutTNs()
        {
            string Domain = GetDBDomain();
            IntegerCollection list = new IntegerCollection();
            LockStatusService lockService = new LockStatusService();
            TnLock[] AllLocks = lockService.GetLockedTNs(Domain);
            foreach (TnLock l in AllLocks)
                list.Add(l.TN);
            return list;
        }

        private static StringCollection IntersectWithCheckoutDB(StringCollection Result)
        {
            if (DAOFactory.GetDBConnectionString().ToUpper() != DBConnectionString.UEIPUBLIC.ToUpper())
            {
                StringCollection CheckedOutList = GetCheckedOutIds();
                StringCollection Reduced = new StringCollection();
                foreach (string id in Result)
                {
                    if (CheckedOutList.Contains(id))
                        Reduced.Add(id);
                }
                Result = Reduced;
            }
            return Result;
        }

        public static string GetDBDomain()
        {
            string Domain = SystemInformation.UserDomainName;

            if (DAOFactory.GetDBConnectionString() == DBConnectionString.UEITEMP_INDIA)
                Domain = WorkflowConfig.GetItem("BangaloreDomain");

            return Domain;
        }

        public static void LockIdsAndTns(List<string> idList, List<int> tnList, bool _ignoreTnLock)
        {
            LockStatusService lockStatus = new LockStatusService();
            int lockTNStatus = lockStatus.BulkLockIdTn(idList.ToArray(), tnList.ToArray(), _ignoreTnLock,
                                                       Environment.UserDomainName, Environment.UserName);
            if (lockTNStatus != 0)
            {
                throw new LockException(string.Format("Error locking multiple IDs and TNs.\n\n{0}", lockStatus.GetMessage(lockTNStatus)));
            }

        }

        public static string GetTimeAndVersionOfConfig()
        {
            return WorkflowConfig.GetTimeStamp() + ":" + WorkflowConfig.GetItem("Version") + ":dll03";
        }

        public static HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag)
        {
            return DAOFactory.Misc().GetChangedIds(strStartDate, strEndDate, intStatusFlag);
        }

        #region ID/BrandReport-DBM
        public static IDBrandResultCollection GetIDBrandResults(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetIDBrandRecords(param);
        }
        #endregion

        #region BSC ID List Report
        public static IDBrandResultCollection GetAllBSCIDList(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetAllBSCIDList(param);
        }
        #endregion

        #region Model Info Report
        public static IDBrandResultCollection GetModelInformation(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetModelInformation(param);
        }
        #endregion

        #region ExecIDTnInfo
        //Created on 23/02/2011 by binil

        public static IList<Int32> GetExecutorCodes()
        {
            return DAOFactory.Misc().SelectExecutorCodes();

        }

        public static PrefixCollection GetPrefixColllection(string id)
        {
            return DAOFactory.IDHeader().SelectPrefix(id);
        }
        public static IList SelectPrefix(String id)
        {
            IList list = DAO.mapper.QueryForList("Prefix.Select", id);
            return list;
        }
        public static IList IDFunctionSelect(String id)
        {
            IList list = DAO.mapper.QueryForList("ReadOnly.IDFunctionSelect", id);
            return list;
        }
        public static IDBrandResultCollection GetModelInfoById(string id)
        {
            return DAOFactory.Misc().GetModelInformation(id);
        }
        #endregion

        #region Empty/NonEmptyIds Report
        //Added on May 19.

        public static List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param)
        {
            try
            {
                return DAOFactory.AdHoc().ExecuteStatement("AdHoc.FilterEmptyNonEmptyIDs", param) as List<String>;
            }
            catch (Exception ex)
            {
                ex.Data.Add("FilterEmptyNonEmptyIDs", param);
                throw;
            }
        }

        #endregion

        #region LabelIntronSearch
        public static FunctionCollection IntronLabelSearch(IDSearchParameter param)
        {
            try
            {
                return DAOFactory.Function().IntronLabelSearch(param);
            }
            catch (Exception ex)
            {
                ex.Data.Add("LabelIntronSearch", param);
                throw;
            }
        }
        public static FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param)
        {
            try
            {
                return DAOFactory.Function().ExcludeIntronLabelSearch(param);
            }
            catch (Exception ex)
            {
                ex.Data.Add("LabelIntronSearch", param);
                throw;
            }
        }

        public static HashtableCollection GetIntronLabelDictionary()
        {
            try
            {
                return DAOFactory.Misc().SelectAll("Intron");
            }
            catch (Exception ex)
            {
                ex.Data.Add("GetIntronLabelDictionary", null);
                throw ex;
            }
        }
        #endregion

        #region Brand Search List
        public static IDBrandResultCollection GetBrandSearchList(IDSearchParameter param)
        {
            //Using BSC Procedure temporaryly
            return DAOFactory.Misc().GetBrandSearchList(param);
        }
        #endregion

        #region Key Template Editor
        public static HashtableCollection GetKeyTemplate()
        {
            try
            {
                return DAOFactory.Misc().GetKeyTemplate();
            }
            catch
            {
                throw;
            }
        }
        public static HashtableCollection GetKeyLabelIntrons()
        {
            try
            {
                return DAOFactory.Misc().GetKeyLabelIntrons();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region ConsolidatedReports

        public static int TotalMainDevices()
        {
            return DAOFactory.Misc().TotalMainDevices();
        }

        public static int TotalSubDevices()
        {
            return DAOFactory.Misc().TotalSubDevices();
        }
        public static int TotalDeviceTypes()
        {
            return DAOFactory.Misc().TotalDeviceTypes();
        }
        public static int TotalDeviceModels()
        {
            return DAOFactory.Misc().TotalDeviceModels();
        }
        public static int TotalRemoteModels()
        {
            return DAOFactory.Misc().TotalRemoteModels();
        }
        public static int TotalKeyFunctions()
        {
            return DAOFactory.Misc().TotalKeyFunctions();
        }
        public static int TotalKeyCount()
        {
            return DAOFactory.Misc().TotalKeyCount();
        }
        public static int TotalExecutors()
        {
            return DAOFactory.Misc().TotalExecutors();
        }
        public static int TotalUsedExecutors()
        {
            return DAOFactory.Misc().TotalUsedExecutors();
        }
        public static HashtableCollection TotalBrandCounts()
        {
            return DAOFactory.Misc().TotalBrandCounts();

        }

        #endregion

        #region Exec/IdMap

        public static HashtableCollection GetExecIdMap(IList<string> idList)
        {
            try
            {
                return DAOFactory.Misc().GetExecIdMap(idList);
            }
            catch (Exception ex)
            {
                ex.Data.Add("GetExecIdMap", null);
                throw;
            }
        }
        #endregion

        public static string GetAirconDescriptorData(string id, string dataType, int version)
        {
            return DAOFactory.Misc().GetAirConDescriptorData(id, dataType, version);
        }
    }

    public class DbFunctionsWrapper : IDbFunctionsWrapper
    {
        public HashtableCollection GetKeyTemplate()
        {
            return DBFunctions.GetKeyTemplate();
        }
        public HashtableCollection GetKeyLabelIntrons()
        {
            return DBFunctions.GetKeyLabelIntrons();
        }
        public IntegerCollection GetAllNonBlockingTnList()
        {
            return DBFunctions.NonBlockingGetAllTNList();
        }
        public StringCollection NonBlockingGetAllIDList()
        {
            return (DBFunctions.NonBlockingGetAllIDList());
        }
        public IDHeader GetHeaderData(String id)
        {
            return (DAOFactory.IDHeader().Select(id));
        }
        public IntegerCollection GetTNList(String id)
        {
            return (DBFunctions.GetTNList(id));
        }
        public TNHeader GetTnStatus(Int32 tn)
        {
            return (DAOFactory.TNHeader().Select(tn));
        }
        public HashtableCollection GetAllModeNames(String mode, String language)
        {
            return (DBFunctions.GetAllModeNames(mode, language));
        }
        public IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param)
        {
            return DBFunctions.GetIDByRegionResults(m_param);
        }
        public StringCollection GetAllIDList()
        {
            return DBFunctions.GetAllIDList();
        }
        public IList<string> GetAllIDList(IDSearchParameter parameter)
        {
            return DBFunctions.GetIDList(parameter);
        }
        public IList<string> GetModeList()
        {
            return DBFunctions.GetModelList();
        }
        public IDBrandResultCollection GetIDBrandResults(IDSearchParameter param)
        {
            return DBFunctions.GetIDBrandResults(param);
        }
        public IDBrandResultCollection GetAllBSCIDList(IDSearchParameter param)
        {
            return DBFunctions.GetAllBSCIDList(param);
        }
        public IDBrandResultCollection GetBrandSearchList(IDSearchParameter param)
        {
            return DBFunctions.GetBrandSearchList(param);
        }
        public IDBrandResultCollection GetModelInformation(IDSearchParameter param)
        {
            return DBFunctions.GetModelInformation(param);
        }
        public CountryCollection GetAllRegionList()
        {
            return DBFunctions.GetAllRegionList();
        }
        public CountryCollection GetLocationList()
        {
            return DBFunctions.GetLocationList();
        }
        public CountryCollection GetAllSubRegionList(IDSearchParameter param)
        {
            return DBFunctions.GetAllSubRegionList(param);
        }
        public CountryCollection GetAllCountryListByRegion(IDSearchParameter param)
        {
            return DBFunctions.GetAllCountryList(param);
        }
        public DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            return DBFunctions.GetCompleteDevices(param);
        }
        public DeviceTypeCollection GetAllDeviceTypeList(IDSearchParameter param)
        {
            return DBFunctions.GetAllDeviceTypeList(param);
        }
        public DeviceTypeCollection GetAllSubDeviceTypeList(IDSearchParameter param)
        {
            return DBFunctions.GetAllSubDeviceTypeList(param);
        }
        public DeviceTypeCollection GetAllComponentList(IDSearchParameter param)
        {
            return DBFunctions.GetAllComponentList(param);
        }
        public StringCollection GetAllDataSources()
        {
            return DBFunctions.GetAllDataSources();
        }
        public IList<Int32> GetExecutorCodes()
        {
            return DBFunctions.GetExecutorCodes();
        }
        public StringCollection GetIDListByExecutor(int executorCode)
        {
            return DBFunctions.GetIDListByExecutor(executorCode);
        }
        public PrefixCollection GetPrefixColllection(string id)
        {
            return DBFunctions.GetPrefixColllection(id);
        }
        public IList SelectPrefix(String id)
        {
            return DBFunctions.SelectPrefix(id);
        }
        public IList IDFunctionSelect(String id)
        {
            return DBFunctions.IDFunctionSelect(id);
        }
        public IDBrandResultCollection GetModelInfoById(string id)
        {
            return DBFunctions.GetModelInfoById(id);
        }
        public StringCollection GetRestrictedIDList()
        {
            return DBFunctions.GetRestrictedIDList();
        }
        /// <summary>
        /// Filters out the empty or non empty ids from the list passed
        /// Added on May 19,2011 by binil.
        /// </summary>
        /// <param name="param">
        /// Contains list of ids and a flag for filtering.
        /// </param>
        /// <returns>
        /// List of ids
        /// </returns>
        public List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param)
        {
            return DBFunctions.FilterEmptyNonEmptyIDs(param);
        }
        public HashtableCollection SelectUniqueFunctionIRCount()
        {
            return (DAOFactory.Function().SelectUniqueFunctionIRCount());
        }
        public Int32 SelectUniqueFuncCount(String mode)
        {
            return DAOFactory.Function().SelectUniqueFuncCount(mode);
        }
        public Int32 SelectUniqueIRCount(String mode)
        {
            return DAOFactory.Function().SelectUniqueIRCount(mode);
        }
        public IList FindDisconnectedTNs()
        {
            IList tnList = DAOFactory.Function().FindDisconnectedTNs();
            return tnList;
        }


        #region ExecIdMap


        public HashtableCollection GetExecIdMap(IList<string> idList)
        {
            return DBFunctions.GetExecIdMap(idList);
        }

        #endregion

        #region IDbFunctionsWrapper Members


        public IList<String> GetRemoteImages(Int32 tnNumber)
        {
            return DAOFactory.Misc().GetRemoteImages(tnNumber);
        }

        #endregion

        #region LabelIntronSearchReport Members


        public FunctionCollection IntronLabelSearch(IDSearchParameter param)
        {
            return DBFunctions.IntronLabelSearch(param);
        }
        public FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param)
        {
            return DBFunctions.ExcludeIntronLabelSearch(param);
        }

        public HashtableCollection GetIntronLabelDictionary()
        {
            return DBFunctions.GetIntronLabelDictionary();
        }
        #endregion

        #region Get All Standard, Alias and Variation Brands
        public HashtableCollection GetAllBrands(IDSearchParameter param)
        {
            return DBFunctions.GetAllBrands(param);
        }
        #endregion

        #region ConsolidateReports

        public int TotalMainDevices()
        {
            return DBFunctions.TotalMainDevices();
        }

        public int TotalSubDevices()
        {
            return DBFunctions.TotalSubDevices();

        }
        public int TotalDeviceTypes()
        {
            return DBFunctions.TotalDeviceTypes();

        }
        public int TotalDeviceModels()
        {
            return DBFunctions.TotalDeviceModels();

        }
        public int TotalRemoteModels()
        {
            return DBFunctions.TotalRemoteModels();

        }
        public int TotalKeyFunctions()
        {
            return DBFunctions.TotalKeyFunctions();
        }
        public int TotalKeyCount()
        {
            return DBFunctions.TotalKeyCount();

        }
        public int TotalExecutors()
        {
            return DBFunctions.TotalExecutors();
        }
        public int TotalUsedExecutors()
        {
            return DBFunctions.TotalUsedExecutors();
        }
        public HashtableCollection TotalBrandCounts()
        {
            return DBFunctions.TotalBrandCounts();

        }
        public HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag)
        {
            return GetUpdatedIds(strStartDate, strEndDate, intStatusFlag);
        }
        #endregion

        #region IDbFunctionsWrapper Members
        public HashtableCollection GetKeyFunction(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetKeyFunction(param);
        }
        #endregion

        #region IDbFunctionsWrapper Members
        public String UpdateEdIdData(String m_MainDevice, String m_SubDevice, String m_Component, String m_Brand, String m_Model, String m_Countries, Byte[] m_FileStructure, String m_Description, Int32 m_Flag, String Base64EDID, String CustomFingerPrint, String CustomFPPlusOSD, String Only128FP, String Only128FPPlusOSD, String OSDData, String OSDFP, String BytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3)
        {
            return DAOFactory.Misc().UpdateEdIdData(m_MainDevice, m_SubDevice, m_Component, m_Brand, m_Model, m_Countries, m_FileStructure, m_Description, m_Flag, Base64EDID, CustomFingerPrint, CustomFPPlusOSD, Only128FP, Only128FPPlusOSD, OSDData, OSDFP,BytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3);
        }
        public HashtableCollection GetEdIdData(IDSearchParameter param)
        {
            return DAOFactory.Misc().GetEdIdData(param);
        }
        #endregion

        #region Supported Ids by Platform
        public IList<Hashtable> GetAllSupportedPlatforms()
        {
            return DAOFactory.Misc().GetAllSupportedPlatforms();
        }
        public IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<String> platforms)
        {
            return DAOFactory.Misc().GetAllSupportedIdsForPlatforms(platforms);
        }
        #endregion

        #region EdID XBox
        public IList<Hashtable> GetXBoxEdIDData()
        {
            return DAOFactory.Misc().GetXBoxEdIDData();
        }
        public IList<Hashtable> GetXBoxEdIDDataEx()
        {
            return DAOFactory.Misc().GetXBoxEdIDDataEx();
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String edidVersion)
        {
            return DAOFactory.Misc().UpdateEdIdXBoxData(edidRID, m_FileStructure, edidVersion);
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String customFingerPrint, String customFPPlusOSD, String str128FP, String str128FPPlusOSD, String osdData, String osdFP, String bytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress)
        {
            return DAOFactory.Misc().UpdateEdIdXBoxData(edidRID, m_FileStructure, customFingerPrint, customFPPlusOSD, str128FP, str128FPPlusOSD, osdData, osdFP, bytePosition,strCustomFingerPrint0,strCustomFingerPrint1,strCustomFingerPrint2,strCustomFingerPrint3,strPhysicalAddress);
        }
        #endregion


       
    }

    public interface IDbFunctionsWrapper
    {
        HashtableCollection GetKeyTemplate();
        HashtableCollection GetKeyLabelIntrons();
        IntegerCollection GetAllNonBlockingTnList();
        StringCollection GetAllIDList();
        IList<string> GetModeList();
        CountryCollection GetLocationList();
        CountryCollection GetAllRegionList();
        CountryCollection GetAllSubRegionList(IDSearchParameter param);
        CountryCollection GetAllCountryListByRegion(IDSearchParameter param);
        DeviceTypeCollection GetCompleteDevices(IDSearchParameter param);
        DeviceTypeCollection GetAllDeviceTypeList(IDSearchParameter param);
        DeviceTypeCollection GetAllSubDeviceTypeList(IDSearchParameter param);
        DeviceTypeCollection GetAllComponentList(IDSearchParameter param);
        StringCollection GetAllDataSources();
        IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param);
        IDBrandResultCollection GetIDBrandResults(IDSearchParameter param);
        IDBrandResultCollection GetAllBSCIDList(IDSearchParameter param);
        IDBrandResultCollection GetModelInformation(IDSearchParameter param);
        IList<String> GetAllIDList(IDSearchParameter parameter);
        HashtableCollection GetAllModeNames(String mode, String language);
        IList<Int32> GetExecutorCodes();
        StringCollection GetIDListByExecutor(int executorCode);
        PrefixCollection GetPrefixColllection(string id);
        IList SelectPrefix(String id);
        IList IDFunctionSelect(String id);
        IDBrandResultCollection GetModelInfoById(string id);
        StringCollection GetRestrictedIDList();
        StringCollection NonBlockingGetAllIDList();
        IDHeader GetHeaderData(String id);
        IntegerCollection GetTNList(String id);
        TNHeader GetTnStatus(Int32 tn);
        List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param);
        Int32 SelectUniqueFuncCount(String mode);
        Int32 SelectUniqueIRCount(String mode);
        HashtableCollection SelectUniqueFunctionIRCount();
        IList FindDisconnectedTNs();
        HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag);
        IList<String> GetRemoteImages(Int32 tnNumber);
        FunctionCollection IntronLabelSearch(IDSearchParameter param);//Label/Intron search report
        FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param);
        HashtableCollection GetIntronLabelDictionary();
        IDBrandResultCollection GetBrandSearchList(IDSearchParameter param);
        HashtableCollection GetAllBrands(IDSearchParameter param);
        HashtableCollection GetExecIdMap(IList<string> idList);
        HashtableCollection GetKeyFunction(IDSearchParameter param);
        #region ConsolidateReports
        int TotalMainDevices();
        int TotalSubDevices();
        int TotalDeviceTypes();
        int TotalDeviceModels();
        int TotalRemoteModels();
        int TotalKeyFunctions();
        int TotalKeyCount();
        int TotalExecutors();
        int TotalUsedExecutors();
        HashtableCollection TotalBrandCounts();
        #endregion

        #region Supported ID by Platforms
        IList<Hashtable> GetAllSupportedPlatforms();
        IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<String> platforms);
        #endregion

        #region EdId
        String UpdateEdIdData(String m_MainDevice, String m_SubDevice, String m_Component, String m_Brand, String m_Model, String m_Countries, Byte[] m_FileStructure, String m_Description, Int32 m_Flag, String Base64EDID, String CustomFingerPrint, String CustomFPPlusOSD, String Only128FP, String Only128FPPlusOSD, String OSDData, String OSDFP, String BytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3);
        HashtableCollection GetEdIdData(IDSearchParameter param);
        IList<Hashtable> GetXBoxEdIDData();
        IList<Hashtable> GetXBoxEdIDDataEx();
        String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String edidVersion);
        String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String customFingerPrint, String customFPPlusOSD, String str128FP, String str128FPPlusOSD, String osdData, String osdFP, String bytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress);
        #endregion
    }
}