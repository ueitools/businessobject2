using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace BusinessObject
{
    public static class XMLPersist
    {
        /// <summary>
        /// XML Serialization Routines
        /// </summary>
        /// <param name="fileName"></param>
        public static void Save(string fileName, Object obj)
        {
            XmlSerializer x = new XmlSerializer(obj.GetType());
            Stream s = File.Create(fileName);
            x.Serialize(s, obj);
            s.Close();
        }

        public static Object Load(string fileName, Type type)
        {
            XmlSerializer x = new XmlSerializer(type);
            Stream s = File.OpenRead(fileName);
            Object obj = x.Deserialize(s);
            s.Close();

            return obj;
        }
    }
}
