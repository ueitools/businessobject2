using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;

namespace BusinessObject
{
    public class RefereceEnforcer
    {
        /// <summary>
        /// insert shadow ID (just for foreign key constraint)
        /// with status flag as 'S' 
        /// 
        /// DoesExistNoFilter also returns id with 'S' flag
        /// </summary>
        /// <param name="id"></param>
        public static void InsertShadow(string id)
        {
            if (id != null &&
                DAOFactory.IDHeader().DoesExistNoFilter(id) != true)
            {
                IDHeader header = new IDHeader();
                header.ID = id;
                header.Status = StatusFlag.SHADOW;

                DAOFactory.IDHeader().Insert(header);
            }
        }

        public static void InsertShadow(int tn)
        {
            if (tn > 0 &&
                DAOFactory.TNHeader().DoesExistNoFilter(tn) != true)
            {
                TNHeader header = new TNHeader();
                header.TN = tn;
                header.Status = StatusFlag.SHADOW;

                DAOFactory.TNHeader().Insert(header);
            }
        }
    }
}
