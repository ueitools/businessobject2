using System;
using System.Collections.Generic;
using System.Text;
using System.IO; 

namespace BusinessObject
{
    /// <summary>
    /// copied from google newsgroup
    /// for serialized xml file comparison (ID or TN)
    /// </summary>
    public static class FileComparer
    {
        private const int BufferSize = 32768;

        public static bool Compare(string file1, string file2)
        {
            if (new FileInfo(file1).Length != new FileInfo(file2).Length)
            {
                return false;
            }

            using (FileStream s1 = new FileStream(file1, FileMode.Open),
                   s2 = new FileStream(file2, FileMode.Open))
            {
                return Compare(s1, s2);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        private static bool Compare(Stream s1, Stream s2)
        {
            byte[] buffer1 = new byte[BufferSize];
            byte[] buffer2 = new byte[BufferSize];

            int buffer1Remaining = 0;
            int buffer2Remaining = 0;
            int buffer1Index = 0;
            int buffer2Index = 0;

            while (true)
            {
                if (buffer1Remaining == 0)
                {
                    buffer1Index = 0;
                    buffer1Remaining = s1.Read(buffer1, 0, BufferSize);
                }
                if (buffer2Remaining == 0)
                {
                    buffer2Index = 0;
                    buffer2Remaining = s2.Read(buffer2, 0, BufferSize);
                }

                // End of both streams simultaneously 
                if (buffer1Remaining == 0 && buffer2Remaining == 0)
                {
                    return true;
                }
                // One stream ended before the other 
                if (buffer1Remaining == 0 || buffer2Remaining == 0)
                {
                    return false;
                }

                int compareSize = Math.Min(buffer1Remaining,
                                           buffer2Remaining);
                for (int i = 0; i < compareSize; i++)
                {
                    if (buffer1[buffer1Index] != buffer2[buffer2Index])
                    {
                        return false;
                    }
                    buffer1Index++;
                    buffer2Index++;
                }

                buffer1Remaining -= compareSize;
                buffer2Remaining -= compareSize;
            }
        }
    }
}
