using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    class KeyDAO : DAO
    {
        #region Singleton Instance
        private static volatile KeyDAO _keyDAO = null;

        public static KeyDAO GetInstance()
        {
            if (_keyDAO == null)
            {
                lock (typeof(KeyDAO))
                {
                    if (_keyDAO == null) // double-check
                        _keyDAO = new KeyDAO();
                }
            }
                
            return _keyDAO;
        }
        #endregion

        /// <summary>
        /// using rid populate deviceInfo for device key
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public KeyCollection Load(string projectName)
        {
            return Load(projectName, true);
        }
        /// <summary>
        /// using rid populate deviceInfo for device key
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="getPDLInfo"></param>
        /// <returns></returns>
        public KeyCollection Load(string projectName, bool getPDLInfo)
        {
            KeyCollection keyList = SelectKey(projectName);
            DeviceInfoCollection deviceInfoList = SelectDeviceInfo(projectName);
            PDLInfoCollection pdlInfoList = null;

            if (getPDLInfo)
            {
                pdlInfoList = SelectPDLInfo(projectName);
            }
            foreach (Key key in keyList)
            {
                DeviceInfo deviceInfo = deviceInfoList.Find(key.RID);
                if (deviceInfo != null)
                {
                    deviceInfo.KeyLabelList = SelectKeyGroup(projectName,
                        deviceInfo.DeviceNumber, keyList);
                    key.DeviceInfo = deviceInfo;
                }
                if (getPDLInfo)
                {
                    key.PDLInfoList = pdlInfoList.Find(key.RID);
                }
                key.KeyPropertyList = SelectKeyPropertyList(key.RID);
            }

            return keyList;
        }

        /// <summary>
        /// order of write is crucial
        /// 
        /// 1. insert non-device keys 
        /// 2. insert device keys
        /// </summary>
        /// <param name="keyList"></param>
        public void Save(KeyCollection keyList)
        {
            Insert(keyList);
            InsertDeviceInfo(keyList);
        }
        /// <summary>
        /// update the key
        /// </summary>
        /// <param name="key"></param>
        public void Update(Key key)
        {
            Mapper().Update("Key.Update", key);
        }
        /// <summary>
        /// non device key only
        /// </summary>
        /// <param name="keyList"></param>
        public void Insert(KeyCollection keyList)
        {
            //insert keyList
            int rid = (int)Mapper().QueryForObject("Key.GetNextRID", null);

            KeyTableGenerator keyTableGenerator = new KeyTableGenerator();

            List<string> tableList =
                keyTableGenerator.GetLocalTable(rid, keyList);
            
            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsert", table);

            foreach (Key key in keyList)
            {
                if (key.KeyPropertyList == null)
                    continue;

                foreach (KeyProperty keyProperty in key.KeyPropertyList)
                {
                    keyProperty.RID = key.RID;
                    InsertKeyProperty(keyProperty);
                }
            }

            //insert PDLInfoList
            int outronRid = (int)Mapper().QueryForObject(
                "Key.GetNextKeyOutronRID", null);

            KeyOutronTableGenerator keyOutronTableGenerator 
                = new KeyOutronTableGenerator();

            tableList = 
                keyOutronTableGenerator.GetLocalTable(outronRid, keyList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertPDLInfo", table);

            //insert IntronList
            OutronIntronTableGenerator outronIntronTableGenerator 
                = new OutronIntronTableGenerator();

            tableList =
                outronIntronTableGenerator.GetLocalTable(outronRid, keyList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertOutronIntron", table);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        void InsertDeviceInfo(KeyCollection keyList)
        {
            //insert deviceInfo
            ProductDeviceTableGenerator productDeviceTableGenerator 
                = new ProductDeviceTableGenerator();

            List<string> tableList =
                productDeviceTableGenerator.GetLocalTable(keyList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertDeviceInfo", table);

            InsertKeyGroup(keyList);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        public void InsertKeyGroup(KeyCollection keyList)
        {
            //insert KeyGroup
            KeyGroupTableGenerator keyGroupTableGenerator
                = new KeyGroupTableGenerator();

            List<string> tableList =
                keyGroupTableGenerator.GetLocalTable(keyList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertKeyGroup", table);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        public void InsertKeyGroupWithPositiveOrder(
            KeyCollection keyList)
        {
            List<int> orderList = new List<int>();

            foreach (Key key in keyList)
            {
                if (key.DeviceInfo == null)
                    continue;
                
                int order = (int)Mapper().QueryForObject(
                    "Key.GetNextOrder", key.RID);

                orderList.Add(order);
            }
            //insert KeyGroup
            KeyGroupTableGenerator keyGroupTableGenerator
                = new KeyGroupTableGenerator();

            List<string> tableList =
                keyGroupTableGenerator.GetLocalTable(keyList, 
                                                    orderList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertKeyGroup", table);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyProperty"></param>
        public void InsertKeyProperty(KeyProperty keyProperty)
        {
            Mapper().Insert("Key.InsertKeyProperty", keyProperty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceInfo"></param>
        public void UpdateDeviceInfo(DeviceInfo deviceInfo)
        {
            Mapper().Update("Key.UpdateDeviceInfo", deviceInfo);
        }

        /// <summary>
        /// This function might raise exception
        /// if a subkey label for a device can't be found 
        /// in key list
        /// </summary>
        /// <param name="deviceKey_RID"></param>
        /// <param name="keyLableList"></param>
        /// <param name="keyList"></param>
        void InsertKeyGroup(int deviceKey_RID, 
            StringCollection keyLableList, KeyCollection keyList)
        {
            int order = 0;
            foreach(string keyLabel in keyLableList)
            {
                Key key = keyList.Find(keyLabel);
                if(key == null)
                {
                    throw new ApplicationException(String.Format(
                        "Key with label {0} can't be found in key List",
                        keyLabel));
                }

                KeyGroup group = new KeyGroup();
                group.Device_RID = deviceKey_RID;
                group.Order = order;
                group.Key_RID = key.RID;

                Mapper().Insert("Key.InsertKeyGroup", group);
                order += 1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfoList"></param>
        void Insert(PDLInfoCollection pdlInfoList)
        {
            int rid = (int)Mapper().QueryForObject(
                "Key.GetNextKeyOutronRID", null);

            foreach (PDLInfo pdlInfo in pdlInfoList)
            {
                pdlInfo.RID = rid;
                Mapper().Insert("Key.InsertPDLInfo", pdlInfo);
                Insert(pdlInfo.RID, pdlInfo.IntronList);
                rid += 1;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfoList"></param>
        public void InsertPDLInfoList(PDLInfoCollection pdlInfoList)
        {
            //insert PDLInfoList
            int outronRid = (int)Mapper().QueryForObject(
                "Key.GetNextKeyOutronRID", null);

            KeyOutronTableGenerator keyOutronTableGenerator
                = new KeyOutronTableGenerator();

            List<string> tableList =
                keyOutronTableGenerator.GetLocalTable(outronRid,
                                                    pdlInfoList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertPDLInfo", table);

            //insert IntronList
            OutronIntronTableGenerator outronIntronTableGenerator
                = new OutronIntronTableGenerator();

            tableList =
                outronIntronTableGenerator.GetLocalTable(outronRid,
                                                         pdlInfoList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertOutronIntron", table);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyOutron_RID"></param>
        /// <param name="intronList"></param>
        void Insert(int keyOutron_RID, StringCollection intronList)
        {
            int order = 0;
            foreach(string intron in intronList)
            {
                OutronIntron outronIntron = new OutronIntron();
                outronIntron.KeyOutron_RID = keyOutron_RID;
                outronIntron.Order = order;
                outronIntron.Intron = intron;

                Mapper().Insert("Key.InsertOutronIntron", outronIntron);
                order += 1;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfo"></param>
        public void Update(PDLInfo pdlInfo)
        {
            Mapper().Update("Key.UpdatePDLInfo", pdlInfo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outronIntron"></param>
        public void Update(OutronIntron outronIntron)
        {
            Mapper().Update("Key.UpdateOutronIntron", outronIntron);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyList"></param>
        public void Delete(KeyCollection keyList)
        {
            foreach (Key key in keyList)
                Mapper().Delete("Key.Delete", key.RID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyProperty"></param>
        public void DeleteKeyProperty(KeyProperty keyProperty)
        {
            Mapper().Delete("Key.DeleteKeyProperty", keyProperty);
        }

        public DeviceInfoCollection SelectDeviceInfo(string projectName)
        {
            return Mapper().QueryForList("Key.SelectDeviceInfo", projectName)
                as DeviceInfoCollection;
        }

        public KeyCollection SelectKey(string projectName)
        {
            KeyCollection keyList =  Mapper().QueryForList("Key.SelectKey",
                projectName) as KeyCollection;

            if (keyList == null)
                return keyList;

            foreach (Key key in keyList)
            {
                key.KeyPropertyList = SelectKeyPropertyList(key.RID);
            }

            return keyList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="deviceNumber"></param>
        /// <param name="keyList"></param>
        /// <returns></returns>
        public StringCollection SelectKeyGroup(string projectName, 
            int deviceNumber, KeyCollection keyList)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("DeviceNumber", deviceNumber);

            IntegerCollection keyRIDList = Mapper().QueryForList(
                "Key.SelectKeyGroup", paramList) as IntegerCollection;

            StringCollection keyLabelList = new StringCollection();
            foreach (int rid in keyRIDList)
            {
                Key key = keyList.Find(rid);
                if(key != null && keyLabelList.Contains(key.Label) == false)
                    keyLabelList.Add(key.Label);
            }

            return keyLabelList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public PDLInfoCollection SelectPDLInfo(string projectName)
        {
            return SelectPDLInfo(projectName, true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public PDLInfoCollection SelectPDLInfo(string projectName, 
            bool getIntronList)
        {
            PDLInfoCollection pdlInfoList = Mapper().QueryForList(
                "Key.SelectPDLInfo", projectName) as PDLInfoCollection;

            OutronIntronCollection outronIntronList =
                SelectIntron(projectName);

            if (getIntronList)
            {
                foreach (PDLInfo info in pdlInfoList)
                    info.IntronList = outronIntronList.GetIntron(
                        info.RID);
            }

            return pdlInfoList;
        }

        OutronIntronCollection SelectIntron(string projectName)
        {
            return Mapper().QueryForList("Key.SelectIntron",
                projectName) as OutronIntronCollection;
        }

        KeyPropertyCollection SelectKeyPropertyList(int key_RID)
        {
            return Mapper().QueryForList("Key.SelectKeyProperty",
                key_RID) as KeyPropertyCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public KeyTypeCollection SelectKeyTypeList()
        {
            return Mapper().QueryForList("Key.SelectKeyType",
                null) as KeyTypeCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public KeyTypeCollection SelectSRIKeyTypeList()
        {
            return Mapper().QueryForList("Key.SelectSRIKeyType",
                null) as KeyTypeCollection;
        }
        /// <summary>
        /// delete the introns with keyOutron_RID
        /// </summary>
        /// <param name="keyOutron_RID"></param>
        public void DeleteIntronList(int keyOutron_RID)
        {
            Mapper().Delete("Key.DeleteIntronList", keyOutron_RID);
        }
        /// <summary>
        /// delete from keyGroup with Device_RID
        /// </summary>
        /// <param name="Device_RID"></param>
        public void DeleteKeyGroup(int Device_RID)
        {
            Mapper().Delete("Key.DeleteKeyGroup", Device_RID);
        }

        /// <summary>
        /// delete from keyGroup with Device_RID and Key_RID
        /// </summary>
        /// <param name="Device_RID"></param>
        public void DeleteKeyGroup(int Device_RID, int Key_RID)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("Device_RID", Device_RID);
            paramList.Add("Key_RID", Key_RID);

            Mapper().Delete("Key.DeleteKeyGroupByKey", paramList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pdlInfo"></param>
        public void DeletePDLInfo(PDLInfo pdlInfo)
        {
            Mapper().Delete("Key.DeletePDLInfo", pdlInfo);
        }
        /// <summary>
        /// insert the outronIntronList
        /// </summary>
        /// <param name="outronIntronList"></param>
        public void InsertIntronList(OutronIntronCollection
            outronIntronList)
        {
            OutronIntronTableGenerator outronIntronTableGenerator
                = new OutronIntronTableGenerator();

            List<string> tableList = 
                outronIntronTableGenerator.GetLocalTable(outronIntronList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsertOutronIntron", table);
        }
        /// <summary>
        /// only insert keys, not change pdl info
        /// </summary>
        /// <param name="keyList"></param>
        public void InsertkeyList(KeyCollection keyList)
        {
            //insert keyList
            int rid = (int)Mapper().QueryForObject("Key.GetNextRID", null);

            KeyTableGenerator keyTableGenerator = new KeyTableGenerator();

            List<string> tableList =
                keyTableGenerator.GetLocalTable(rid, keyList);

            foreach (string table in tableList)
                Mapper().Insert("Key.BulkInsert", table);
        }
    }
}
