using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    public class AdHocDAO : DAO
    {
        public HashtableCollection Select(string statment, 
            Hashtable paramList)
        {
            IList list = Mapper().QueryForList(statment, paramList);
            HashtableCollection tableList = new HashtableCollection();

            foreach (Hashtable table in list)
            {
                tableList.Add(table);
            }

            return tableList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statment"></param>
        /// <param name="paramList"></param>
        public void Insert(string statment, Hashtable paramList)
        {
            Mapper().Insert(statment, paramList);
        }

        public void Delete(string statment, Hashtable paramList)
        {
            Mapper().Delete(statment, paramList);
        }

        public void Update(string statment, Hashtable paramList)
        {
            Mapper().Update(statment, paramList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statment"></param>
        /// <param name="paramList"></param>
        /// <returns></returns>
        public StringCollection SelectString(string statment,
            Hashtable paramList)
        {
            IList list = Mapper().QueryForList(statment, paramList);
            StringCollection strList = new StringCollection();

            foreach (String str in list)
            {
                strList.Add(str);
            }

            return strList;
        }

        public IntegerCollection SelectInteger(string statment,
            Hashtable paramList)
        {
            IList list = Mapper().QueryForList(statment, paramList);
            IntegerCollection intList = new IntegerCollection();

            foreach (int n in list)
            {
                intList.Add(n);
            }

            return intList;
        }

        public Hashtable SelectIntron(Hashtable paramList)
        {
            return (Hashtable) Mapper().QueryForObject(
                "AdHoc.GetIntronData", paramList);
        }

        /// <summary>
        /// Executes the statement passed as argument.
        /// </summary>
        /// <remarks>Created by binil on 12/10/2010 for implementing stored procedure</remarks>
        /// <param name="statement"></param>
        /// <param name="parameterObject"></param>
        /// <returns></returns>
        public IList<string> ExecuteStatement(string statement, object parameterObject)
        {
            return Mapper().QueryForList<string>(statement, parameterObject);
        }
    }
}
