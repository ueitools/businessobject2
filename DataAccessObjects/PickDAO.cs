using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    public class PickDAO : DAO
    {
        readonly char[] IntronSeparator = new char[] { (char)0x3F };

        /// <summary>
        /// Populate intron list 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public DataMapCollection GetDataMap(string projectName, string mode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("Mode", mode);

            DataMapCollection dataMapList = 
                Mapper().QueryForList("Pick.GetDataMap", paramList)
                as DataMapCollection;

            OutronIntronCollection outronIntronList = GetOutronIntron(
                projectName, mode);

            PopulateIntronList(dataMapList, outronIntronList);
            return dataMapList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public OutronIntronCollection GetOutronIntron(
            string projectName, string mode)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("Mode", mode);

            return Mapper().QueryForList("Pick.GetOutronIntron", paramList)
                as OutronIntronCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataMapList"></param>
        /// <param name="outronIntronList"></param>
        void PopulateIntronList(DataMapCollection dataMapList,
            OutronIntronCollection outronIntronList)
        {
            foreach (DataMap dataMap in dataMapList)
            {
                dataMap.IntronList =
                    outronIntronList.GetIntron(dataMap.Outron);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetNopData(string id)
        {
            return Mapper().QueryForObject(
                "Pick.GetNopData", id) as String;
        }

        public IList<Hashtable> GetAllNopData()
        {
            return Mapper().QueryForList<Hashtable>("Pick.GetAllNopData",null);
        }

        public void InsertPickID(PickIDSnapshot pickID, bool automaticVersion)
        {
            Hashtable table = new Hashtable();
            ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(pickID.ProjectName);
            int projectCode = projectHeader.Code;
            int version = pickID.Version;

            if (automaticVersion)
                version = GetMaxVersion(pickID.ID, projectHeader.Code) + 1;
            table["ID"] = pickID.ID;
            table["Version"] = version;
            table["Time"] = pickID.Time;
            table["Name"] = pickID.Name;
            table["Comment"] = pickID.Comment;
            table["Project_Code"] = projectCode;
            table["Exec"] = pickID.Executor_Code;
            table["isinverseddata"] = pickID.IsInversedData;
            table["isexternalprefix"] = pickID.IsExternalPrefix;
            table["isfrequencydata"] = pickID.IsFrequencyData;
            table["isinversedprefix"] = pickID.IsInversedPrefix;
            table["ishexformat"] = pickID.IsHexFormat;
            table["PickFromID"] = AdaptTo4DigitID(pickID.PickFromID);
            table["isinternalpick"] = pickID.IsInternalPick;
            table["istemporary"] = pickID.IsTemporary;
            int pickRID = (int)Mapper().Insert("Pick.InsertPickIDOnly", table);

            InsertPickPrefix(pickID, pickRID);
            InsertPickDataMap(pickID.DataMapList, pickRID);
            InsertOriginalIDLog(pickID.LogList, pickRID);
            InsertOriginalIDDevice(pickID.DeviceList, pickRID);
            InsertOriginalIDFunction(pickID.FunctionList, pickRID);
        }

        public PickIDSnapshotCollection GetPickIDSnapshotHeader(string projectName)
        {
            return
                Mapper().QueryForList("Pick.GetPickIDSnapshotCollection", projectName) as
                PickIDSnapshotCollection;
        }

        public void GetPickIDSnapshotPrefixList(PickIDSnapshot snapshot)
        {
            Hashtable table = new Hashtable();

            table["RID"] = snapshot.RID;
            table["ID"] = snapshot.ID;
            snapshot.PrefixList =
                Mapper().QueryForList("Pick.GetPrefixCollection", table) as PrefixCollection;
        }

        public void GetPickIDSnapshotDataMapList(PickIDSnapshot snapshot)
        {
            snapshot.DataMapList = 
                Mapper().QueryForList("Pick.GetDataMapCollection", snapshot.RID) as DataMapCollection;

            for (int i = 0; i < snapshot.DataMapList.Count; i++)
            {
                DataMap dataMap = snapshot.DataMapList[i];

                dataMap.LabelList = Mapper().
                    QueryForList("Pick.GetPickDataMapLabelCollection", dataMap.RID) as StringCollection;
                dataMap.IntronList = Mapper().
                    QueryForList("Pick.GetPickDataMapPDLIntronCollection", dataMap.RID) as StringCollection;
            }
        }

        public void GetPickIDSnapshotOriginalIDLog(PickIDSnapshot snapshot)
        {
            Hashtable table = new Hashtable();

            table["PickRID"] = snapshot.RID;
            table["ID"] = snapshot.ID;
            snapshot.LogList = Mapper().
                QueryForList("Pick.GetOriginalIDLog", table) as LogCollection;
        }

        public void GetPickIDSnapshotOriginalIDDevice(PickIDSnapshot snapshot)
        {
            Hashtable table = new Hashtable();

            table["PickRID"] = snapshot.RID;
            table["ID"] = snapshot.ID;
            snapshot.DeviceList = Mapper().
                QueryForList("Pick.GetOriginalIDDevice", table) as DeviceCollection;
        }

        public void GetPickIDSnapshotOriginalIDFunction(PickIDSnapshot snapshot)
        {
            Hashtable table = new Hashtable();

            table["PickRID"] = snapshot.RID;
            snapshot.FunctionList = Mapper().
                QueryForList("Pick.GetOriginalIDFunction", table) as IDFunctionCollection;

            for (int i = 0; i < snapshot.FunctionList.Count; i++)
            {
                snapshot.FunctionList[i].IDTNCaptureList = Mapper().
                    QueryForList("Pick.GetOriginalIDTNCapture", snapshot.FunctionList[i].RID) as
                    IDTNCaptureCollection;
            }
        }

        public void InsertPickPrefix(PickIDSnapshot pickID, int pickRID)
        {
            int order = 0;

            foreach (Prefix prefix in pickID.PrefixList)
            {
                Hashtable table = new Hashtable();

                table["Data"] = prefix.Data;
                table["Comment"] = prefix.Description;
                table["Order"] = order++;
                table["PickRID"] = pickRID;
                Mapper().Insert("Pick.InsertPickPrefix", table);
            }
        }

        public void InsertPickDataMap(DataMapCollection dataMapList, int pickRID)
        {
            foreach (DataMap dataMap in dataMapList)
            {
                Hashtable table = new Hashtable();

                table["PickRID"] = pickRID;
                table["Data"] = dataMap.Data;
                if (dataMap.Synth != null && dataMap.Synth.Length < 10)
                {
                    table["Synth"] = dataMap.Synth;
                }
                table["Outron"] = dataMap.Outron;
                table["KeyLabel"] = dataMap.KeyLabel;
                table["OutronGroup"] = dataMap.OutronGroup;
                AssignIntronAndPriority(table, dataMap.Intron, dataMap.IntronPriority);
                table["Position"] = dataMap.Position;
                table["Comment"] = "";
               
                int rid = (int)Mapper().Insert("Pick.InsertPickDataMap", table);

                InsertPickDataMapLabel(dataMap.LabelList, rid);
                InsertPickDataMapPDLIntron(dataMap.IntronList, rid);
            }
        }

        public void InsertPickDataMapLabel(StringCollection labelList, int pickDataMapRID)
        {
            int order = 0;

            foreach (string label in labelList)
            {
                Hashtable table = new Hashtable();

                table["Label"] = label;
                table["PickDataMap_RID"] = pickDataMapRID;
                table["Order"] = order++;

                Mapper().Insert("Pick.InsertPickDataMapLabel", table);
            }
        }

        public void InsertPickDataMapPDLIntron(StringCollection intronList, int pickDataMapRID)
        {
            int order = 0;

            foreach (string intron in intronList)
            {
                Hashtable table = new Hashtable();

                table["Intron"] = intron;
                table["PickDataMap_RID"] = pickDataMapRID;
                table["Order"] = order++;

                Mapper().Insert("Pick.InsertPickDataMapPDLIntron", table);
            }
        }

        public void InsertOriginalIDLog(LogCollection logList, int pickRID)
        {
            foreach (Log log in logList)
            {
                Hashtable table = new Hashtable();

                table["TN"] = log.TN;
                table["Time"] = log.Time;
                table["Operator"] = log.Operator;
                table["PickRID"] = pickRID;

                Mapper().Insert("Pick.InsertOriginalIDLog", table);
            }            
        }

        public void InsertOriginalIDDevice(DeviceCollection deviceList, int pickRID)
        {
            foreach (Device device in deviceList)
            {
                Hashtable table = new Hashtable();

                table["PickRID"] = pickRID;
                table["TN"] = device.TN;
                table["Branch"] = device.Branch;
                table["ispartnumber"] = device.IsPartNumber;
                table["isretail"] = device.IsRetail;

                Mapper().Insert("Pick.InsertOriginalIDDevice", table);
            }
        }

        public void InsertOriginalIDFunction(IDFunctionCollection functionList, int pickRID)
        {
            foreach (IDFunction function in functionList)
            {
                try
                {
                    Hashtable table = new Hashtable();

                    table["Synth"] = function.Synth;
                    table["Data"] = function.Data;
                    table["Label"] = function.Label;
                    AssignIntronAndPriority(table, function.Intron, function.IntronPriority);
                    table["Comment"] = function.Comment;
                    table["PickRID"] = pickRID;

                    int functionRID = (int)Mapper().Insert("Pick.InsertOriginalIDFunction", table);

                    InsertOriginalIDTNCaptureList(function.IDTNCaptureList, functionRID);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void InsertOriginalIDTNCaptureList(IDTNCaptureCollection IDTNCaptureList, int functionRID)
        {
            foreach (IDTNCapture idtn in IDTNCaptureList)
            {
                Hashtable table = new Hashtable();

                table["TN"] = idtn.TN;
                table["Filename"] = idtn.Filename;
                table["LegacyLabel"] = idtn.LegacyLabel;
                table["FunctionRID"] = functionRID;
                table["Label"] = idtn.Label;

                Mapper().Insert("Pick.InsertOriginalIDTNCaptureList", table);
            }
        }

        public void DeleteTempPickID(PickID pickID, string product)
        {
            Hashtable paramList = new Hashtable();

            paramList["ProjectName"] = product;
            paramList["ID"] = pickID.Header.ID;
            Mapper().Delete("Pick.DeleteTempID", paramList);
        }

        public void DeleteAllPick(string product)
        {
            ProjectHeader projectHeader = ProductDBFunctions.GetProjectHeader(product);
            int projectCode = projectHeader.Code;

            Mapper().Delete("Pick.DeleteAllPick", projectCode);
        }

        public int GetMaxVersion(string ID, int projectCode)
        {
            Hashtable table = new Hashtable();

            table["ID"] = ID;
            table["Project_Code"] = projectCode;
            object max = Mapper().QueryForObject("Pick.GetMaxPickIDVersion", table);

            if (max == null)
                return -1;
            else
                return (int)max;
        }

        private void AssignIntronAndPriority(Hashtable table, 
            string intronAndPriority, 
            string intronPriority)
        {
            table["IntronPriority"] = intronPriority;
            if (intronAndPriority == null)
            {
                return;
            }
            string[] array = intronAndPriority.Split(IntronSeparator, 2, StringSplitOptions.None);

            table["Intron"] = array[0];
            if (array.Length > 1)
            {
                table["IntronPriority"] = array[1];
            }
        }

        private string AdaptTo4DigitID(string id)
        {
            if (id.Length == 5)
                return id;

            int num = Int32.Parse(id.Substring(1));

            return id[0] + num.ToString("D4");
        }

        

    }
}
