using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using IBatisNet.DataMapper;

namespace BusinessObject
{
    class FirmwareDAO : DAO
    {
        #region Singleton Instance
        private static volatile FirmwareDAO _firmwareDAO = null;

        public static FirmwareDAO GetInstance()
        {
            if (_firmwareDAO == null)
            {
                lock (typeof(FirmwareDAO))
                {
                    if (_firmwareDAO == null) // double-check
                        _firmwareDAO = new FirmwareDAO();
                }
            }

            return _firmwareDAO;
        }
        #endregion

        public Firmware Load(string projectName)
        {
            Firmware firmware = new Firmware();
            firmware.Header = SelectHeader(projectName);
            firmware.AddressList = SelectAddress(projectName);
            firmware.ExecutorList = SelectExecutorLoad(projectName);
            firmware.IDList = SelectIDLoad(projectName);
            firmware.DigitGroupList = SelectDigitGroup(projectName);
            firmware.AlternateIDList = SelectAlternateCodeTable(projectName);
            firmware.GoldenImage = SelectGoldenImageCollection(projectName);

            return firmware;
        }

        public void Save(Firmware firmware, bool saveIDLoad)
        {
            Insert(firmware.Header);
            Insert(firmware.AddressList);
            Insert(firmware.DigitGroupList);
            Insert(firmware.ExecutorList);
            if (saveIDLoad)
                Insert(firmware.IDList);
            Insert(firmware.AlternateIDList);
            InsertGoldenImageCollection(firmware.Header.Project_Code, firmware.GoldenImage);
        }

        public void Delete(Firmware firmware)
        {
            Delete(firmware, false);
        }

        public void Delete(Firmware firmware, bool keepPickInfo)
        {
            Hashtable paramList = new Hashtable();

            int code = firmware.Header.Project_Code;

            Mapper().Delete("Firmware.DeleteFirmware", code);
            Mapper().Delete("Firmware.DeleteAddress", code);
            if (!keepPickInfo)
                Mapper().Delete("Firmware.DeleteIDLoad", code);
            Mapper().Delete("Firmware.DeleteExecutorLoad", code);
            Mapper().Delete("Firmware.DeleteGoldenImage", code);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public FirmwareHeader SelectHeader(string projectName)
        {
            return Mapper().QueryForObject("Firmware.SelectHeader",
                    projectName) as FirmwareHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public AddressCollection SelectAddress(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectAddress",
                projectName) as AddressCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public ExecutorLoadCollection SelectExecutorLoad(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectExecutorLoad",
                projectName) as ExecutorLoadCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ExecutorLoadCollection SelectAllExecutors()
        {
            return Mapper().QueryForList("Firmware.SelectDistinctExecutors",
                null) as ExecutorLoadCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public IDLoadCollection SelectIDLoad(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectIDLoad",
                    projectName) as IDLoadCollection;
        }
        public IDLoadCollection SelectIDLoadWithCode(int projectCode)
        {
            return Mapper().QueryForList("Firmware.SelectIDLoadWithCode",
                    projectCode) as IDLoadCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public AlternateIDCollection SelectAlternatesInLoad(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectAlternateLoad",
                projectName) as AlternateIDCollection;
        }

        public AlternateIDCollection SelectAlternatesInLoad(int projectCode)
        {
            return Mapper().QueryForList("Firmware.SelectAlternateLoadWithCode",
                projectCode) as AlternateIDCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public AlternateIDCollection SelectAlternateCodeTable(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectAlternateCodeTable",
                projectName) as AlternateIDCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public AlternateIDCollection SelectAlternateCodeTableByExternalID(string projectName)
        {
            return Mapper().QueryForList("Firmware.SelectAlternateCodeTableByExternalID",
                projectName) as AlternateIDCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public int DeleteAlternateCode(string projectName, string mode)
        {
            Hashtable paramList = new Hashtable();

            paramList["ProjectName"] = projectName;
            paramList["mode"] = mode;

            return Mapper().Delete("Firmware.DeleteAlternateCode", paramList);
        }

        public void DeleteAliasCode(int projectCode, string modeList)
        {
            DeleteAliasCodeCodeParams paramList = new DeleteAliasCodeCodeParams();

            paramList.FK_ProjectCode = projectCode;
            paramList.ModeList = modeList;

            Mapper().Delete("Firmware.DeleteAliasCode", paramList);
        }

        public void DeleteEquivalentAlternates(int projectCode)
        {
            Mapper().Delete("Firmware.DeleteEquivalentAlternateCodes", projectCode);
        }

        public void InsertAliasCode(int projectCode, string globalCode, string localCode)
        {
            AliasCode aliasCode = new AliasCode();

            aliasCode.ProjectCode = projectCode;
            aliasCode.GlobalCode = globalCode;
            aliasCode.LocalCode = localCode;

            Mapper().QueryForObject<int>("Firmware.InsertAliasCode", aliasCode);
        }

        public AliasCodeCollection GetAliasCodes(int projectCode)
        {
            return Mapper().QueryForList("Firmware.GetAliasCodes", projectCode) as AliasCodeCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public StringCollection SelectAllID()
        {
            return Mapper().QueryForList("Firmware.SelectDistinctIDs",
                null) as StringCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        DigitGroupCollection SelectDigitGroup(string projectName)
        {
            DigitGroupCollection digitGroupList = 
                Mapper().QueryForList("Firmware.SelectDigitGroupNumber",
                projectName) as DigitGroupCollection;

            foreach (DigitGroup group in digitGroupList)
            {
                group.DataList = SelectDigitGroupData(
                    projectName, group.GroupNumber);
            }

            return digitGroupList;
        }

        StringCollection SelectDigitGroupData(
            string projectName, int digitGroupNumber)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectName", projectName);
            paramList.Add("DigitGroupNumber", digitGroupNumber);

            return Mapper().QueryForList("Firmware.SelectDigitGroupData",
                paramList) as StringCollection;
        }

        public FirmwareHeader SelectHeaderWithCode(string projectName)
        {
            return Mapper().QueryForObject("Firmware.SelectHeaderWithCode",
                    projectName) as FirmwareHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        void Insert(FirmwareHeader header)
        {
            Mapper().Insert("Firmware.InsertHeader", header);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        public void Update(FirmwareHeader header)
        {
            Mapper().Update("Firmware.UpdateHeader", header);
        } 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="addressList"></param>
        void Insert(AddressCollection addressList)
        {
            //insert addressList
            AdressTableGenerator adressTableGenerator
                = new AdressTableGenerator();

            List<string> tableList =
                adressTableGenerator.GetLocalTable(addressList);

            foreach (string table in tableList)
                Mapper().Insert("Firmware.BulkInsertAddress", table);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addresst"></param>
        public void Update(Address address)
        {
            Mapper().Update("Firmware.UpdateAddress", address);
        }

        public void Update(IDLoad idLoad)
        {
            Mapper().Update("Firmware.UpdateIDLoad",idLoad);
        }

        void Insert(DigitGroupCollection digitGroupList)
        {
            //insert digitGroupList
            DigitGroupTableGenerator digitGroupTableGenerator
                = new DigitGroupTableGenerator();

            List<string> tableList =
                digitGroupTableGenerator.GetLocalTable(digitGroupList);

            foreach (string table in tableList)
                Mapper().Insert("Firmware.BulkInsertDigitGroup", table);
        }

        void Insert(DigitGroup digitGroup)
        {
            int order = 0;
            foreach (string data in digitGroup.DataList)
            {
                DigitGroupRow row = new DigitGroupRow();
                row.Project_Code = digitGroup.Project_Code;
                row.Code = digitGroup.GroupNumber;
                row.Order = order;
                row.Data = data;

                Mapper().Insert("Firmware.InsertDigitGroup", row);
                order += 1;
            }
        }

        void Insert(ExecutorLoadCollection executorLoadList)
        {
            //insert executorLoadList
            ExecutorLoadTableGenerator executorLoadTableGenerator
                = new ExecutorLoadTableGenerator();

            List<string> tableList =
                executorLoadTableGenerator.GetLocalTable(executorLoadList);

            foreach (string table in tableList)
                Mapper().Insert("Firmware.BulkInsertExecutorLoad", table);
        }

        internal void Insert(IDLoadCollection idLoadList)
        {
            //insert executorLoadList
            IdLoadTableGenerator idLoadTableGenerator
                = new IdLoadTableGenerator();

            List<string> tableList =
                idLoadTableGenerator.GetLocalTable(idLoadList);

            foreach (string table in tableList)
                Mapper().Insert("Firmware.BulkInsertIDLoad", table);
        }

        /// <summary>
        /// </summary>
        /// <param name="altIDList"></param>
        public void Insert(AlternateIDCollection altIDList)
        {
            AlternateCodeTableGenerator altCodeTableGenerator
                = new AlternateCodeTableGenerator();

            List<string> tableList =
                altCodeTableGenerator.GetLocalTable(altIDList);

            foreach (string table in tableList)
                Mapper().Insert("Firmware.BulkInsertAlternateCode", table);
        }

        public void InsertGoldenImage(int projectCode, byte[] image, DateTime dateTime)
        {
            Hashtable parameterMap = new Hashtable();

            parameterMap["BinaryData"] = image;
            parameterMap["ProjectCode"] = projectCode;
            parameterMap["Date"] = dateTime;
            Mapper().Insert("Firmware.InsertGoldenImageWithProjectCode", parameterMap);
        }

        public void InsertGoldenImageCollection(int projectCode, GoldenImageCollection collection)
        {
            if (collection == null)
                return;

            foreach (GoldenImage image in collection)
            {
                InsertGoldenImage(projectCode, image.BinaryData, DateTime.Now);
            }
        }

        public void InsertGoldenImage(string projectName, byte[] image)
        {
            Hashtable parameterMap = new Hashtable();

            parameterMap["BinaryData"] = image;
            parameterMap["ProjectName"] = projectName;

            Mapper().Insert("Firmware.InsertGoldenImage", parameterMap);
        }

        public byte[] SelectGoldenImage(string projectName)
        {
            Hashtable parameterMap = new Hashtable();

            parameterMap["ProjectName"] = projectName;

            GoldenImage image = Mapper().QueryForObject("Firmware.SelectGoldenImage", 
                parameterMap) as GoldenImage;

            return image.BinaryData;
        }

        public GoldenImageCollection SelectGoldenImageCollection(string projectName)
        {
            Hashtable parameterMap = new Hashtable();

            parameterMap["ProjectName"] = projectName;

            GoldenImageCollection collection = Mapper().QueryForList(
                "Firmware.SelectGoldenImageCollection", parameterMap) as GoldenImageCollection;

            return collection;
        }

        public void FlagIDAsObsolete(string id)
        {
            Mapper().Update("Firmware.FlagIDAsObsolete", id);
        }

        public void FlagExecutorAsObsolete(int executorCode)
        {
            Mapper().Update("Firmware.FlagExecutorAsObsolete", executorCode);
        }

        public void SetExecutorDoUpdate(int projectCode,
            int executorCode, bool update)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ExecutorCode", executorCode);

            if (update)
                paramList.Add("DoUpdate", BooleanFlag.YES);
            else paramList.Add("DoUpdate", BooleanFlag.NO);

            Mapper().Update("Firmware.SetExecutorDoUpdate", paramList);
        }

        public void SetIDDoUpdate(int projectCode, string id,
            bool update)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ProjectCode", projectCode);
            paramList.Add("ID", id);

            if (update)
                paramList.Add("DoUpdate", BooleanFlag.YES);
            else paramList.Add("DoUpdate", BooleanFlag.NO);

            Mapper().Update("Firmware.SetIDDoUpdate", paramList);
        }
    }

    [Serializable]
    public class LoadAlternateFirmwareInfo : DAO
    {
    }

    [Serializable]
    public class LoadAlternateFirmwareCollection : DAO
    {
    }

    public class DeleteAliasCodeCodeParams
    {
        private int projectCode;
        private string modeList;

        public int FK_ProjectCode
        {
            get { return projectCode; }
            set { projectCode = value; }
        }

        public string ModeList
        {
            get { return modeList; }
            set { modeList = value;  }
        }
    }
}
