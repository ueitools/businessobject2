using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class ProjectDAO : DAO
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public Project Load(string projectName)
        {
            return Load(projectName,true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public Project Load(string projectName, bool getPDLInfo)
        {
            Project project = new Project();
            project.Header = SelectHeader(projectName);
            project.KeyList = KeyDAO.GetInstance().Load(projectName,getPDLInfo);
            project.Firmware = FirmwareDAO.GetInstance().Load(projectName);
            project.ConfigList = SelectConfig(projectName);
            project.ChipList = SelectChip(projectName);
            return project;
        }

        /// <summary>
        /// Clean and Reinsert
        /// 
        /// Every record related to project will be deleted by
        /// "cascading delete" 
        /// </summary>
        /// <param name="project"></param>
        public void Save(Project project)
        {
            Delete(project.Header.Name);
            Save(project, GetNextProjectCode());
        }

        /// <summary>
        /// Clean and Reinsert with the project code
        /// 
        /// Every record related to project will be deleted by
        /// "cascading delete" 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="projectCode"></param>
        public void Save(Project project, int projectCode)
        {
            Delete(project.Header.Name);
            project.SyncProjectCode(projectCode);
            Save(project.Header);
            KeyDAO.GetInstance().Save(project.KeyList);
            FirmwareDAO.GetInstance().Save(project.Firmware, true);

            foreach (Chip chip in project.ChipList)
                chip.Project_Code = projectCode;

            Save(project.ChipList);
            Save(project.ConfigList);
        }

        public void AddPick2Related(Project project)
        {
            project.Firmware.SyncProjectCode(project.Header.Code);
            FirmwareDAO.GetInstance().Save(project.Firmware, false);
            project.ConfigList.SyncProjectCode(project.Header.Code);
            Save(project.ConfigList);
        }

        public void RemovePick2Related(Project project)
        {
            DeleteAllConfig(project.Header.Code);
            const bool keepPick = true;
            project.Firmware.SyncProjectCode(project.Header.Code);
            FirmwareDAO.GetInstance().Delete(project.Firmware, keepPick);
        }

        public Project LoadPick2Related(string projectName)
        {
            Project project = new Project();

            project.Header = SelectHeader(projectName);
            project.Firmware = FirmwareDAO.GetInstance().Load(projectName);
            project.ConfigList = SelectConfig(projectName);
            project.ChipList = SelectChip(projectName);

            return project;
        }

        public void SaveHeaderOnly(Project project)
        {
            Delete(project.Header.Name);
            int projectCode = GetNextProjectCode();
            project.SyncProjectCode(projectCode);
            Save(project.Header);
        }

        /// <summary>
        /// Get a list of all project headers
        /// </summary>
        /// <returns></returns>
        public ProjectHeaderCollection ProjectList()
        {
            return Mapper().QueryForList(
                "Project.GetProjectList", null)
                as ProjectHeaderCollection;
        }

        /// <summary>
        /// Get a list of all project headers with the executor
        /// </summary>
        /// <returns></returns>
        public ProjectHeaderCollection ProjectList(int executor)
        {
            return Mapper().QueryForList(
                "Project.GetProjectListByExecutor", executor)
                as ProjectHeaderCollection;
        }
        /// <summary>
        /// Get a list of all project headers with ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProjectHeaderCollection ProjectList(string id)
        {
            return Mapper().QueryForList(
                "Project.GetProjectListByID", id)
                as ProjectHeaderCollection;
        }

        public ProjectHeaderCollection ProjectListByAddressVersion(string addressVersion)
        {
            return Mapper().QueryForList(
                "Project.GetProjectListByAddressVersion", addressVersion)
                as ProjectHeaderCollection;
        }

        public ProjectHeader SelectHeader(string projectName)
        {
            return Mapper().QueryForObject(
                "Project.SelectHeader", projectName)
                as ProjectHeader;
        }

        void Save(ProjectHeader header)
        {
            Mapper().Insert("Project.Insert", header);
        }

        public void Update(ProjectHeader header)
        {
            Mapper().Update("Project.Update", header);
        }

        public void Delete(string projectName)
        {
            Mapper().Delete("Project.Delete", projectName);
        }

        int GetNextProjectCode()
        {
            return (int)Mapper().QueryForObject(
                "Project.GetNextProjectCode", null);
        }

        public ConfigCollection SelectConfig(string projectName)
        {
            return Mapper().QueryForList("Project.SelectConfig",
                projectName) as ConfigCollection;
        }
        /// <summary>
        /// Get the syth_digit 
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public string SelectSynthDigit(string projectName)
        {
            return Mapper().QueryForObject("Project.SelectSynthDigit",
                projectName) as string;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ChipCollection GetChipList()
        {
            return Mapper().QueryForList("Project.SelectChip",
                null) as ChipCollection;
        }

        public ChipCollection SelectChip(string projectName)
        {
            return Mapper().QueryForList("Project.SelectChip", 
                projectName) as ChipCollection;
        }

        int GetChipCode(string number)
        {
            Object obj = Mapper().QueryForObject(
                "Project.GetChipCode", number);

            if (obj != null )
                return (int)obj;

            return -1;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chipList"></param>
        public void Save(ChipCollection chipList)
        {
            foreach (Chip chip in chipList)
            {
                chip.Chip_Code = GetChipCode(chip.Number);

                if (chip.Chip_Code == -1)
                {
                    chip.Chip_Code = (int)Mapper().QueryForObject(
                        "Project.GetNextChipCode", null);
                    Mapper().Insert("Project.InsertChip", chip);                
                }
            }

            //insert chipList
            Project_ChipTableGenerator project_ChipTableGenerator
                = new Project_ChipTableGenerator();

            List<string> tableList =
                project_ChipTableGenerator.GetLocalTable(chipList);

            foreach (string table in tableList)
                Mapper().Insert("Project.BulkInsertProject_Chip", table);
        }
        /// <summary>
        /// insert configList
        /// </summary>
        /// <param name="configList"></param>
        public void Save(ConfigCollection configList)
        {
            //insert configList
            ConfigTableGenerator configTableGenerator
                = new ConfigTableGenerator();

            List<string>  tableList =
                configTableGenerator.GetLocalTable(configList);

            foreach (string table in tableList)
                Mapper().Insert("Project.BulkInsertConfig", table);
        }
        /// <summary>
        /// update the config list of the project 
        /// </summary>
        /// <param name="project_code"></param>
        public void Update(Config config)
        {
            Mapper().Update("Project.UpdateConfig", config);
        }
        /// <summary>
        /// delete the config
        /// </summary>
        /// <param name="config"></param>
        public void Delete(Config config)
        {
            Mapper().Delete("Project.DeleteConfig", config);
        }

        public void DeleteAllConfig(int projectCode)
        {
            Mapper().Delete("Project.DeleteAllProjectConfig", projectCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectCode"></param>
        /// <param name="market"></param>
        /// <param name="branch"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public IList<BrandId> GetBrandIdList(string market, string branch, string mode, List<string> idLoad)
        {
            Hashtable param = new Hashtable();
         
            param.Add("Market", market);
            param.Add("Branch", branch);
            param.Add("Mode", mode);
            param.Add("IDLoad", idLoad);
            return Mapper().QueryForList<BrandId>("Project.GetBrandList2", param);
        }

        public IList<string> GetBrandIdListOptimized(string market, string branch, string mode, string idLoad)
        {
            Hashtable param = new Hashtable();

            param.Add("Market", market);
            param.Add("Branch", branch);
            param.Add("Mode", mode);
            param.Add("IDLoad", idLoad);
            return Mapper().QueryForList<string>("Project.GetBrandListV3", param);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectName"></param>
        /// <param name="chipNumber"></param>
        /// <param name="URCNumber"></param>
        /// <param name="revision"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public HashtableCollection LoadInfo(string projectName, string chipNumber,
                                            int URCNumber, string revision, string description)
        {
            Hashtable param = new Hashtable();
            if (!string.IsNullOrEmpty(projectName))
                param.Add("Name", projectName);
            if (!string.IsNullOrEmpty(chipNumber))
                param.Add("Chip", chipNumber);
            if (URCNumber > 0)
                param.Add("URC", URCNumber);
            if (!string.IsNullOrEmpty(revision))
                param.Add("Rev", revision);
            if (!string.IsNullOrEmpty(description))
                param.Add("Desc", description);

            return Mapper().QueryForList("Project.GetProjectInfo", param) as HashtableCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public ProductCollection ProjectProductList(int project)
        {
            return Mapper().QueryForList(
                "Project.GetProductList", project) as ProductCollection;
        }

        void DeleteProjectProducts(int project)
        {
            ProductCollection productList = ProjectProductList(project);
            foreach (Product item in productList)
            {
                if (item.Chip_Code <= 0)
                    continue;

                if (item.URC <= 0 && string.IsNullOrEmpty(item.Description))
                    continue;

                Hashtable param = new Hashtable();
                param.Add("URC", item.URC);
                if ( item.Revision != null )
                    param.Add("Revision", item.Revision);
                param.Add("Chip_Code", item.Chip_Code);
                Mapper().Delete("Project.DeleteProduct", param);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="project"></param>
        public void Save(ProductCollection product, int project)
        {
            DeleteProjectProducts(project);
            foreach (Product item in product)
            {
                if (string.IsNullOrEmpty(item.Chip_Number))     // null or bad entry
                    continue;

                if (item.Chip_Code <= 0)
                {
                    object tmp = Mapper().QueryForObject(
                        "Project.GetChipCode", item.Chip_Number);
                    if (tmp != null)
                        item.Chip_Code = (int)tmp;
                }

                if (item.Chip_Code <= 0)    // Create a new chip
                    CreateNewChip(project, item);

                if (item.URC > 0 || !string.IsNullOrEmpty(item.Description))
                    Mapper().Insert("Project.InsertProduct", item);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="project"></param>
        public void Insert(ProductCollection product, int project)
        {
            foreach (Product item in product)
            {
                if (string.IsNullOrEmpty(item.Chip_Number))     // null or bad entry
                    continue;

                if (item.Chip_Code <= 0)
                {
                    object tmp = Mapper().QueryForObject(
                        "Project.GetChipCode", item.Chip_Number);
                    if (tmp != null)
                        item.Chip_Code = (int)tmp;
                }

                if (item.Chip_Code <= 0)    // Create a new chip
                    CreateNewChip(project, item);

                if (item.URC > 0 || !string.IsNullOrEmpty(item.Revision))
                    Mapper().Insert("Project.InsertProduct", item);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <param name="item"></param>
        private void CreateNewChip(int project, Product item)
        {
            Chip newchip = new Chip();
            item.Chip_Code = (int)Mapper().QueryForObject("Project.GetNextChipCode", null);
            newchip.Chip_Code = item.Chip_Code;
            newchip.Number = item.Chip_Number;
            newchip.Project_Code = project;
            Mapper().Insert("Project.InsertChip", newchip);
            Mapper().Insert("Project.InsertProject_Chip", newchip);
        }

        public void AddNewChip(Chip chip)
        {
            chip.Chip_Code = (int)Mapper().QueryForObject("Project.GetNextChipCode", null);
            Mapper().Insert("Project.InsertChip", chip);
            Mapper().Insert("Project.InsertProject_Chip", chip);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="project"></param>
        public void Save(ProductCollection product)
        {
            foreach (Product item in product)
            {
                if (string.IsNullOrEmpty(item.Chip_Number))     // null or bad entry
                    continue;

                object tmp = Mapper().QueryForObject(
                    "Project.GetChipCode", item.Chip_Number);
                if (tmp != null)
                    continue;

                // Create a new chip
                CreateNewChip(item);

                if (item.URC > 0 || !string.IsNullOrEmpty(item.Description))
                    Mapper().Insert("Project.InsertProduct", item);
            }
        }

        private void CreateNewChip(Product item)
        {
            Chip newchip = new Chip();
            item.Chip_Code = (int)Mapper().QueryForObject("Project.GetNextChipCode", null);
            newchip.Chip_Code = item.Chip_Code;
            newchip.Number = item.Chip_Number;
            Mapper().Insert("Project.InsertChip", newchip);
        }

        /// <summary>
        /// delete the chip
        /// </summary>
        /// <param name="chip"></param>
        public void DeleteChip(Chip chip)
        {
            chip.Chip_Code = GetChipCode(chip.Number);
            Mapper().Delete("Project.DeleteChip", chip);
        }

        /// <summary>
        /// delete the product
        /// </summary>
        /// <param name="chipCode"></param>
        public void DeleteProduct(int chipCode)
        {
            Hashtable param = new Hashtable();
            param.Add("Chip_Code", chipCode);
            Mapper().Delete("Project.DeleteProduct", param);
        }

        public IList GetChipManufacturerList()
        {
            return Mapper().QueryForList("Project.GetChipManufacturerList", null);
        }

        public void CopyPick2Partially(string sourceName, string dbSourceId, string destName, string dbDestId)
        {
            CopyProcedureParameter parameter = new CopyProcedureParameter();

            parameter.ProjectNameSource = sourceName;
            parameter.ProjectNameDestination = destName;

            switch (dbSourceId)
            {
                case "Working":
                    parameter.DbSourceId = 1;
                    break;
                case "Public":
                    parameter.DbSourceId = 2;
                    break;
                case "Temp":
                    parameter.DbSourceId = 3;
                    break;
            }
            switch (dbDestId)
            {
                case "Working":
                    parameter.DbDestId = 1;
                    break;
                case "Public":
                    parameter.DbDestId = 2;
                    break;
                case "Temp":
                    parameter.DbDestId = 3;
                    break;
            }
            int errorCode = Mapper().QueryForObject<int>("Project.PS_CopyPartial", parameter);

            if (errorCode != 0)
                throw new ArgumentException("Unable to copy with given parameters");
        }

    }

    [Serializable]
    class CopyProcedureParameter
    {
        private string _projectNameSource;
        private string _projectNameDestination;
        private int _dbSourceId;
        private int _dbDestId;

        public string ProjectNameSource
        {
            get { return _projectNameSource; }
            set { _projectNameSource = value; }
        }

        public string ProjectNameDestination
        {
            get { return _projectNameDestination; }
            set { _projectNameDestination = value; }
        }

        public int DbSourceId
        {
            get { return _dbSourceId; }
            set { _dbSourceId = value; }
        }

        public int DbDestId
        {
            get { return _dbDestId; }
            set { _dbDestId = value; }
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            CopyProcedureParameter parameter = (CopyProcedureParameter)obj;
            if (this.ProjectNameDestination != parameter.ProjectNameDestination)
                return false;
            if (this.ProjectNameSource != parameter.ProjectNameSource)
                return false;
            if (this.DbSourceId != parameter.DbSourceId)
                return false;
            if (this.DbDestId != parameter.DbDestId)
                return false;

            return true;
        }
    }

}
