using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace BusinessObject
{
    public class DeviceDAO : DAO
    {
        /// <summary>
        /// select
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public DeviceCollection Select(int tn)
        {
            DeviceCollection list = Mapper().QueryForList("Device.Select", tn)
                as DeviceCollection;
            PopulateSubList(tn, list);
            return list;
        }

        public DeviceCollection SelectIDOnly(string id)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectIDOnly", id) as DeviceCollection;
            PopulateSubList(id, list);
            return list;
        }
        //for PGS
        //updated by Satyadeep on 04/03/2013
        public DeviceCollection SelectIDOnly_PGS(String id)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectIDOnly_PGS", id) as DeviceCollection;
            PopulateSubList_PGS(id, list);
            return list;
        }
        #region TempCode For Time Logging - by binil
        public DeviceCollection SelectIDOnly_PGS(String id,StringBuilder sBuilder)
        {
            //DeviceCollection list = Mapper().QueryForList(
            //    "Device.SelectIDOnly", id) as DeviceCollection;
            //PopulateSubList_PGS(id, list, sBuilder);
            //return list;

            sBuilder.AppendLine("===> SelectIDOnly_PGS Method in DeviceDao.cs");

            DateTime startTime = DateTime.Now;

            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectIDOnly_PGS", id) as DeviceCollection;

          //  DeviceCollection newList = RemoveDuplicates(list);
           
            DateTime endTime = DateTime.Now;
            TimeSpan selectIDOnlySpan = endTime - startTime;

            sBuilder.AppendLine(String.Format("====> EXEC UEI2_BO_Sp_GetIDDrTN execution:{0}", selectIDOnlySpan.ToString()));

            PopulateSubList_PGS(id, list, sBuilder);

            sBuilder.AppendLine("===> SelectIDOnly Method in DeviceDao.cs End.");
            return list;
        }       

        private void PopulateSubList_PGS(String id, DeviceCollection deviceList, StringBuilder sBuilder)
        {
            CountryCollection CountryList =
                DAOFactory.Misc().CountrySelect(id);

            DateTime startTime = DateTime.Now;
            DeviceTypeCollection DeviceTypeList =
                DAOFactory.Misc().DeviceTypeSelect_PGS(id);
            DateTime endTime = DateTime.Now;
            TimeSpan deviceLoadSpan = endTime - startTime;

            sBuilder.AppendLine(String.Format("Device Load(UEI2_Sp_GetDrDeviceTypeByID):{0}", deviceLoadSpan.ToString()));



            startTime = DateTime.Now;
            OpInfoCollection OpInfoList = new OpInfoCollection();
            
            foreach (Device device in deviceList)
            {
                device.CountryList = DAOFactory.Misc().GetCountryList(
                    device.RID, CountryList);

                device.DeviceTypeList = DAOFactory.Misc().GetDeviceTypeList(
                    device.RID, DeviceTypeList);

                device.OpInfoList = DAOFactory.Misc().GetOpInfoList(
                    device.RID, OpInfoList);
            }

            endTime = DateTime.Now;
            TimeSpan businessLogicSpan = endTime - startTime;
            sBuilder.AppendLine(String.Format("PopulateSubList Method Business Logic Time :{0}", businessLogicSpan.ToString()));
        }

        #endregion

        public DeviceCollection SelectID(string id)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectID", id) as DeviceCollection;
            PopulateSubList(id, list);
            return list;
        }

        public DeviceCollection SelectBranch(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectBranch", branch) as DeviceCollection;
            PopulateSubList(branch, list);
            return list;
        }

        public DeviceCollection SelectAddedRecByBranch(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectAddedRecByBranch", branch) as DeviceCollection;
            //PopulateSubList(branch, list);
            return list;
        }

        public DeviceCollection SelectDroppedRecByBranch(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectDroppedRecByBranch", branch) as DeviceCollection;
            //PopulateSubList(branch, list);
            return list;
        }
        public DeviceCollection SelectChangedRecByBranch(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectChangedRecByBranch", branch) as DeviceCollection;
            //PopulateSubList(branch, list);
            return list;
        }

        public DeviceCollection SelectCYRec(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectCYRec", branch) as DeviceCollection;
            return list;
        }

        public DeviceCollection SelectAddedCYRec(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectAddedCYRec", branch) as DeviceCollection;
            return list;
        }

        /* one time operation for CY data, will be removed very soon */
        public DeviceCollection SelectCYRecInDR(string branch)
        {
            DeviceCollection list = Mapper().QueryForList(
                "Device.SelectCYRecInDR", branch) as DeviceCollection;
            return list;
        }

        /* one time operation for CY data, will be removed very soon */
        public void UpdateBranchToCY(string branch)
        {
            Mapper().Update("Device.UpdateBranchToCY", branch);
        }

        /// <summary>
        /// delete
        /// </summary>
        /// <param name="tn"></param>
        public void Delete(int tn)
        {
            Mapper().Delete("Device.Delete", tn);
        }

        public void DeleteIDOnly(string id)
        {
            Mapper().Delete("Device.DeleteIDOnly", id);
        }

        public void DeleteBranch(string branch)
        {
            Mapper().Delete("Device.DeleteOldBVDrRec", branch);
        }

        public void DeleteTempBranch()
        {
            Mapper().Delete("Device.DeleteTempBranch", null);
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deviceList"></param>
        public void Insert(string id, DeviceCollection deviceList)
        {
            foreach (Device device in deviceList)
                device.ID = id;

            BulkInsert(deviceList);
        }

        public void Insert(int tn, DeviceCollection deviceList)
        {
            foreach (Device device in deviceList)
                device.TN = tn;

            BulkInsert(deviceList);
        }


        public int UpdateTempBranch(string branch)
        {
            return Mapper().Update("Device.UpdateTempBranch", branch);
        }

        /////////////////////////////////////////////////////////////////////
        ////
        //// private
        ////

        /// <summary>
        /// dr and 5 relation tables
        /// dr_function for id, dr_function for tn, dr_coutnry, dr_deviceType
        /// dr_opInfo
        /// </summary>
        /// <param name="deviceList"></param>
        private void BulkInsert(DeviceCollection deviceList)
        {
            int startRID = (int)Mapper().QueryForObject(
                "Device.GetNextRID", null);

            ///
            /// dr
            ///
            DeviceTableGenerator drTableGenerator = new DeviceTableGenerator();

            List<string> drTableList = 
                drTableGenerator.GetLocalTable(startRID, deviceList);
            foreach (string table in drTableList)
                Mapper().Insert("Device.BulkInsert", table);

            ///
            /// 5 relation tables
            ///
            RelationTableGenerator relationTableGenerator = 
                new RelationTableGenerator();

            List<string> dridTableList = relationTableGenerator.GetLocalTable(
                drTableGenerator.DRIDList);
            foreach (string table in dridTableList)
                Mapper().Insert("Device.ToFunctionBulk", table);

            List<string> drtnTableList = relationTableGenerator.GetLocalTable(
                drTableGenerator.DRTNList);
            foreach (string table in drtnTableList)
                Mapper().Insert("Device.ToFunctionBulk", table);

            List<string> countryTableList = relationTableGenerator.GetLocalTable(
                drTableGenerator.CountryList);
            foreach (string table in countryTableList)
                Mapper().Insert("Device.ToCountryBulk", table);

            List<string> deviceTypeTableList = relationTableGenerator.GetLocalTable(
                drTableGenerator.DeviceTypeList);
            foreach (string table in deviceTypeTableList)
                Mapper().Insert("Device.ToDeviceTypeBulk", table);

            List<string> opInfoTableList = relationTableGenerator.GetLocalTable(
                drTableGenerator.OpInfoList);
            foreach (string table in opInfoTableList)
                Mapper().Insert("Device.ToOpInfoBulk", table);
        }

        private void PopulateSubList(string id, DeviceCollection deviceList)
        {
            CountryCollection CountryList =
                DAOFactory.Misc().CountrySelect(id);

            DeviceTypeCollection DeviceTypeList =
                DAOFactory.Misc().DeviceTypeSelect(id);

            OpInfoCollection OpInfoList = new OpInfoCollection();

            foreach (Device device in deviceList)
            {
                device.CountryList = DAOFactory.Misc().GetCountryList(
                    device.RID, CountryList);

                device.DeviceTypeList = DAOFactory.Misc().GetDeviceTypeList(
                    device.RID, DeviceTypeList);

                device.OpInfoList = DAOFactory.Misc().GetOpInfoList(
                    device.RID, OpInfoList);
            }
        }
        //for PGS
        //Updated by satyadeep on 04/03/2013
        private void PopulateSubList_PGS(String id, DeviceCollection deviceList)
        {
            CountryCollection CountryList =
                DAOFactory.Misc().CountrySelect(id);

            DeviceTypeCollection DeviceTypeList =
                DAOFactory.Misc().DeviceTypeSelect_PGS(id);

            OpInfoCollection OpInfoList = new OpInfoCollection();

            foreach(Device device in deviceList)
            {
                device.CountryList = DAOFactory.Misc().GetCountryList(
                    device.RID, CountryList);

                device.DeviceTypeList = DAOFactory.Misc().GetDeviceTypeList(
                    device.RID, DeviceTypeList);

                device.OpInfoList = DAOFactory.Misc().GetOpInfoList(
                    device.RID, OpInfoList);
            }
        }

       

        private void PopulateSubList(int tn, DeviceCollection deviceList)
        {
            CountryCollection CountryList =
                DAOFactory.Misc().CountrySelect(tn);

            DeviceTypeCollection DeviceTypeList =
                DAOFactory.Misc().DeviceTypeSelect(tn);

            OpInfoCollection OpInfoList =
                DAOFactory.Misc().OpInfoSelect(tn);

            foreach (Device device in deviceList)
            {
                device.CountryList = DAOFactory.Misc().GetCountryList(
                    device.RID, CountryList);

                device.DeviceTypeList = DAOFactory.Misc().GetDeviceTypeList(
                    device.RID, DeviceTypeList);

                device.OpInfoList = DAOFactory.Misc().GetOpInfoList(
                    device.RID, OpInfoList);
            }
        }

        public DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            return Mapper().QueryForList("DeviceType.SelectCompleteDeviceTypes", param) as DeviceTypeCollection;
        }

    }
}
