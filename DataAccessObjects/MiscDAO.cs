using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using UEIException;

namespace BusinessObject
{
    public class MiscDAO : DAO
    {
        public Country GetCountry(int code)
        {
            return Mapper().QueryForObject("Country.SelectByCode", code)
                as Country;
        }

        #region Change Ids Intimation
        public HashtableCollection GetChangedIds(String strStartDate, String strEndDate, Int32 intStatusFlag)
        {
            Hashtable ht = new Hashtable();
            ht.Add("StartDate", strStartDate);
            ht.Add("EndDate", strEndDate);
            ht.Add("StatusFlag", intStatusFlag);
            
            //It is used to provide the flag as whether 0 or 1. It is mandatory input.
            //0 � Latest Modifications
            //1 � All  Modifications

            return Mapper().QueryForList("Misc.GetChangedIds", ht) as HashtableCollection;
        }
        #endregion

        public CountryCollection GetAllCountryList()
        {
            return Mapper().QueryForList("Country.Select", 0)
                as CountryCollection;
        }
        //Added by binil on May 09,2012
        public DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            return Mapper().QueryForList("DeviceType.SelectCompleteDeviceTypes", param) as DeviceTypeCollection;
        }

        public DeviceTypeCollection GetAllDeviceTypeList()
        {
            return Mapper().QueryForList("DeviceType.Select", 0)
                as DeviceTypeCollection;
        }

        public BrandCollection GetAllBrandList()
        {
            return Mapper().QueryForList("Brand.Select", 0)
                as BrandCollection;
        }

        public IList GetBrandList(string mode, string market, string branch)
        {
            Hashtable param = new Hashtable();

            param.Add("Market", market);
            param.Add("Branch", branch);
            param.Add("Mode", mode);
            return Mapper().QueryForList("Brand.GetBrandList2", param);
        }

        public OpInfoCollection GetAllOpInfoList()
        {
            return Mapper().QueryForList("OpInfo.Select", 0)
                as OpInfoCollection;
        }


        /// <summary>
        /// Optimized sub tables for DR, by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CountryCollection CountrySelect(string id)
        {
            return Mapper().QueryForList("Country.SelectByID", id)
                as CountryCollection;
        }

        public DeviceTypeCollection DeviceTypeSelect(string id)
        {
            return Mapper().QueryForList("DeviceType.SelectByID", id)
                as DeviceTypeCollection;
        }
        //for PGS
        //Updated by Satyadeep on 04/03/2013
        public DeviceTypeCollection DeviceTypeSelect_PGS(string id)
        {
            return Mapper().QueryForList("DeviceType.SelectByID_PGS", id)
                as DeviceTypeCollection;
        }

        public OpInfoCollection OpInfoSelect(string id)
        {
            return Mapper().QueryForList("OpInfo.SelectByID", id)
                as OpInfoCollection;
        }

        /// <summary>
        /// Optimized sub tables for DR, by TN
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public CountryCollection CountrySelect(int tn)
        {
            return Mapper().QueryForList("Country.SelectByTN", tn)
                as CountryCollection;
        }

        public DeviceTypeCollection DeviceTypeSelect(int tn)
        {
            return Mapper().QueryForList("DeviceType.SelectByTN", tn)
                as DeviceTypeCollection;
        }

        public OpInfoCollection OpInfoSelect(int tn)
        {
            return Mapper().QueryForList("OpInfo.SelectByTN", tn)
                as OpInfoCollection;
        }

        /// <summary>
        /// Get list by dr rid
        /// </summary>
        /// <param name="rid"></param>
        /// <param name="countryList"></param>
        /// <returns></returns>
        public CountryCollection GetCountryList(int rid,
         CountryCollection countryList)
        {
            CountryCollection countryListByRID = new CountryCollection();
            foreach (Country country in countryList)
            {
                if (country.DR_RID == rid)
                {
                    countryListByRID.Add(country);
                }
            }

            return countryListByRID;
        }

        public DeviceTypeCollection GetDeviceTypeList(int rid,
           DeviceTypeCollection deviceTypeList)
        {
            DeviceTypeCollection deviceTypeListByRID =
                new DeviceTypeCollection();

            foreach (DeviceType deviceType in deviceTypeList)
            {
                if (deviceType.DR_RID == rid)
                {
                    deviceTypeListByRID.Add(deviceType);
                }
            }

            return deviceTypeListByRID;
        }

        public OpInfoCollection GetOpInfoList(int rid,
         OpInfoCollection opInfoList)
        {
            OpInfoCollection opInfoListByRID = new OpInfoCollection();
            foreach (OpInfo opInfo in opInfoList)
            {
                if (opInfo.DR_RID == rid)
                {
                    opInfoListByRID.Add(opInfo);
                }
            }

            return opInfoListByRID;
        }

        /// <summary>
        /// Don't insert if not exist throw exception
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetBrandCode(string name)
        {
            Object code = Mapper().QueryForObject("Brand.GetCode", name);

            if (code == null)
            {
                string brand = "null";
                if (name != null)
                    brand = name;

                throw new BrandReferenceException(name + " brand doesn't exist!");
            }

            return (int)code;
        }

        public int GetCountryCode(Country country)
        {
            Object code = Mapper().QueryForObject(
                "Country.GetCode", country);
 
            if (code == null)
            {
                throw new CountryReferenceException(String.Format(
                    "Country {0} in Region {1} doesn't exist!",
                    country.Name, country.Region));
            }

            return (int)code;
        }

        public int GetDeviceTypeCode(DeviceType deviceType)
        {
            Object code = Mapper().QueryForObject(
                "DeviceType.GetCode", deviceType);
 
            if (code == null)
            {
                throw new DeviceTypeReferenceException(String.Format(
                    "Device Type {0} doesn't exist!", deviceType.Name));
            }

            return (int)code;
        }
        
        public string GetDeviceTypeMode(DeviceType deviceType)
        {
            Object mode = Mapper().QueryForObject(
                "DeviceType.GetMode", deviceType);

            if (mode == null)
            {
                //throw new DeviceTypeReferenceException(String.Format(
                //    "Device Type {0} didn't find a mode!", deviceType.Name));
                return "";
            }

            return mode.ToString();
        }

        public int GetOpInfoCode(OpInfo opInfo)
        {
            Object code = Mapper().QueryForObject(
                "OpInfo.GetCode", opInfo);
 
            if (code == null)
            {
                throw new OpInfoReferenceException(String.Format(
                    "Information {0} for Operation {1} doesn't exist!",
                    opInfo.Operation, opInfo.Information));
            }

            return (int)code;
        }

        /// <summary>
        /// only for intron, intronDictionary, pdlTemplate, pickTemplate, protocolPriority
        /// nopData tables export
        /// 
        /// dynamic query selection depends on table name 
        /// e.g.) Misc.intronSelect for intron
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public HashtableCollection SelectAll(
            string tableName, string dbName)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ColumnList", GetColumnList(tableName));
            paramList.Add("TableName", tableName);
            paramList.Add("DBName", dbName);

            return Mapper().QueryForList(
                "Misc." + tableName + "Select", paramList)
                as HashtableCollection;
        }

        public HashtableCollection GetKeyTemplate()
        {
            return Mapper().QueryForList("Misc.GetKeyTemplate", null) as HashtableCollection;
        }
        public HashtableCollection GetKeyLabelIntrons()
        {
            return Mapper().QueryForList("Misc.GetKeyLabelIntrons", null) as HashtableCollection;
        }
        /// <summary>
        /// only for intron, intronDictionary, pdlTemplate, pickTemplate, protocolPriority
        /// nopData tables export
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public HashtableCollection SelectAll(string tableName)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ColumnList", GetColumnList(tableName));
            paramList.Add("TableName", tableName);

            return Mapper().QueryForList(
                "Misc." + tableName + "Select", paramList)
                   as HashtableCollection;
        }

        /// <summary>
        /// !!! WARNING this will delete the target table !!!
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dbName"></param>
        public void DeleteAll(string tableName, string dbName)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("TableName", tableName);
            paramList.Add("DBName", dbName);

            Mapper().Delete("Misc.Delete", paramList);
        }

        /// <summary>
        /// truncate dest table first
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="destDBName"></param>
        /// <param name="srcDBName"></param>
        public void Transfer(string tableName,
            string destDBName, string srcDBName)
        {
            DeleteAll(tableName, destDBName);

            Hashtable paramList = new Hashtable();
            paramList.Add("ColumnList", GetColumnList(tableName));
            paramList.Add("TableName", tableName);
            paramList.Add("DestDBName", destDBName);
            paramList.Add("SrcDBName", srcDBName);

            Mapper().Insert("Misc.Transfer", paramList);
        }

        /// <summary>
        /// Get a list of columns - meant to exclude identity columns
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private string GetColumnList(string tableName)
        {
            switch (tableName.ToLower())
            {
                case "intron": return "Label, Intron, Category";
                case "introndictionary": return "Mode, Label, Intron";
                case "nop_data": return "Executor_Code, Nop_Data";
                case "pdl_template": return "Mode, Country_Code, Key_Name, " +
                     "Sorting_Order, Popular, Outron, Intron, Pick_Priority";
                case "pick_template": return "Device, Key_Name, Sorting_Order, " +
                     "Popular, Firmware_Label, Intron, Pick_Priority";
                case "protocol_priority": return "ID, Country_Code, PriorityInMode, MarketCoverage";

                default: return "*";
            }
        }

        /// <summary>
        ///  this is used for second part of id update in id header
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ProtocolPriorityDoesExist(string id)
        {
            if (Mapper().QueryForObject(
                "Misc.Protocol_PrioritySelectID", id) == null)
                return false;
            else
                return true;
        }

        /// <summary>
        ///  this is used for third check of id update in id header
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool LogWebModelDoesExist(string id)
        {
            if (Mapper().QueryForObject(
                "Misc.LogWebModelSearchID", id) == null)
                return false;
            else
                return true;
        }

        public byte[] GetZipIRBinary(string projectName, string id)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("projectName", projectName);
            paramList.Add("id", id);
            return Mapper().QueryForObject<byte[]>("Misc.GetZipIRBinary", paramList);
        }

        public string GetAirConDescriptorData(string id, string dataType, int version)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);
            paramList.Add("DataType", dataType);
            paramList.Add("Version", version);
            return Mapper().QueryForObject<string>("Misc.GetAirconDescriptorData", paramList);
        }

        #region Get All Standard, Alias and Veriation Brands
        public HashtableCollection GetAllBrands(IDSearchParameter param)
        {
            HashtableCollection brandList = Mapper().QueryForList("Misc.GetAllBrands", param) as HashtableCollection;
            return brandList;
        }
        #endregion

        #region Key Function Count Report
        public HashtableCollection GetKeyFunction(IDSearchParameter param)
        {
            HashtableCollection keyfunctionList = Mapper().QueryForList("Misc.GetKeyFunctions", param) as HashtableCollection;
            return keyfunctionList;
        }
        #endregion

        #region EdID XBox
        public IList<Hashtable> GetXBoxEdIDData()
        {
            IList<Hashtable> edidList = Mapper().QueryForList<Hashtable>("Misc.GetXBoxEdIdData", null);
            return edidList;
        }
        public IList<Hashtable> GetXBoxEdIDDataEx()
        {
            IList<Hashtable> edidList = Mapper().QueryForList<Hashtable>("Misc.GetXBoxEdIdDataEx", null);
            return edidList;
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String edidVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("EdIdRID", edidRID);
            _ht.Add("FileStructure", m_FileStructure);
            _ht.Add("EdIdVersion", edidVersion);
            return Mapper().QueryForObject<String>("Misc.UpdateXBoxEdIdData", _ht);
        }
        public String UpdateEdIdXBoxData(Int32 edidRID, Byte[] m_FileStructure, String customFingerPrint, String customFPPlusOSD, String str128FP, String str128FPPlusOSD, String osdData, String osdFP, String bytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3, String strPhysicalAddress)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("EdIdRID", edidRID);
            _ht.Add("FileStructure", m_FileStructure);
            _ht.Add("CustomFingerPrint", customFingerPrint);
            _ht.Add("CustomFPPlusOSD", customFPPlusOSD);
            _ht.Add("128FP", str128FP);
            _ht.Add("128FPPlusOSD", str128FPPlusOSD);
            _ht.Add("OSDData", osdData);
            _ht.Add("OSDFP", osdFP);
            _ht.Add("BytePosition", bytePosition);
            _ht.Add("CustomFingerPrint0", strCustomFingerPrint0);
            _ht.Add("CustomFingerPrint1", strCustomFingerPrint1);
            _ht.Add("CustomFingerPrint2", strCustomFingerPrint2);
            _ht.Add("CustomFingerPrint3", strCustomFingerPrint3);
            _ht.Add("PhysicalAddress", strPhysicalAddress);
            return Mapper().QueryForObject<String>("Misc.UpdateXBoxEdIdDataEx", _ht);
        }
        #endregion

        #region EdId
        public String UpdateEdIdData(String m_MainDevice, String m_SubDevice, String m_Component, String m_Brand, String m_Model, String m_Countries, Byte[] m_FileStructure, String m_Description, Int32 m_Flag, String Base64EDID, String CustomFingerPrint, String CustomFPPlusOSD, String Only128FP, String Only128FPPlusOSD, String OSDData, String OSDFP, String BytePosition, String strCustomFingerPrint0, String strCustomFingerPrint1, String strCustomFingerPrint2, String strCustomFingerPrint3)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("MainDevice", m_MainDevice);
            _ht.Add("SubDevice", m_SubDevice);
            //_ht.Add("DeviceComponent", m_Component);
            _ht.Add("Brand", m_Brand);
            _ht.Add("Model", m_Model);
            _ht.Add("Countries", m_Countries);
            _ht.Add("FileStructure", m_FileStructure);
            //_ht.Add("Description", m_Description);
            _ht.Add("Base64EDID", Base64EDID);
            _ht.Add("CustomFingerPrint", CustomFingerPrint);
            _ht.Add("CustomFPPlusOSD", CustomFPPlusOSD);
            _ht.Add("128FP", Only128FP);
            _ht.Add("128FPPlusOSD", Only128FPPlusOSD);
            _ht.Add("OSDData", OSDData);
            _ht.Add("OSDFP", OSDFP);
            _ht.Add("BytePosition", BytePosition);           
            _ht.Add("Flag", m_Flag);
            _ht.Add("CustomFingerPrint0", strCustomFingerPrint0);
            _ht.Add("CustomFingerPrint1", strCustomFingerPrint1);
            _ht.Add("CustomFingerPrint2", strCustomFingerPrint2);
            _ht.Add("CustomFingerPrint3", strCustomFingerPrint3);
            return Mapper().QueryForObject<String>("Misc.UpdateEdIdData", _ht);
        }
        public HashtableCollection GetEdIdData(IDSearchParameter param)
        {
            HashtableCollection edidList = Mapper().QueryForList("Misc.GetEdIdData", param) as HashtableCollection;
            return edidList;
        }
        #endregion

        #region DBMIntegrations
        //AddedOn11/17/2010-Binil -code added as part of integrating DBM with workflow

        public HashtableCollection GetAllIntrons(string Intron, string id)
        {
            Hashtable param = new Hashtable();
            param.Add("Intron", Intron);
            param.Add("Id", id);
            HashtableCollection labellist = Mapper().QueryForList("Misc.GetAllIntrons", param) as HashtableCollection;
            return labellist;
        }

        public HashtableCollection GetallLabels(string label, string id)
        {
            Hashtable param = new Hashtable();
            param.Add("Label", label);
            param.Add("Id", id);
            HashtableCollection labellist = Mapper().QueryForList("Misc.GetAllLabels_Ex", param) as HashtableCollection;
            return labellist;
        }

        public LabelCollection GetAllLabels(string label)
        {
            //IList labellist = Mapper().QueryForList("Misc.GetAllLabels", label);
            LabelCollection _labellist = Mapper().QueryForList("Misc.GetAllLabels", label) as LabelCollection;
            List<string> labels = new List<string>();
            foreach (Labels lbl in _labellist)
            {
                labels.Add(lbl.Keylabel);
            }
            return _labellist;
        }

        //Added by Satyadeep on Sep 02 2010
        //Gives List of All regions
        public CountryCollection GetAllRegionList()
        {
            return Mapper().QueryForList("Country.SelectAllRegions", 0) as CountryCollection;
        }
        public CountryCollection GetLocationList()
        {            
            return Mapper().QueryForList("Country.SelectAllLocations", 0) as CountryCollection;
        }
        //Added By Satyadeep on April 04 2012
        public CountryCollection GetAllSubRegionList(IDSearchParameter param)
        {
            return Mapper().QueryForList("Country.SelectAllSubRegions", param) as CountryCollection;
        }
        //Commented on june 27 as part of implementation of sp for getting countries
        //Added by Satyadeep on Sep 02 1010
        //Gives List of Countries based on Region
        //public CountryCollection GetAllCountryList(StringCollection regionList)
        //{
        //    IDSearchParameter param = new IDSearchParameter();
        //    param.RegionList = regionList;
        //    return Mapper().QueryForList("Country.SelectAllCountries", param) as CountryCollection;
        //}

        public CountryCollection GetAllCountryList(IDSearchParameter param)
        {
            return Mapper().QueryForList("Country.SelectAllCountries", param) as CountryCollection;
        }

        //Added By Satyadeep on Sep 02 2010
        //Gives List of All Device Type List by Region and Country
        public DeviceTypeCollection GetAllDeviceTypeList(IDSearchParameter param)
        {            
            return Mapper().QueryForList("DeviceType.SelectAllDeviceTypes", param) as DeviceTypeCollection;
        }
        public DeviceTypeCollection GetAllSubDeviceTypeList(IDSearchParameter param)
        {
            return Mapper().QueryForList("DeviceType.SelectAllSubDeviceTypes", param) as DeviceTypeCollection;
        }
        public DeviceTypeCollection GetAllComponentList(IDSearchParameter param)
        {
            return Mapper().QueryForList("DeviceType.SelectAllComponents", param) as DeviceTypeCollection;
        }
        //Added by Satyadeep on Sep 02 2010
        //Gives List of all Data Source Location 
        public StringCollection GetDataSourceLocation()
        {
            return DAOFactory.AdHoc().SelectString("Misc.DataSourcesSelect", null) as StringCollection;
        }

        #region ID/BrandReport-DBM
        public IDBrandResultCollection GetIDBrandRecords(IDSearchParameter param)
        {
            return Mapper().QueryForList("Misc.GetIDBrandCollection", param) as IDBrandResultCollection;
        }
        #endregion

        #region BSC ID List
        public IDBrandResultCollection GetAllBSCIDList(IDSearchParameter param)
        {
            return Mapper().QueryForList("Misc.GetBSCIDCollection", param) as IDBrandResultCollection;
        }
        #endregion

        #region Brand Based Search
        public IDBrandResultCollection GetBrandSearchList(IDSearchParameter param)
        {
            //using BSC procedure temporary
            return Mapper().QueryForList("Misc.GetBrandSearchReport", param) as IDBrandResultCollection;
        }
        #endregion

        #region Model Info Report
        public IDBrandResultCollection GetModelInformation(IDSearchParameter param)
        {            
            //with xml params
            return Mapper().QueryForList("Misc.GetModelInfoReport", param) as IDBrandResultCollection;
        }
       
        #endregion

        #region Get Remote Images
        public IList<String> GetRemoteImages(Int32 tnNumber)
        {
            return Mapper().QueryForList<String>("Misc.SelectRemoteImages", tnNumber);
        }
        #endregion

        public IList<Int32> SelectExecutorCodes()
        {
            return Mapper().QueryForList<Int32>("Misc.ExecutorCodes", null);
        }
        public IDBrandResultCollection GetModelInformation(string id)
        {
            return Mapper().QueryForList("Misc.ModelInfoById", id) as IDBrandResultCollection;
        }
        #endregion       

        #region ExecIdMap

        public HashtableCollection GetExecIdMap(IList<String> idList)
        {
            //return Mapper().QueryForList("Misc.ExecIdMapSelect", idList) as HashtableCollection;
            StringBuilder builder = new StringBuilder();
            builder.Append("'");
            foreach (String id in idList)
                builder.Append(id + ",");

            if (builder.Length > 0)
                builder.Remove(builder.Length - 1, 1);//remove ',' from the end

            builder.Append("'");

            return Mapper().QueryForList("Misc.ExecIdMapSelect", builder.ToString()) as HashtableCollection;
        }

        #endregion


        #region Consolidate Reports

        public int TotalMainDevices()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalMainDevices", null);

            return (int)totalCount;
        }

        public int TotalSubDevices()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalSubDevices", null);

            return (int)totalCount;
        }
        public int TotalDeviceTypes()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalDeviceTypes", null);

            return (int)totalCount;
        }
        public int TotalDeviceModels()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalDeviceModels", null);

            return (int)totalCount;
        }
        public int TotalRemoteModels()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalRemoteModels", null);

            return (int)totalCount;
        }
        public int TotalKeyFunctions()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalUniqueKeyFunctions", null);

            return (int)totalCount;
        }
        public int TotalKeyCount()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalKeyCount", null);

            return (int)totalCount;
        }
        public int TotalExecutors()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalExecutors", null);

            return (int)totalCount;
        }
        public int TotalUsedExecutors()
        {
            object totalCount = Mapper().QueryForObject("Misc.TotalUsedExecutors", null);

            return (int)totalCount;
        }
        public HashtableCollection TotalBrandCounts()
        {
             return Mapper().QueryForList("Misc.TotalBrandCounts", null) as HashtableCollection;

        }
        #endregion

        #region QUERIES FOR LOAD REQUEST TOOL

        //public string InsertNewProject(string projecttype,string platform,string templatename,int version,int romsize,int systemflag,string description)
        //{
        //    Hashtable ht = new Hashtable();
        //    ht.Add("ProjectType",projecttype);
        //    ht.Add("Platform",platform);
        //    ht.Add("TemplateName",templatename);
        //    ht.Add("Version",version);
        //    ht.Add("RomSize",romsize);
        //    ht.Add("SystemFlag",systemflag);
        //    ht.Add("Description",description);
        //    return Mapper().QueryForObject("Misc.InsertTemplateHeader", ht);
        //}

        public IList<Hashtable> IsTemplatePresent(string projectname)
        {
            Hashtable ht = new Hashtable();
            ht.Add("TemplateName", projectname);
            return Mapper().QueryForList<Hashtable>("Misc.IsTemplatePresent", ht);
        }

        public int GetProjectVersion(string projectName)
        {
            if (!String.IsNullOrEmpty(projectName))
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", projectName);
                try
                {
                    return Mapper().QueryForObject<Int32>("Misc.GetProjectVersion", ht);
                }
                catch (NullReferenceException)
                {
                    return -1;
                }
                catch
                {
                    throw;
                }
            }

            return -1;
        }

        public int GetTemplateVersion(string projectName,double projectVersion,string templateName)
        {
            if (!String.IsNullOrEmpty(projectName))
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName",projectName);
                ht.Add("ProjectVersion", projectVersion);
                ht.Add("TemplateName", templateName);

                try
                {
                    return Mapper().QueryForObject<Int32>("Misc.GetTemplateVersion", ht);
                }
                catch (NullReferenceException)
                {
                    return -1;
                }
                catch
                {
                    throw;
                }
            }

            return -1;
        }

        public int ValidateTemplate(string projectname)
        {
            Hashtable ht = new Hashtable();
            ht.Add("TemplateName", projectname);


            if (Mapper().QueryForObject("Misc.ValidateProject", ht) == null)
                return 0;
            else
                return 1;
        }
        public IList<Hashtable> GetAllLoadTemplates()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetAllLoadTemplates", null);
        }

        public IList<Hashtable> GetTemplateHeaderInfo(string templatename, string version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", templatename);
            ht.Add("Version", version);

            return Mapper().QueryForList<Hashtable>("Misc.GetTemplateHeaderInfo", ht);
        }
        public IList<Hashtable> GetRegionInfo(string templatename, string version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", templatename);
            ht.Add("Version", version);

            return Mapper().QueryForList<Hashtable>("Misc.GetRegionInfo", ht);
        }

        public IList<Hashtable> GetTemplateLocations(string projectName, int projectVersion, string templatename, int templateVersion)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", projectName);
                ht.Add("ProjectVersion", projectVersion);
                ht.Add("TemplateName", templatename);
                ht.Add("TemplateVersion", templateVersion);

                return Mapper().QueryForList<Hashtable>("Misc.GetTemplateLocations", ht);
            }
            catch
            {
                throw;
            }

        }

        public IList<Hashtable> GetCountryInfo(string templatename, string version, string region)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", templatename);
            ht.Add("Version", version);
            ht.Add("Region", region);

            return Mapper().QueryForList<Hashtable>("Misc.GetCountriesInfo", ht);
        }
        public IList<Hashtable> GetDataSourceInfo(string templateName, string templateVersion)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Modified by binil on Oct 03,2012
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public IList<Hashtable> GetDataSourceInfo(string projectName,int projectVersion,string templatename, int templateVersion)
        {
            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectName);
            ht.Add("ProjectVersion", projectVersion);
            ht.Add("TemplateName", templatename);
            ht.Add("TemplateVersion", templateVersion);

            return Mapper().QueryForList<Hashtable>("Misc.GetDataSourceInfo", ht);
        }
        public IList<Hashtable> GetDeviceTypeInfo(string ProjectName, int ProjectVersion, string TemplateName,
            int TemplateVersion,string projectDevices,string modes)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", ProjectName);
                ht.Add("ProjectVersion", ProjectVersion);
                ht.Add("TemplateName", TemplateName);
                ht.Add("TemplateVersion", TemplateVersion);
                ht.Add("ProjectDevices", projectDevices);
                ht.Add("Modes", modes);

                return Mapper().QueryForList<Hashtable>("Misc.GetDeviceTypeInfo", ht);
            }
            catch
            {
                throw;
            }
        }
        public IList<Hashtable> GetProjectDevices(string templatename, string version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", templatename);
            ht.Add("Version", version);

            return Mapper().QueryForList<Hashtable>("Misc.GetProjectDevices", ht);
        }

        public IList<Hashtable> GetDeviceModes(string templatename, string version, string Device)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Name", templatename);
            ht.Add("Version", version);
            ht.Add("Device", Device);

            return Mapper().QueryForList<Hashtable>("Misc.GetDeviceModes", ht);
        }

        public IList<Hashtable> GetAllProjectTypes()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetAllProjectTypes", null);
        }

        public IList<Hashtable> GetAllHardwarePlatforms()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetAllHardwarePlatforms", null);
        }
        public IList<Hashtable> GetAllSupportedPlatforms()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetAllPlatforms", null);
        }
        public IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<String> platforms)
        {                                   
            IList<Hashtable> htclist = Mapper().QueryForList<Hashtable>("Misc.GetIdsForPlatforms", platforms);
            return htclist;
        }
        public IList<Hashtable> GetRomSize(string platform)
        {
            Hashtable ht = new Hashtable();
            ht.Add("Platform", platform);
            return Mapper().QueryForList<Hashtable>("Misc.GetAllRomSizes", ht);
        }
        /// <summary>
        /// For getting the mandatory brands and models.
        /// </summary>
        /// <remarks>Added by binil on Oct16,2012</remarks>
        /// <param name="inputObject"></param>
        /// <returns></returns>
        public IList<Hashtable> GetBrandModelXml(string projectName, int projectVersion, string templateName, int templateVersion, string projectDevice)
        {
            try
            {
                Hashtable input = new Hashtable();
                input.Add("ProjectName", projectName);
                input.Add("ProjectVersion", projectVersion);
                input.Add("TemplateName", templateName);
                input.Add("TemplateVersion", templateVersion);
                input.Add("ProjectDevice", projectDevice);

                IList<Hashtable> result = Mapper().QueryForList<Hashtable>("Misc.GetBrandModelXml", input);

                return result;
            }
            catch
            {
                throw;
            }
        }

        public void InsertTemplateHeader(string projectType, string hardwareplatform, string templatename,int romsize, int systemflag, string description)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectType", projectType);
                ht.Add("Platform", hardwareplatform);
                ht.Add("TemplateName", templatename);
                ht.Add("RomSize", romsize);
                ht.Add("SystemFlag", systemflag);
                ht.Add("Description", description);
                //ht.Add("UpdateStatus", updatestatus);

                //System.Guid  retVal = (System.Guid)Mapper().QueryForObject("Misc.InsertTemplateHeader", ht);
                Hashtable result = Mapper().QueryForObject<Hashtable>("Misc.InsertTemplateHeader", ht);
                //return retVal.ToString();

                if (result != null && result["ErrorMessage"] != null && result["ErrorMessage"].ToString() != String.Empty)
                {
                    Exception customEx = new Exception(result["ErrorMessage"].ToString());
                    customEx.Data.Add("ErrorProcedure", result["ErrorProcedure"].ToString());

                    throw customEx;
                }
            }
            catch
            {
                throw;
            }
        }

        public void InsertLoadTemplate(string projectname, string templatename, double version, int appcodesize, int flag, string description, int updatestatus)
        {

            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("TemplateName", templatename);
            ht.Add("ProjectVersion", version);
            ht.Add("ApplicationCodeSize", appcodesize);
            ht.Add("SystemFlag", flag);
            ht.Add("Description", description);
            ht.Add("UpdateStatus", updatestatus);

            //System.Guid  retVal = (System.Guid)Mapper().QueryForObject("Misc.InsertTemplateHeader", ht);
            IList<Hashtable> _result = Mapper().QueryForList<Hashtable>("Misc.InsertLoadTemplate", ht);
            //return retVal.ToString();
        }


        public void InsertRegion(string region, string projectname, int version, int locid, double weight, int systemflag)
        {

            Hashtable ht = new Hashtable();

            ht.Add("ProjectName", projectname);
            ht.Add("ProjectVersion", version);
            ht.Add("RegionName", region);
            ht.Add("LocationRId", locid);
            ht.Add("Override", weight);
            ht.Add("SystemFlag", systemflag);


            //System.Guid  retVal = (System.Guid)Mapper().QueryForObject("Misc.InsertTemplateHeader", ht);
            IList<Hashtable> _result = Mapper().QueryForList<Hashtable>("Misc.InsertRegion", ht);







        }

        public void InsertTemplateLocations(string ProjectName, int ProjectVersion, string TemplateName,int TemplateVersion,string locationXml)
        {//InsertTemplateLocations
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", ProjectName);
                ht.Add("ProjectVersion", ProjectVersion);
                ht.Add("TemplateName", TemplateName);
                ht.Add("TemplateVersion", TemplateVersion);
                ht.Add("LocationXml", locationXml);

                Hashtable result = Mapper().QueryForObject<Hashtable>("Misc.InsertTemplateLocations", ht);

                if (result != null && result["ErrorMessage"] != null && result["ErrorMessage"].ToString() != String.Empty)
                {
                    Exception customEx = new Exception(result["ErrorMessage"].ToString());
                    customEx.Data.Add("ErrorProcedure", result["ErrorProcedure"].ToString());

                    throw customEx;
                }

            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Added by binil on Sept 28,2012
        /// </summary>
        /// <param name="DeviceName"></param>
        /// <param name="Ordinal"></param>
        /// <param name="ProjectName"></param>
        /// <param name="ProjectVersion"></param>
        /// <param name="TemplateName"></param>
        /// <param name="TemplateVersion"></param>
        /// <param name="SystemFlag"></param>
        /// <param name="UpdateStatus"></param>
        /// <param name="Description"></param>
        /// <returns></returns>
        public void InsertProjectDevices(string DeviceName,int Ordinal,string ProjectName,int ProjectVersion,string TemplateName,
            int TemplateVersion,int SystemFlag,int UpdateStatus,string Description)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("DeviceName", DeviceName);
                ht.Add("Ordinal", Ordinal);
                ht.Add("ProjectName", ProjectName);
                ht.Add("ProjectVersion", ProjectVersion);
                ht.Add("TemplateName", TemplateName);
                ht.Add("TemplateVersion", TemplateVersion);
                ht.Add("Override", UpdateStatus);
                ht.Add("SystemFlag", SystemFlag);
                ht.Add("Description", Description);

                Mapper().Insert("Misc.InsertProjectDevices", ht);
            }
            catch
            {
                throw;
            }

          
        }

        public void InsertDeviceModes(string ProjectName, int ProjectVersion, string TemplateName,
            int TemplateVersion, char mode, string modeName, string projectDevice, int ordinal, int SystemFlag, int UpdateStatus, string Description)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", ProjectName);
                ht.Add("ProjectVersion", ProjectVersion);
                ht.Add("TemplateName", TemplateName);
                ht.Add("TemplateVersion", TemplateVersion);
                ht.Add("ProjectDevice", projectDevice);
                ht.Add("Mode", mode.ToString());
                ht.Add("ModeName", modeName);
                ht.Add("Ordinal", ordinal);
                ht.Add("Override", UpdateStatus);
                ht.Add("SystemFlag", SystemFlag);
                ht.Add("Description", Description);

                Mapper().Insert("Misc.InsertDeviceModes", ht);
            }
            catch
            {
                throw;
            }
        }

        public void InsertDeviceTypes(string ProjectName, int ProjectVersion, string TemplateName,
            int TemplateVersion,string projectDevice,string Description, int UpdateStatus,string deviceXml)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", ProjectName);
                ht.Add("ProjectVersion", ProjectVersion);
                ht.Add("TemplateName", TemplateName);
                ht.Add("TemplateVersion", TemplateVersion);
                ht.Add("ProjectDevice", projectDevice);
                ht.Add("Override", UpdateStatus);
                //ht.Add("Description", Description);
                ht.Add("DeviceXml", deviceXml);

                Hashtable result= Mapper().QueryForObject<Hashtable>("Misc.InsertDeviceTypes",ht);

                if (result!=null && result["ErrorMessage"] != null && result["ErrorMessage"].ToString() != String.Empty)
                {
                    Exception customEx = new Exception(result["ErrorMessage"].ToString());
                    customEx.Data.Add("ErrorProcedure", result["ErrorProcedure"].ToString());

                    throw customEx;
                }
            }
            catch
            {
                throw;
            }

        }

        public void InsertProjectDevices(string devicename, int ordinal, string projectname, int version, int systemflag)
        {

            Hashtable ht = new Hashtable();
            ht.Add("DeviceName", devicename);
            ht.Add("Ordinal", ordinal);
            ht.Add("ProjectName", projectname);
            ht.Add("ProjectVersion", version);
            ht.Add("Override", 0);
            ht.Add("SystemFlag", systemflag);
            ht.Add("Description", string.Empty);



            //System.Guid  retVal = (System.Guid)Mapper().QueryForObject("Misc.InsertTemplateHeader", ht);
            IList<Hashtable> _result = Mapper().QueryForList<Hashtable>("Misc.InsertProjectDevices", ht);
        }
        //New method added. Need to delete.
        public void InsertDataSources(string projectname, int version, string datasource, int systemflag)
        {

            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("Version", version);
            ht.Add("DataSource", datasource);
            ht.Add("SystemFlag", systemflag);

            Mapper().QueryForList<Hashtable>("Misc.InsertDataSources", ht);
        }

        public void InsertDataSources(string projectname, int projectVersion,string templateName,int templateVersion, string datasource, int systemflag,int updateStatus)
        {

            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("ProjectVersion", projectVersion);
            ht.Add("TemplateName", templateName);
            ht.Add("TemplateVersion", templateVersion);
            ht.Add("DataSource", datasource);
            ht.Add("SystemFlag", systemflag);
            ht.Add("UpdateStatus", updateStatus);

            Mapper().Insert("Misc.InsertDataSources", ht);
            
        }

        public void UpdateLoadList(string projectname, int projectVersion, string templateName, int templateVersion, string projectDevices,string modes,string ids)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", projectname);
                ht.Add("ProjectVersion", projectVersion);
                ht.Add("TemplateName", templateName);
                ht.Add("TemplateVersion", templateVersion);
                ht.Add("ProjectDevices", projectDevices);
                ht.Add("Modes", modes);
                ht.Add("Ids", ids);

                Hashtable result = Mapper().QueryForObject<Hashtable>("Misc.UpdateLoadList",ht);
                if (result!=null && result["ErrorMessage"] != null && result["ErrorMessage"].ToString() != String.Empty)
                {
                    Exception customEx = new Exception(result["ErrorMessage"].ToString());
                    customEx.Data.Add("ErrorProcedure", result["ErrorProcedure"].ToString());

                    throw customEx;
                }
            }
            catch
            {
                throw;
            }
        }

        public IList<Hashtable> GetLoadList(string projectname, int projectVersion, string templateName, int templateVersion, string projectDevices, string modes)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("ProjectName", projectname);
                ht.Add("ProjectVersion", projectVersion);
                ht.Add("TemplateName", templateName);
                ht.Add("TemplateVersion", templateVersion);
                ht.Add("ProjectDevices", projectDevices);
                ht.Add("Modes", modes);

                return Mapper().QueryForList<Hashtable>("Misc.GetLoadList", ht);
            }
            catch
            {
                throw;
            }
        }
        public void DeleteRegions(string projectname, int version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("Version", version);


            Mapper().QueryForList<Hashtable>("Misc.DeleteRegions", ht);
        }

        public void DeleteDataSources(string projectname, int version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("Version", version);


            Mapper().QueryForList<Hashtable>("Misc.DeleteDataSources", ht);
        }

        public void DeleteProjectDevices(string projectname, int version)
        {
            Hashtable ht = new Hashtable();
            ht.Add("ProjectName", projectname);
            ht.Add("Version", version);


            Mapper().QueryForList<Hashtable>("Misc.ProjectDevices", ht);

        }

        #endregion

        #region Queries for ID Ranking
        public IList<Hashtable> GetContributorTypes()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetContributorTypes", null);
        }
        public IList<Hashtable> GetLocationGroups()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetLocationGroups", null);
        }
        public IList<Hashtable> GetLocationByNameID(String m_LocatioName, String m_LocationID)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationName", m_LocatioName);
            _ht.Add("LocationID", m_LocationID);
            return Mapper().QueryForList<Hashtable>("Misc.GetLocationByNameID", _ht);
        }
        public IList<Hashtable> GetContributorTypeParams(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("Name", m_CountrybutorTypeName);
            _ht.Add("Version", m_CountrybutorTypeVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetContributorTypeParams", _ht);
        }
        public IList<Hashtable> GetContributorInstances(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("Name", m_CountrybutorTypeName);
            _ht.Add("Version", m_CountrybutorTypeVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetContributorInstances", _ht);
        }
        public IList<Hashtable> GetContributorTypesForInstance(String m_InstanceName, String m_InstanceVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("Name", m_InstanceName);
            _ht.Add("Version", m_InstanceVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetContributorTypesForInstance", _ht);
        }
        public IList<Hashtable> CreateContributorInstanceParameter(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion, String m_InstanceName, String m_InstanceVersion, String m_ParameterName, Double m_ParameterValue, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("ParameterName", m_ParameterName);
            _ht.Add("Value", m_ParameterValue);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateContributorInstanceParameter", _ht);
        }
        public IList<Hashtable> UpdateContributorInstance(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion, String m_InstanceName, String m_InstanceVersion, String m_InstanceDescription, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("InstanceDescription", m_InstanceDescription);            
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateContributorInstance", _ht);
        }
        public IList<Hashtable> UpdateContributorInstanceParameter(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion, String m_InstanceName, String m_InstanceVersion, String m_ParameterName, Double m_ParameterValue, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("ParameterName", m_ParameterName);
            _ht.Add("Value", m_ParameterValue);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateContributorInstanceParameter", _ht);
        }
        public IList<Hashtable> CreateContributorInstance(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion, String m_InstanceName, String m_InstanceDescription, String m_InstanceVersion, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceDescription", m_InstanceDescription);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateContributorInstance", _ht);
        }
        public IList<Hashtable> DeleteContributorInstance(String m_InstanceName, String m_InstanceVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("Name", m_InstanceName);
            _ht.Add("Version", m_InstanceVersion);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteContributorInstance", _ht);
        }
        public IList<Hashtable> DeleteContributorInstanceParameter(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion, String m_InstanceName, String m_InstanceVersion, String m_ParameterName, Double m_ParameterValue, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("ParameterName", m_ParameterName);
            _ht.Add("Value", m_ParameterValue);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteContributorInstanceParameter", _ht);
        }
        public IList<Hashtable> DeleteRankingRuleSet(String m_RuleSetName, String m_RuleSetVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteRankingRuleSet", _ht);
        }
        public IList<Hashtable> DeleteRankingRuleSetContributor(String m_RuleSetName, String m_RuleSetVersion, String m_InstanceName, String m_InstanceVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteRankingRuleSetContributor", _ht);
        }
        public IList<Hashtable> CreateRankingRuleSet(String m_ProfileName, String m_ProfileVersion,String m_RuleSetName, String m_RuleSetVersion, String m_RuleSetDescription, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("Description", m_RuleSetDescription);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateRankingRuleSet", _ht);
        }
        public IList<Hashtable> GetRankingProfiles(String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetRankingProfiles", _ht);
        }
        public IList<Hashtable> GetRankingProfiles()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetRankingProfiles", null);
        }
        public IList<Hashtable> GetLocationByGroup(String m_LocationGroup)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroup", m_LocationGroup);
            return Mapper().QueryForList<Hashtable>("Misc.GetLocationByGroup", _ht);
        }
        public IList<Hashtable> CreateLocationGroup(String m_LocationGroup, String m_Description, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroup", m_LocationGroup);
            _ht.Add("Description", m_Description);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateLocationGroup", _ht);
        }
        public IList<Hashtable> UpdateLocationGroup(String m_LocationGroup, String m_Description, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroup", m_LocationGroup);
            _ht.Add("Description", m_Description);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateLocationGroup", _ht);
        }
        public IList<Hashtable> DeleteLocationGroup(String m_LocationGroup)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroup", m_LocationGroup);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteLocationGroup", _ht);
        }
        public IList<Hashtable> CreateGroupedLocations(String m_LocationGroup, String m_LocationXML)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroup", m_LocationGroup);
            _ht.Add("LocationXML", m_LocationXML);
            return Mapper().QueryForList<Hashtable>("Misc.CreateGroupedLocations", _ht);
        }
        public IList<Hashtable> CreateBrandProfile(String m_BrandProfile,String m_BrandProfileVersion, String m_BrandProfileDescription, String m_BrandMarketShareXML, String m_Location, String m_LocationGroup)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            _ht.Add("Location", m_Location);
            _ht.Add("LocationGroup", m_LocationGroup);
            _ht.Add("BrandProfileDescription", m_BrandProfileDescription);
            _ht.Add("BrandProfileXML", m_BrandMarketShareXML);
            return Mapper().QueryForList<Hashtable>("Misc.CreateBrandProfile", _ht);
        }
        public IList<Hashtable> CreateRankingProfile(String m_LocationGroup,String m_LocationName, String m_RuleSetName, String m_RuleSetVersion, String m_ProfileName, String m_ProfileVersion, String m_ProfileDescription, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroupName", m_LocationGroup);
            _ht.Add("LocationName", m_LocationName);
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("Description", m_ProfileDescription);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateRankingProfile", _ht);
        }
        public IList<Hashtable> UpdateRankingProfile(String m_LocationGroup, String m_LocationName, String m_RuleSetName, String m_RuleSetVersion, String m_ProfileName, String m_ProfileVersion, String m_ProfileDescription, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("LocationGroupName", m_LocationGroup);
            _ht.Add("LocationName", m_LocationName);
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("Description", m_ProfileDescription);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateRankingProfile", _ht);
        }
        public IList<Hashtable> DeleteRankingProfile(String m_ProfileName, String m_ProfileVersion, String m_RankingModeXML)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("RankingMode", m_RankingModeXML);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteRankingProfile", _ht);
        }
        public IList<Hashtable> CreateRankingRuleSetContributor(String m_RuleSetName, String m_RuleSetVersion, String m_InstanceName, String m_InstanceVersion, Int32 m_Ordinal, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateRankingRuleSetContributor", _ht);
        }        
        public IList<Hashtable> UpdateRankingRuleSet(String m_ProfileName, String m_ProfileVersion,String m_RuleSetName, String m_RuleSetVersion, String m_RuleSetDescription, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("Description", m_RuleSetDescription);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateRankingRuleSet", _ht);
        }
        public IList<Hashtable> UpdateRankingRuleSetContributor(String m_RuleSetName, String m_RuleSetVersion, String m_InstanceName, String m_InstanceVersion, Int32 m_Ordinal, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RuleSetName", m_RuleSetName);
            _ht.Add("RuleSetVersion", m_RuleSetVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateRankingRuleSetContributor", _ht);
        }
        //Time Decay Profile
        public IList<Hashtable> GetTimeDecayProfiles()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetTimeDecayProfiles", null);
        }
        public IList<Hashtable> CreateTimeDecayProfile(String m_ProfileName,String m_ProfileVersion,Int32 m_MaxDecayAge,float m_AnnualDecayRate,String m_Description,Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("MaxDecayAge", m_MaxDecayAge);
            _ht.Add("AnnualDecayRate", m_AnnualDecayRate);
            _ht.Add("Description", m_Description);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateTimeDecayProfile", _ht);
        }
        public IList<Hashtable> UpdateTimeDecayProfile(String m_ProfileName, String m_ProfileVersion, Int32 m_MaxDecayAge, float m_AnnualDecayRate, String m_Description, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("MaxDecayAge", m_MaxDecayAge);
            _ht.Add("AnnualDecayRate", m_AnnualDecayRate);
            _ht.Add("Description", m_Description);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateTimeDecayProfile", _ht);
        }
        public IList<Hashtable> DeleteTimeDecayProfile(String m_ProfileName, String m_ProfileVersion, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);         
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteTimeDecayProfile", _ht);
        }       
        //Time Decay RuleSet
        public IList<Hashtable> GetTimeDecayRuleSets(String m_ProfileName, String m_ProfileVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetTimeDecayRuleSets", _ht);
        }
        public IList<Hashtable> CreateTimeDecayRuleSet(String m_ProfileName,String m_ProfileVersion,Int32 m_Age,float m_AgeDecayFactor,float m_AnnualDecayRate,Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("Age", m_Age);
            _ht.Add("AgeDecayFactor", m_AgeDecayFactor);
            _ht.Add("AnnualDecayRate", m_AnnualDecayRate);            
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.CreateTimeDecayRuleSet", _ht);
        }
        public IList<Hashtable> UpdateTimeDecayRuleSet(String m_ProfileName, String m_ProfileVersion, Int32 m_Age, float m_AgeDecayFactor, float m_AnnualDecayRate, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("Age", m_Age);
            _ht.Add("AgeDecayFactor", m_AgeDecayFactor);
            _ht.Add("AnnualDecayRate", m_AnnualDecayRate);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateTimeDecayRuleSet", _ht);
        }
        public IList<Hashtable> DeleteTimeDecayRuleSet(String m_ProfileName, String m_ProfileVersion, Int32 m_Age, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProfileName", m_ProfileName);
            _ht.Add("ProfileVersion", m_ProfileVersion);
            _ht.Add("Age", m_Age);          
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteTimeDecayRuleSet", _ht);
        }
        public IList<Hashtable> GetContributorParameterValues(String m_CountrybutorTypeName, String m_CountrybutorTypeVersion,String m_InstanceName, String m_InstanceVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("TypeName", m_CountrybutorTypeName);
            _ht.Add("TypeVersion", m_CountrybutorTypeVersion);
            _ht.Add("InstanceName", m_InstanceName);
            _ht.Add("InstanceVersion", m_InstanceVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetContributorParameterValues", _ht);
        }
        public IList<Hashtable> GetRankingRuleSets()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetRankingRuleSets", null);
        }
        public IList<Hashtable> GetRankingModes(String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetRankingModes", _ht);
        }
        public IList<Hashtable> CreateRankingMode(String m_BrandProfile, String m_BrandProfileVersion, String m_RankingProfile, String m_RankingProfileVersion, String m_RankingRuleSet, String m_RankingRuleSetVersion, String m_Mode, Int32 m_Ordinal, Int32 m_SystemFlag, String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("RankingRuleSet", m_RankingRuleSet);
            _ht.Add("RankingRuleSetVersion", m_RankingRuleSetVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("SystemFlag", m_SystemFlag);
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            return Mapper().QueryForList<Hashtable>("Misc.CreateRankingMode", _ht);
        }
        public IList<Hashtable> DeleteRankingMode(String m_BrandProfile, String m_BrandProfileVersion, String m_RankingProfile, String m_RankingProfileVersion, String m_RankingRuleSet, String m_RankingRuleSetVersion, String m_Mode,Int32 m_Ordinal, String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("RankingRuleSet", m_RankingRuleSet);
            _ht.Add("RankingRuleSetVersion", m_RankingRuleSetVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteRankingMode", _ht);
        }
        public IList<Hashtable> UpdateRankingMode(String m_BrandProfile, String m_BrandProfileVersion, String m_RankingProfile, String m_RankingProfileVersion, String m_RankingRuleSet, String m_RankingRuleSetVersion, String m_Mode, Int32 m_Ordinal, Int32 m_SystemFlag)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("RankingRuleSet", m_RankingRuleSet);
            _ht.Add("RankingRuleSetVersion", m_RankingRuleSetVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("SystemFlag", m_SystemFlag);
            return Mapper().QueryForList<Hashtable>("Misc.UpdateRankingMode", _ht);
        }
        public Int32 GetRankingModeOrdinal(String m_BrandProfile, String m_BrandProfileVersion, String m_RankingProfile, String m_RankingProfileVersion, String m_RankingRuleSet, String m_RankingRuleSetVersion, String m_Mode)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("RankingRuleSet", m_RankingRuleSet);
            _ht.Add("RankingRuleSetVersion", m_RankingRuleSetVersion);
            _ht.Add("Mode", m_Mode);            
            return Mapper().QueryForObject<Int32>("Misc.GetRankingModeOrdinal", _ht);
        }
        public IList<Hashtable> GetRankingRuleSetContributors(String m_RankingRulesetName, String m_RankingRulesetVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RuleSetName", m_RankingRulesetName);
            _ht.Add("RuleSetVersion", m_RankingRulesetVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetRankingRuleSetContributors", _ht);
        }
        public IList<Hashtable> GetBrandProfiles()
        {
            return Mapper().QueryForList<Hashtable>("Misc.GetBrandProfiles", null);
        }
        public IList<Hashtable> GetBrandProfileDetails(String m_BrandProfile, String m_BrandProfileVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("BrandProfile", m_BrandProfile);
            _ht.Add("BrandProfileVersion", m_BrandProfileVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetBrandProfileDetails", _ht);
        }
        public Int32 GetIDRankingStatus(String m_RankingProfile, String m_RankingProfileVersion, String m_Mode, Int32 m_Ordinal)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            return Mapper().QueryForObject<Int32>("Misc.GetIDRankingStatus", _ht);
        }
        public IList<Hashtable> GetIDRanking(String m_RankingProfile, String m_RankingProfileVersion, String m_Mode, Int32 m_Ordinal)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            return Mapper().QueryForList<Hashtable>("Misc.GetIDRanking", _ht);
        }
        public IList<Hashtable> GetIDRankingContributions(String m_RankingProfile, String m_RankingProfileVersion, String m_ContributorType, String m_Mode, Int32 m_Ordinal, String m_ColumnAliases)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("ContributorType", m_ContributorType);
            _ht.Add("ColumnAliases", m_ColumnAliases);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            return Mapper().QueryForList<Hashtable>("Misc.GetIDRankingContributions", _ht);
        }
        public Int32 ClearIDRanking(String m_RankingProfile, String m_RankingProfileVersion, String m_Mode, Int32 m_Ordinal)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            return Mapper().QueryForObject<Int32>("Misc.ClearIDRanking", _ht);
        }
        public IList<Hashtable> CreateIDRanking(String m_RankingProfile, String m_RankingProfileVersion, String m_Mode, Int32 m_Ordinal, String m_IDList)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("IDS", m_IDList);
            return Mapper().QueryForList<Hashtable>("Misc.CreateIDRanking", _ht);
        }
        public Int32 CalculateIDRanking(String m_RankingProfile, String m_RankingProfileVersion, String m_Mode, Int32 m_Ordinal, String m_StagingDBName)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            _ht.Add("Mode", m_Mode);
            _ht.Add("Ordinal", m_Ordinal);
            _ht.Add("StagingDB", m_StagingDBName);
            return Mapper().QueryForObject<Int32>("Misc.CalculateIDRanking", _ht);
        }
        public IList<Hashtable> CreateTemplateRankingProfiles(String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion, String m_RankingProfile, String m_RankingProfileVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            return Mapper().QueryForList<Hashtable>("Misc.CreateTemplateRankingProfile", _ht);
        }
        public IList<Hashtable> DeleteTemplateRankingProfiles(String m_ProjectName, String m_ProjectVersion, String m_TemplateName, String m_TemplateVersion, String m_RankingProfile, String m_RankingProfileVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            _ht.Add("TemplateName", m_TemplateName);
            _ht.Add("TemplateVersion", m_TemplateVersion);
            _ht.Add("RankingProfile", m_RankingProfile);
            _ht.Add("RankingProfileVersion", m_RankingProfileVersion);
            return Mapper().QueryForList<Hashtable>("Misc.DeleteTemplateRankingProfile", _ht);
        }
        public IList<Hashtable> GetTemplateInfo(String m_ProjectName, String m_ProjectVersion)
        {
            Hashtable _ht = new Hashtable();
            _ht.Add("ProjectName", m_ProjectName);
            _ht.Add("ProjectVersion", m_ProjectVersion);
            return Mapper().QueryForList<Hashtable>("Misc.GetTemplateInfo", _ht);
        }
        #endregion
    }
}
