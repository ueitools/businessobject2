using System;
using System.Collections.Generic;
using System.Text;

using IBatisNet.Common;
using IBatisNet.DataMapper;

namespace BusinessObject
{
    /// <summary>
    /// Base class for Helper objects (*Helper). 
    /// Provides shared utility methods.
    /// </summary>
    public abstract class DAO
    {
        public static volatile ISqlMapper mapper = IBatisNet.DataMapper.Mapper.Instance();

#if DEBUG || iBatisDataMap1_5_1
        // below is for ibatis data mapper v1.5.1 
        public static ISqlMapper Mapper()
        {
            return mapper;
        }
#else
        public SqlMapper Mapper()
        {
            return IBatisNet.DataMapper.Mapper.Instance();
        }
#endif
    }
}
