using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductDAO : DAO
    {
        public ProductCollection SelectProducts(int URC, int Revision)
        {
            Hashtable paramList = new Hashtable();

            if (URC > 0)
            {
                paramList.Add("URC",URC);
                paramList.Add("Revision", Revision);
            }

            return Mapper().QueryForList("Product.SelectProduct", paramList)
                 as ProductCollection;
        }

        public void SaveProduct(Product product)
        {
            Mapper().Insert("Product.InsertProduct", product);
        }

    }
}
