using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace BusinessObject
{
    public class DeviceListDAO : DAO
    {
        public const string _GROUPFLAG = "All ";

        /// <summary>
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public DeviceCollection SelectDeviceListByBrandModelExact(
            string mode, int? deviceTypeCode, string brand, string model, List<string> market, string branch)
        {
            BrandModelSearchParameter bmSearchParam = new BrandModelSearchParameter();

            bmSearchParam.Type = mode;
            bmSearchParam.Brand = brand;
            bmSearchParam.Model = model;
            bmSearchParam.DeviceTypeCode = deviceTypeCode;
            StringCollection regionList;
            StringCollection countryList;
            BuildMarketFilter(market, out regionList, out countryList);
            bmSearchParam.RegionList = regionList;
            bmSearchParam.CountryList = countryList;
            bmSearchParam.Branch = branch;

            DeviceCollection list = Mapper()
                .QueryForList("DeviceList.SelectDeviceListByBrandModel", 
                bmSearchParam)
                as DeviceCollection;
            return list;
        }

        public void BuildMarketFilter(
            List<string> market, out StringCollection regionList, out StringCollection countryList)
        {
            regionList = new StringCollection();
            countryList = new StringCollection();

            if (market != null)
            {
                foreach (string item in market)
                {
                    if (item.StartsWith(_GROUPFLAG))
                    {
                        regionList.Add(item.Substring(_GROUPFLAG.Length));
                    }
                    else
                    {
                        countryList.Add(item);
                    }
                }
            }
        }
    }
}
