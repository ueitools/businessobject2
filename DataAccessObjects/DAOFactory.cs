using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;


namespace BusinessObject
{
    /// <summary>
    /// Singleton "controller" for Helper classes.
    /// </summary>
    public class DAOFactory
    {
        #region private fields
        private static volatile IDHeaderDAO _IDHeaderDAO = null;
        private static volatile FunctionDAO _FunctionDAO = null;
        private static volatile DeviceDAO _DeviceDAO = null;
        private static volatile MiscDAO _MiscDAO = null;
        private static volatile TNHeaderDAO _TNHeaderDAO = null;
        private static volatile AdHocDAO _AdHocDAO = null;
        private static volatile ReadOnlyDAO _readOnlyDAO = null;
        private static volatile LogDAO _logDAO = null;

        private static volatile ProjectDAO _projectDAO = null;
        private static volatile PickDAO _pickDAO = null;

        private static volatile DeviceListDAO _DeviceListDAO = null;
        #endregion

        #region Transaction Related
        public static void BeginTransaction()
        {
            Mapper().BeginTransaction();
        }

        public static void BeginTransaction(IsolationLevel level)
        {
            Mapper().BeginTransaction(level);
        }

        public static void CommitTransaction()
        {
            Mapper().CommitTransaction();
        }

        public static void RollBackTransaction()
        {
            Mapper().RollBackTransaction();
        }
        #endregion

        #region Connection Related
        /// <summary>
        /// optimize: only changes if conn str is changed
        /// </summary>
        /// <param name="connStr"></param>
        /// <returns></returns>
        public static void ResetDBConnection(string connStr)
        {
            string prevConnStr = Mapper().DataSource.ConnectionString;

            if (prevConnStr != connStr)
            {
                Mapper().DataSource.ConnectionString = connStr;
                Mapper().OpenConnection();
                Mapper().CloseConnection();
            }
        }

        public static ISqlMapper MakeSqlMapper(string connStr)
        {
            ISqlMapper sqlMap = new DomSqlMapBuilder().Configure("SqlMap.config");

            return sqlMap;
        }

        /// <summary>
        /// Close the database connection.
        ///     This was added to allow closing the connection after 
        ///     an error is encountered while trying to connect.
        /// </summary>
        public static void CloseDBConnection()
        {
            Mapper().CloseConnection();
        }

        /// <summary>
        /// Close the database connection.
        ///     This was added to allow closing the connection after 
        ///     an error is encountered while trying to connect.
        /// </summary>
        public static void CloseDBConnection(bool check)
        {
            if (!check || Mapper().IsSessionStarted)
                Mapper().CloseConnection();
        }

        public static string GetDBConnectionString()
        {
            return Mapper().DataSource.ConnectionString;
        }
        #endregion

        #region Singleton
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IDHeaderDAO IDHeader()
        {
            if (_IDHeaderDAO == null)
            {
                lock (typeof(IDHeaderDAO))
                {
                    if (_IDHeaderDAO == null) // double-check
                        _IDHeaderDAO = new IDHeaderDAO();
                }
            }
            return _IDHeaderDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static FunctionDAO Function()
        {
            if (_FunctionDAO == null)
            {
                lock (typeof(FunctionDAO))
                {
                    if (_FunctionDAO == null) // double-check
                        _FunctionDAO = new FunctionDAO();
                }
            }
            return _FunctionDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DeviceDAO Device()
        {
            if (_DeviceDAO == null)
            {
                lock (typeof(DeviceDAO))
                {
                    if (_DeviceDAO == null) // double-check
                        _DeviceDAO = new DeviceDAO();
                }
            }
            return _DeviceDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DeviceListDAO DeviceListDAO()
        {
            if (_DeviceListDAO == null)
            {
                lock (typeof(DeviceListDAO))
                {
                    if (_DeviceListDAO == null) // double-check
                        _DeviceListDAO = new DeviceListDAO();
                }
            }
            return _DeviceListDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MiscDAO Misc()
        {
            if (_MiscDAO == null)
            {
                lock (typeof(MiscDAO))
                {
                    if (_MiscDAO == null) // double-check
                        _MiscDAO = new MiscDAO();
                }
            }
            return _MiscDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static TNHeaderDAO TNHeader()
        {
            if (_TNHeaderDAO == null)
            {
                lock (typeof(TNHeaderDAO))
                {
                    if (_TNHeaderDAO == null) // double-check
                        _TNHeaderDAO = new TNHeaderDAO();
                }
            }
            return _TNHeaderDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static AdHocDAO AdHoc()
        {
            if (_AdHocDAO == null)
            {
                lock (typeof(AdHocDAO))
                {
                    if (_AdHocDAO == null) // double-check
                        _AdHocDAO = new AdHocDAO();
                }
            }
            return _AdHocDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ReadOnlyDAO ReadOnly()
        {
            if (_readOnlyDAO == null)
            {
                lock (typeof(ReadOnlyDAO))
                {
                    if (_readOnlyDAO == null) // double-check
                        _readOnlyDAO = new ReadOnlyDAO();
                }
            }
            return _readOnlyDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static LogDAO Log()
        {
            if (_logDAO == null)
            {
                lock (typeof(LogDAO))
                {
                    if (_logDAO == null) // double-check
                        _logDAO = new LogDAO();
                }
            }
            return _logDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ProjectDAO Project()
        {
            if (_projectDAO == null)
            {
                lock (typeof(ProjectDAO))
                {
                    if (_projectDAO == null) // double-check
                        _projectDAO = new ProjectDAO();
                }
            }
            return _projectDAO;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static PickDAO Pick()
        {
            if (_pickDAO == null)
            {
                lock (typeof(PickDAO))
                {
                    if (_pickDAO == null) // double-check
                        _pickDAO = new PickDAO();
                }
            }
            return _pickDAO;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
#if DEBUG || iBatisDataMap1_5_1
        /// below is to run ibatis data mapper v1.5.1 
        private static IBatisNet.DataMapper.ISqlMapper Mapper()
        {
            return DAO.mapper;
        }
#else
        private static IBatisNet.DataMapper.SqlMapper Mapper()
        {
            return IBatisNet.DataMapper.Mapper.Instance();
        }
#endif
    }
}
