using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    public class TNHeaderDAO : DAO
    {
        public TNHeader Select(int tn)
        {
            TNHeader tnHeader =
                Mapper().QueryForObject("TNHeader.Select", tn) as TNHeader;
            if (tnHeader == null)
                throw new Exception("TN " + tn + " dosen't exist!");
           
            tnHeader.LogList = DAOFactory.Log().Select(tn);
            return tnHeader;
        }

        public TNHeader SelectHeaderOnly(int tn)
        {
            TNHeader tnHeader =
                Mapper().QueryForObject("TNHeader.Select", tn) as TNHeader;
            if (tnHeader == null)
                throw new Exception("TN " + tn + " dosen't exist!");

            return tnHeader;
        }

        /// <summary>
        /// if id exist already throw exception
        /// </summary>
        /// <param name="tnHeader"></param>
        public void Insert(TNHeader tnHeader)
        {
            if (DoesExistNoFilter(tnHeader.TN) == true)
                throw new Exception("TN " + tnHeader.TN + " already exist!");

            if(tnHeader.Parent_TN != tnHeader.TN)
                RefereceEnforcer.InsertShadow(tnHeader.Parent_TN);

            Mapper().Insert("TNHeader.Insert", tnHeader);
            DAOFactory.Log().ReInsert(tnHeader.TN, tnHeader.LogList);
        }

        public void Update(TNHeader tnHeader)
        {
            if (DoesExistNoFilter(tnHeader.TN) != true)
                throw new Exception("TN " + tnHeader.TN + " dosen't exist!");

            if (tnHeader.Parent_TN != tnHeader.TN)
                RefereceEnforcer.InsertShadow(tnHeader.Parent_TN);

            Mapper().Update("TNHeader.Update", tnHeader);
            DAOFactory.Log().ReInsert(tnHeader.TN, tnHeader.LogList);
        }

        public void UpdateHeaderOnly(TNHeader tnHeader)
        {
            if (DoesExistNoFilter(tnHeader.TN) != true)
                throw new Exception("TN " + tnHeader.TN + " dosen't exist!");

            if (tnHeader.Parent_TN != tnHeader.TN)
                RefereceEnforcer.InsertShadow(tnHeader.Parent_TN);

            Mapper().Update("TNHeader.Update", tnHeader);
        }

        /// <summary>
        /// make it shadow
        /// </summary>
        /// <param name="tn"></param>
        public void MakeUnAvailable(int tn)
        {
            TNHeader header = new TNHeader();
            header.TN = tn;
            header.Status = StatusFlag.SHADOW;

            this.Update(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DoesExist(int tn)
        {
            if (Mapper().QueryForObject("TNHeader.Select", tn) == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// for insert or update this should be used
        /// </summary>
        /// <param name="tn"></param>
        /// <returns></returns>
        public bool DoesExistNoFilter(int tn)
        {
            if (Mapper().QueryForObject(
                "TNHeader.SelectNoFilter", tn) == null)
                return false;
            else
                return true;
        }
    }
}
