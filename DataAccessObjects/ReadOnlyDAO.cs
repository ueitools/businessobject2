using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace BusinessObject
{
    public class ReadOnlyDAO : DAO
    {
        public const int TNLOW1 = 21425;
        public const int TNHIGH1 = 29999;
        public const int TNLOW2  = 94855;
        public const int TNHIGH2 = 99999;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executorCodeList"></param>
        /// <param name="platformCode"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public IList<Image> GetExecutorOBJ(IList<int> executorCodeList, 
            int platformCode, string version)
        {
            ImageCollection result = new ImageCollection();

            foreach (int exec in executorCodeList)
            {
                Hashtable paramList = new Hashtable();
                paramList.Add("Executor", exec);
                paramList.Add("Platform_Code", platformCode);
                if (!string.IsNullOrEmpty(version))
                    paramList.Add("Version", version);
                ImageCollection item = 
                    Mapper().QueryForList("ReadOnly.GetExecutorOBJ",
                            paramList) as ImageCollection;
                if (item.Count > 0)
                    result.Add(item[0]);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="executorCodeList"></param>
        /// <param name="platformName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public IList<Image> GetExecutorUSH(IList<int> executorCodeList,
            string platformName, string version)
        {
            IList<Image> result = new List<Image>();

            foreach (int exec in executorCodeList)
            {
                Hashtable paramList = new Hashtable();
                paramList.Add("Executor", exec);
                int platformCode = GetPlatformCode(platformName);
                // temporary reassignment for MORF executor image type
                // move to stored procedure later for better centralization
                const int GreenPeakMorf = 4;
                const int TI = 3;
                if (platformCode == GreenPeakMorf)
                    platformCode = TI;
                paramList.Add("Platform_Code", platformCode);
                if (!string.IsNullOrEmpty(version))
                    paramList.Add("Version", version);
                ImageCollection item = 
                    Mapper().QueryForList("ReadOnly.GetExecutorUSH",
                            paramList) as ImageCollection;
                if (item.Count > 0)
                    result.Add(item[0]);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public NumericValueLabelPairCollection 
            GetNumericValueForLabel(StringCollection labelList)
        {
            return Mapper().QueryForList("ReadOnly.GetNumericValueForLabel", labelList) as
                NumericValueLabelPairCollection;
        }

        private int GetPlatformCode(string platformName)
        {
            return (int)Mapper().QueryForObject("ReadOnly.GetPlatformCode", platformName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IDFunctionCollection IDFunctionSelect(string id)
        {
            IList list = Mapper().QueryForList(
                "ReadOnly.IDFunctionSelect", id);
            IDFunctionCollection functionList = new IDFunctionCollection();
            IDTNCaptureCollection captureList = IDTNCaptureSelect(id);

            foreach (IDFunction function in list)
            {
                function.IDTNCaptureList = FilterTNCaptureList(
                    captureList, function.Data, function.Label);
                functionList.Add(function);
            }

            return functionList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
                public IDFunctionCollection IDFunctionModelSelect(string id, string model)
        {
            Hashtable paramTable = new Hashtable();

            paramTable["ID"] = id;
            paramTable["Model"] = model.Trim();
            paramTable["TNLOW1"]  = 21425;
            paramTable["TNHIGH1"] = 29999;
            paramTable["TNLOW2"]  = 94855;
            paramTable["TNHIGH2"] = 99999;

            IList list = Mapper().QueryForList(
                "ReadOnly.IDModelFunctionSelect", paramTable);

            IDFunctionCollection functionList = new IDFunctionCollection();
            IDTNCaptureCollection captureList = IDTNCaptureSelect(id);

            foreach (IDFunction function in list)
            {
                function.IDTNCaptureList = FilterTNCaptureList(
                    captureList, function.Data, function.Label);

                functionList.Add(function);
            }

            return functionList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IDTNCaptureCollection IDTNCaptureSelect(string id)
        {
            IList list = Mapper().QueryForList(
                "ReadOnly.IDTNCaptureSelect", id);
            IDTNCaptureCollection captureList = new IDTNCaptureCollection();

            foreach (IDTNCapture capture in list)
            {
                captureList.Add(capture);
            }

            return captureList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputList"></param>
        /// <param name="data"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        IDTNCaptureCollection FilterTNCaptureList(
            IDTNCaptureCollection inputList, string data, string label)
        {
            IDTNCaptureCollection outputList = new IDTNCaptureCollection();

            foreach(IDTNCapture capture in inputList)
            {
                if (capture.Data == data && capture.Label == label)
                {
                    outputList.Add(capture);
                }
            }

            return outputList;
        }        
    }
}
