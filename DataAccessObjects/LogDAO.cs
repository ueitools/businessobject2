using System;
using System.Collections.Generic;

namespace BusinessObject
{
    public class LogDAO : DAO
    {
        public LogCollection Select(int tn) {
            LogCollection logCollection = Mapper().QueryForList("Log.Select", tn) as LogCollection;
            ShiftToLocalTime(logCollection);
            return logCollection;
        }

        public LogCollection Select(string id) {
            LogCollection logCollection = Mapper().QueryForList("Log.SelectIDOnly", id) as LogCollection;
            ShiftToLocalTime(logCollection);
            return logCollection;
        }

        private void ShiftToLocalTime(LogCollection logCollection)
        {
            if (logCollection != null)
            {
                foreach (Log log in logCollection)
                {
                    log.Time = log.Time.ToLocalTime();
                }
            }
        }

        /// <summary>
        /// bulk insert
        /// </summary>
        /// <param name="id"></param>
        /// <param name="logList"></param>
        public void ReInsert(string id, LogCollection logList)
        {
            Mapper().Delete("Log.DeleteIDOnly", id);

            int startRID = (int)Mapper().QueryForObject(
                "Log.GetNextRID", null);

            LogTableGenerator tableGenerator = new LogTableGenerator();
            List<string> tableList = tableGenerator.GetLocalTable(
                id, startRID, logList);
            foreach(string table in tableList)
                Mapper().Insert("Log.BulkInsert", table);
        }

        public void ReInsert(int tn, LogCollection logList)
        {
            Mapper().Delete("Log.Delete", tn);

            int startRID = (int)Mapper().QueryForObject(
                "Log.GetNextRID", null);

            LogTableGenerator tableGenerator = new LogTableGenerator();
            List<string> tableList = tableGenerator.GetLocalTable(
                tn, startRID, logList);
            foreach (string table in tableList)
                Mapper().Insert("Log.BulkInsert", table);
        }
    }
}
