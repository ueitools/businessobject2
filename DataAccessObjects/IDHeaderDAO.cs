using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using BusinessObject.LockStatus;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class IDHeaderDAO : DAO
    {
        /// <summary>
        /// exception: if id not exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IDHeader Select(string id)
        {
            IDHeader idHeader =
                Mapper().QueryForObject("IDHeader.Select", id) as IDHeader;
            if (idHeader == null)
                throw new Exception("ID " + id + " dosen't exist!");

            idHeader.PrefixList = SelectPrefix(id);
            idHeader.LogList = DAOFactory.Log().Select(id);
            return idHeader;
        }

        /// <summary>
        /// exception: if id not exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IDHeader SelectHeaderOnly(string id)
        {
            IDHeader idHeader =
                Mapper().QueryForObject("IDHeader.Select", id) as IDHeader;
            if (idHeader == null)
                throw new Exception("ID " + id + " dosen't exist!");

            return idHeader;
        }

   
        /// <summary>
        /// Take care of parent ID reference
        /// exception: if id exist
        /// Check if Parent ID is same as ID (need to be fixed later)
        /// </summary>
        /// <param name="idHeader"></param>
        public void Insert(IDHeader idHeader)
        {
            if (DoesExistNoFilter(idHeader.ID) == true)
                throw new Exception("ID " + idHeader.ID + " already exist!");

            if (idHeader.Parent_ID != idHeader.ID)
                RefereceEnforcer.InsertShadow(idHeader.Parent_ID);

            Mapper().Insert("IDHeader.Insert", idHeader);
            ReInsertPrefix(idHeader.ID, idHeader.PrefixList);
            DAOFactory.Log().ReInsert(idHeader.ID, idHeader.LogList);
        }

        /// <summary>
        /// exception: if id not exist
        /// </summary>
        /// <param name="idHeader"></param>
        public void Update(IDHeader idHeader)
        {
            if (DoesExistNoFilter(idHeader.ID) != true)
                throw new Exception("ID " + idHeader.ID + " dosen't exist!");

            if (idHeader.Parent_ID != idHeader.ID)
                RefereceEnforcer.InsertShadow(idHeader.Parent_ID);

            Mapper().Update("IDHeader.Update", idHeader);
            ReInsertPrefix(idHeader.ID, idHeader.PrefixList);
            DAOFactory.Log().ReInsert(idHeader.ID, idHeader.LogList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public void Delete(string id)
        {
            if (DoesExistNoFilter(id) != true)
                throw new Exception("ID " + id + " dosen't exist!");

            Mapper().Delete("IDHeader.Delete", id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idHeader"></param>
        public void UpdateHeaderOnly(IDHeader idHeader)
        {
            if (DoesExistNoFilter(idHeader.ID) != true)
                throw new Exception("ID " + idHeader.ID + " dosen't exist!");

            if (idHeader.Parent_ID != idHeader.ID)
                RefereceEnforcer.InsertShadow(idHeader.Parent_ID);

            Mapper().Update("IDHeader.Update", idHeader);
        }

        /// <summary>
        /// make it as shadow
        /// </summary>
        /// <param name="id"></param>
        public void MakeUnAvailable(string id)
        {
            IDHeader header = new IDHeader();
            header.ID = id;
            header.Status = StatusFlag.SHADOW;

            this.Update(header);
        }

        ///
        ///
        ///
        public PrefixCollection SelectPrefix(string id)
        {
            IList list = Mapper().QueryForList("Prefix.Select", id);
            PrefixCollection prefixList = new PrefixCollection();

            foreach (Prefix prefix in list)
            {
                prefixList.Add(prefix);
            }

            return prefixList;
        }

        /// <summary>
        /// delete previous prefix list before inserting new one
        /// RID is manually incremented 
        /// </summary>
        /// <param name="prefixList"></param>
        private void ReInsertPrefix(string id, PrefixCollection prefixList)
        {
            Mapper().Delete("Prefix.Delete", id);
            Guid curRID = (Guid)Mapper().QueryForObject("Prefix.GetNextRID", null);

            foreach (Prefix prefix in prefixList)
            {
                prefix.RID = curRID;
                prefix.ID = id;

                Mapper().Insert("Prefix.Insert", prefix);
                curRID = Guid.NewGuid();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DoesExist(string id)
        {
            return (Mapper().QueryForObject("IDHeader.Select", id) != null);
        }

        /// <summary>
        ///  for insert or update decision this should be used
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DoesExistNoFilter(string id)
        {
            return (Mapper().QueryForObject("IDHeader.SelectNoFilter", id) != null);
        }

        /// <summary>
        /// Update ID mode and code in three places directly: protocol, protocolpriority, logwebmodelsearch
        /// Also, cascade changes to function, log, protocol prefix
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public void UpdateName(string source, string destination)
        {
            DoUpdateNamePreconditions(source, destination);

            Hashtable paramList = new Hashtable();
            paramList.Add("source", source);
            paramList.Add("destination", destination);

            Mapper().Update("IDHeader.UpdateName", paramList);
            Mapper().Update("IDHeader.UpdateProtocolPriorityID", paramList);
            Mapper().Update("IDHeader.UpdateLogWebModelID", paramList);
        }

        /// <summary>
        /// before update verify source exists and destination does not in two tables.
        /// for log web model search, the id may not exist yet. so allow no source, but check destination.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        private void DoUpdateNamePreconditions(string source, string destination)
        {
            MiscDAO misc = new MiscDAO();

            if (DoesExistNoFilter(destination) == true)
                throw new Exception("ID " + destination + " already exists!");
            if (misc.ProtocolPriorityDoesExist(destination) == true ||
                misc.LogWebModelDoesExist(destination))
                throw new Exception("ID " + destination + " already exists in support tables!");
            if (DoesExistNoFilter(source) != true)
                throw new Exception("ID " + source + " doesn't exist!");

            IdLock lockedID = GetLockedId(source);
            if (lockedID != null) {
                throw new Exception("ID " + source + " is checked out!");
            }
        }

        private static IdLock GetLockedId(string id) {
            LockStatusService lockStatus = new LockStatusService();
            IdLock[] lockedIDs = lockStatus.GetLockedIDs(Environment.UserDomainName);
            IdLock lockedID = null;
            foreach (IdLock idLock in lockedIDs) {
                if (idLock.ID.ToUpper() == id.ToUpper()) {
                    lockedID = idLock;
                }
            }

            return lockedID;
        }
    }
}
