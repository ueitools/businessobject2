using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class FunctionDAO : DAO
    {
        public void Insert(string id, FunctionCollection list)
        {
            int startRID = (int)Mapper().QueryForObject(
                "Function.GetNextRID", null);

            FunctionTableGenerator tableGenerator = 
                new FunctionTableGenerator();

            List<string> tableList = 
                tableGenerator.GetLocalTable(id, startRID, list);
            foreach(string table in tableList)
                Mapper().Insert("Function.BulkInsert", table);
        }

        /// <summary>
        /// Handle Shadow ID first
        /// </summary>
        /// <param name="tn"></param>
        /// <param name="list"></param>
        public void Insert(int tn, FunctionCollection list)
        {
            StringCollection idList = GetIDList(list);
            foreach (string id in idList)
            {
                RefereceEnforcer.InsertShadow(id);
            }

            int startRID = (int)Mapper().QueryForObject(
                "Function.GetNextRID", null);

            FunctionTableGenerator tableGenerator = 
                new FunctionTableGenerator();

            List<string> tableList = 
                tableGenerator.GetLocalTable(tn, startRID, list);
            foreach (string table in tableList)
                Mapper().Insert("Function.BulkInsert", table);
        }


        public IList FindDisconnectedTNs()
        {
            IList list = Mapper().QueryForList(
                "Function.DisconnectedTNs", 0);
            return list;
        }

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="id"></param>
        /// <returns></returns>
        public FunctionCollection Select(int tn)
        {
            FunctionCollection list = Mapper().QueryForList(
                "Function.Select", tn) as FunctionCollection;

            AddInversedDataFlag(list);

            return list;
        }

        /// <summary>
        /// IsInversedData flag is not part of the function 
        /// query so this adds it from ID header
        /// </summary>
        /// <param name="list">FunctionCollection list</param>
        private void AddInversedDataFlag(FunctionCollection list)
        {
            IDHeader idHeader;
            foreach (Function item in list)
            {
                string id = item.ID;
                if (string.IsNullOrEmpty(id))
                    continue;

                idHeader = Mapper().QueryForObject(
                    "IDHeader.Select", id) as IDHeader;

                if (idHeader == null)
                    continue;

                item.IsInversedData = idHeader.IsInversedData;
            }
        }

        /// <summary>
        /// ID Level Edit support (Data, Label, Intron and Priority)
        /// </summary>
        /// <param name="paramList"></param>
        /// <returns></returns>
        public FunctionCollection SelectID(Hashtable paramList)
        {
            return Mapper().QueryForList("Function.SelectID", paramList)
                 as FunctionCollection;
        }

        public FunctionCollection SelectIDFunc(Hashtable paramList)
        {
            return Mapper().QueryForList("Function.SelectIDFunc", paramList)
                 as FunctionCollection;
        }

        public void UpdateIDData(string id, string currentData, 
            string newData)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);
            paramList.Add("CurrentData", currentData);
            paramList.Add("NewData", newData);

            Mapper().Update("Function.UpdateIDData", paramList);
        }

        public void UpdateIDLabel(string id, string currentData,
            string currentLabel, string newLabel)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);
            paramList.Add("CurrentData", currentData);
            paramList.Add("CurrentLabel", currentLabel);
            paramList.Add("NewLabel", newLabel);

            Mapper().Update("Function.UpdateIDLabel", paramList);
        }

        public void UpdateIDIntron(string id, string currentLabel,
           string currentIntron, string newIntron, string newPriority)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);
            paramList.Add("CurrentLabel", currentLabel);
            paramList.Add("CurrentIntron", currentIntron);
            paramList.Add("NewIntron", newIntron);
            paramList.Add("NewPriority", newPriority);

            Mapper().Update("Function.UpdateIDIntron", paramList);
        }

        public void UpdateIDIntron(string id, string currentData,
            string currentLabel, string currentIntron, 
            string newIntron, string newPriority)
        {
            Hashtable paramList = new Hashtable();
            paramList.Add("ID", id);
            paramList.Add("currentData", currentData);
            paramList.Add("CurrentLabel", currentLabel);
            paramList.Add("CurrentIntron", currentIntron);
            paramList.Add("NewIntron", newIntron);
            paramList.Add("NewPriority", newPriority);

            Mapper().Update("Function.UpdateIDIntron2", paramList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tn"></param>
        public void Delete(int tn)
        {
            Mapper().Delete("Function.Delete", tn);
        }

        /// <summary>
        /// Partial ID Edit -- delete Fuction with given ID with null TN
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public FunctionCollection SelectIDOnly(string id)
        {
            return Mapper().QueryForList("Function.SelectIDOnly", id)
                 as FunctionCollection;
        }

        public void DeleteIDOnly(string id)
        {
            Mapper().Delete("Function.DeleteIDOnly", id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetIDRID(string id)
        {
            object rid = Mapper().QueryForObject(
                "Function.GetIDRID", id);

            if (rid == null)
            {
                InsertProxy(id);
                rid = Mapper().QueryForObject(
                    "Function.GetIDRID", id);
            }
          
            return (int)rid;
        }

        public int GetTNRID(int tn)
        {
            object rid = Mapper().QueryForObject(
                "Function.GetTNRID", tn);

            if (rid == null)
            {
                InsertProxy(tn);
                rid = Mapper().QueryForObject(
                    "Function.GetTNRID", tn);
            }

            return (int)rid;
        }

        /// <summary>
        /// TN = 0 (it will insert null value for TN)
        /// IsConnection = "Y"
        /// rest of them should be null
        /// 
        /// Also insert shadow ID if referenced ID dosen't exist
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int InsertProxy(string id)
        {
            RefereceEnforcer.InsertShadow(id);

            Function function = new Function();
            function.TN = 0;
            function.IsConnection = "Y";
            function.ID = id;
            function.RID = (int)Mapper().QueryForObject(
                    "Function.GetNextRID", null);

            Mapper().Insert("Function.Insert", function);
            return function.RID;
        }

        public int InsertProxy(int tn)
        {
            RefereceEnforcer.InsertShadow(tn);

            Function function = new Function();
            function.TN = tn;
            function.IsConnection = "Y";
            function.ID = null;
            function.RID = (int)Mapper().QueryForObject(
                    "Function.GetNextRID", null);

            Mapper().Insert("Function.Insert", function);
            return function.RID;
        }

        /// <summary>
        /// DANGEROUS!!! DON'T CALL THIS (ASK Byung)
        /// Basically this will affect multiple table in cascade
        /// </summary>
        /// <param name="id"></param>
        public void DropProxy(string id)
        {
            Mapper().Delete("Function.DeleteProxy", id);
        }

        private StringCollection GetIDList(FunctionCollection functionList)
        {
            StringCollection idList = new StringCollection();
            foreach (Function function in functionList)
            {
                if (idList.Contains(function.ID) != true)
                    idList.Add(function.ID);
            }

            return idList;
        }

        public FunctionCollection SelectSynTableFuncSortIntron(string id)
        {
            return Mapper().QueryForList(
                "Function.SelectSynTableFuncSortIntron", id)
                 as FunctionCollection;
        }

        public FunctionCollection SelectSynTableFunc(string id)
        {
            return Mapper().QueryForList(
                "Function.SelectSynTableFunc", id)
                 as FunctionCollection;
        }

        public FunctionCollection SelectSynTableFuncNoIntron(string id)
        {
            return Mapper().QueryForList(
                "Function.SelectSynTableFuncNoIntron", id)
                 as FunctionCollection;
        }
        public HashtableCollection SelectUniqueFunctionIRCount()
        {
            IList list = Mapper().QueryForList("Function.SelectUniqueFunctionIRCount", null);
            HashtableCollection tableList = new HashtableCollection();
            foreach (Hashtable table in list)
            {
                tableList.Add(table);
            }
            return tableList;
        }
        public int SelectUniqueFuncCount(string mode)
        {            
            int count = (int)Mapper().QueryForObject(
                    "Function.SelectUniqueFuncCount", mode);
            return count;
        }

        public int SelectUniqueIRCount(string mode)
        {
            int count = (int)Mapper().QueryForObject(
                    "Function.SelectUniqueIRCount", mode);
            return count;
        }

        public FunctionCollection SelectFunc(string conditions)
        {
            return Mapper().QueryForList(
                "Function.SelectFunc", conditions)
                 as FunctionCollection;
        }
        /// <summary>
        /// Retrieves ID,Data,Label,Intron,IsInversedData,IntronPriority from db based on the parameter passed.Used for label
        /// and intron search report.
        /// </summary>
        /// <remarks>
        /// Added on june 22,2011 by binil.Implements SP for label and intron search report.
        /// Replaces SelectFunc(string conditions).
        /// </remarks>
        /// <param name="searchParam">
        /// Input contains a list of ids,label or intron patterns and flag for toggling intron based or label
        /// based search.
        /// </param>
        /// <returns></returns>
        public FunctionCollection IntronLabelSearch(IDSearchParameter searchParam)
        {
            return Mapper().QueryForList(
               "Function.IntronLabelSearch", searchParam)
                as FunctionCollection;
        }
        public FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter searchParam)
        {
            return Mapper().QueryForList(
               "Function.ExcludeIntronLabelSearch", searchParam)
                as FunctionCollection;
        }
        

        public StringCollection SelectLabelForFunc(StringCollection idList)
        {
            return Mapper().QueryForList(
                "Function.SelectLabelForFunc", idList)
                as StringCollection;
        }

        public IList<Hashtable> LabelSearch(string m_Modes, string labels)
        {
            try
            {
                Hashtable paramList = new Hashtable();
                paramList.Add("Modes", m_Modes);
                paramList.Add("Labels", labels);
                return Mapper().QueryForList<Hashtable>("Function.LabelSearch", paramList);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetAllModesWithNames()
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetAllModesWithNames", null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetDeviceTypesByModes(string modes,string regions,string sources,string countries,int filter)
        {
            try
            {
                Hashtable param = new Hashtable();
                param.Add("Modes",modes);
                param.Add("Regions",regions);
                param.Add("Sources",sources);
                param.Add("Countries",countries);
                param.Add("Filter",filter);
                               

                return Mapper().QueryForList<Hashtable>("Function.GetDeviceTypesByModes", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetLoadList(Hashtable ht)
        {
            try
            {
                //Modified by Satyadeep on 27/05/2013
                return Mapper().QueryForList<Hashtable>("Function.GenerateLoadlist_Test", ht);
                //return Mapper().QueryForList<Hashtable>("Function.GenerateLoadList2", ht);
                //return Mapper().QueryForList<Hashtable>("Function.GenerateLoadList", ht);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetExecutorSize(Hashtable ht)
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetExecutorSize", ht);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetKeyListForMode(string mode)
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetKeyListForMode", mode);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Key Template Editor Methods
        
        public IList<Hashtable> GetPickTemplate(string mode)
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetPickTemplate", mode);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region PGS1.2
        
        public IList<Hashtable> GetPickTemplate()
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetPickTemplatePGS", null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int GetExecutorForId(string Id)
        {
            try
            {
                return (int)Mapper().QueryForObject("Function.GetExecutorForId", Id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetIdData(string Id)
        {
            Hashtable ht = new Hashtable();

            ht.Add("ID", Id);
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetIdData", ht);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        
        public IList<Hashtable> GetIdExecList()
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetIDExecList", null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

        public IList<Hashtable> GetIDTNInfo()
        {

            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetIdTNInfo", null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> GetAllExecutorsSize()
        {
            try
            {
                return Mapper().QueryForList<Hashtable>("Function.GetAllExecutorSize", null);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> SearchBrand(string brand)
        {
            try
            {
                Hashtable param = new Hashtable();
                string newVal = "%" + brand + "%";
                param.Add("Brand", newVal);
                return Mapper().QueryForList<Hashtable>("Function.SearchBrand", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Hashtable> SearchModel(string model)
        {
            try
            {
                Hashtable param = new Hashtable();
                string newVal = "%" + model + "%";
                param.Add("Model", newVal);
                return Mapper().QueryForList<Hashtable>("Function.SearchModel", param);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}

