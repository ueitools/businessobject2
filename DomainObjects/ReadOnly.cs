using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    [Serializable]
    public class IDFunctionCollection : BindingListEx<IDFunction>
    {
        bool[] picked = null;
        //used to keep track of wild card introns
        bool[] wildcardPicked = null;

        /// <summary>
        /// intron with order 
        /// </summary>
        /// <param name="intronList"></param>
        /// <returns></returns>
        public string SimplePick(IList<string> intronList,
            out string intronPicked, out string intronPriority,
            int order)
        {
            string data = String.Empty;
            intronPicked = String.Empty;
            intronPriority = null;

            if (picked == null || wildcardPicked == null)
            {
                if (this.Count > 0)
                {
                    picked = new bool[this.Count];
                    wildcardPicked = new bool[this.Count];
                }

                for (int i = 0; i < this.Count; i++)
                {
                    picked[i] = false;
                    wildcardPicked[i] = false;
                }
            }

            if (order >= intronList.Count)
                return data;

            data = SimplePick(intronList[order], out intronPicked,
                out intronPriority);

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intronList"></param>
        /// <returns></returns>
        private string SimplePick(string intron,
            out string intronPicked, out string intronPriority)
        {
            string data = String.Empty;
            intronPicked = String.Empty;
            intronPriority = null;
            string s = intron;

            data = GetDataByIntron(ref s, out intronPriority);
            if (data != String.Empty)
            {
                intronPicked = s;
            }

            return data;
        }

        /// <summary>
        /// for end user (what this function do)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public StringCollection GetLabelByData(string data)
        {
            StringCollection labelList = new StringCollection();

            foreach (IDFunction function in this)
            {
                if (function.Data == data &&
                    labelList.Contains(function.Label) != true)
                {
                    labelList.Add(function.Label);
                }
            }

            if (labelList.Count == 0)
                labelList.Add("Non functional");

            return labelList;
        }

        /// <summary>
        /// TODO: handle wild character *
        /// </summary>
        /// <param name="intron"></param>
        /// <returns></returns>
        string GetDataByIntron(ref string intron, out string intronPriority)
        {
            char[] dot = { '.' };
            string[] pattern = intron.Split(dot);
            int index = 0, selectedIndex = -1;
            string data = String.Empty;
            int priority = int.MaxValue;
            bool found = false;
            intronPriority = null;

            foreach (IDFunction function in this)
            {
                index++;

                if (function.Intron == null)
                {
                    continue;
                }

                if (PatternMatch(pattern, function.Intron.Split(dot)))
                {
                    //we deal with wild card introns differently
                    if (intron.Contains("*"))
                    {
                        if (wildcardPicked[index - 1])
                            continue;

                        data = PickData(ref intron, ref intronPriority,
                            function);
                        wildcardPicked[index - 1] = true;
                        return data;
                    }

                    found = true;
                    if (picked[index-1])
                    {
                        continue;
                    }
                    GetData(ref intron, ref intronPriority, index, 
                        ref selectedIndex,ref data, ref priority, 
                        function);
                }
            }

            if (data == String.Empty && found)
            {
                index = 0; 
                foreach (IDFunction function in this)
                {
                    index++;

                    if (function.Intron == null)
                    {
                        continue;
                    }

                    if (PatternMatch(pattern, function.Intron.Split(dot)))
                    {
                        picked[index - 1] = false;
                        GetData(ref intron, ref intronPriority, index,
                            ref selectedIndex, ref data, ref priority,
                            function);
                    }
                }
            }

            if ( selectedIndex != -1 )
                picked[selectedIndex] = true;
            return data;
        }

        private void GetData(ref string intron, ref string 
            intronPriority, int index, ref int selectedIndex, 
            ref string data, ref int priority, IDFunction function)
        {
            int p = int.MaxValue;

            if (!String.IsNullOrEmpty(
                function.IntronPriority))
                p = int.Parse(function.IntronPriority);

            if (data == String.Empty)
            {
                data = PickData(ref intron, ref intronPriority,
                    function);
                selectedIndex = index - 1;
                priority = p;
            }
            else if (p < priority)
            {
                data = PickData(ref intron, ref intronPriority,
                    function);
                selectedIndex = index - 1;
                priority = p;
            }
        }

        private string PickData(ref string intron, ref string 
            intronPriority, IDFunction function)
        {
            intronPriority = function.IntronPriority;
            intron = function.Intron;
            return function.Data;
        }

        bool PatternMatch(string[] pattern, string[] intron)
        {
            for (int ex = 0; ex < pattern.Length; ex++)
            {
                if (ex >= intron.Length)
                    return false;

                if (pattern[ex] == "*")
                    continue;

                if (pattern[ex] != intron[ex])
                    return false;
            }

            if (pattern[pattern.Length - 1] != "*" &&
                intron.Length > pattern.Length)
                return false;

            return true;
        }

        public IDFunctionCollection Clone()
        {
            IDFunctionCollection clone = new IDFunctionCollection();

            foreach (IDFunction idFunction in this)
            {
                IDFunction cloneIDFunction = new IDFunction();
                cloneIDFunction = idFunction;
                cloneIDFunction.IDTNCaptureList = new IDTNCaptureCollection();
                foreach (IDTNCapture idtnCapture in idFunction.IDTNCaptureList)
                {
                    cloneIDFunction.IDTNCaptureList.Add(idtnCapture);
                }
                clone.Add(cloneIDFunction);
            }

            return clone;
        }
    }

    /// <summary>
    /// data, label is composite key
    /// </summary>
    public class IDFunction
    {
        #region Private Fields

        private string _synth;
        private string _data;
        private string _label;
        private string _intron;
        private string _intronPriority;
        private string _comment;
        private string _IsInversedData;
        private IDTNCaptureCollection _idTNCaptureList;

        private int _rid;

        #endregion

        #region Business Properties and Methods

        public string IsInversedData
        {
            get { return _IsInversedData; }
            set { _IsInversedData = value; }
        }

        public string Synth
        {
            get 
            {
                CommonFunctions helper = new CommonFunctions();
                if (_IsInversedData == BooleanFlag.YES)
                {
                    return helper.GetSynthesizer(
                        CommonFunctions.InvertBinaryString(_data));
                }
                else
                    return helper.GetSynthesizer(_data);
            }
            set { _synth = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string FormattedData
        {
            get
            {
                CommonFunctions helper = new CommonFunctions();
                return helper.FormatBinaryData(_data);
            }
            set
            {
                CommonFunctions helper = new CommonFunctions();
                _data = helper.StripBinaryFormatting(value);
            }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _intronPriority; }
            set { _intronPriority = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public IDTNCaptureCollection IDTNCaptureList
        {
            get { return _idTNCaptureList; }
            set { _idTNCaptureList = value; }
        }

        [XmlIgnore]
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        #endregion
    }

    public class IDTNCaptureCollection : BindingListEx<IDTNCapture>
    {

    }

    public class IDTNCapture
    {
        #region Private Fields

        private string _data;
        private string _label;
        private int _tn;
        private string _filename;
        private string _legacyLabel;

        #endregion

        #region Business Properties and Methods

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public string Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public string LegacyLabel
        {
            get { return _legacyLabel; }
            set { _legacyLabel = value; }
        }

        #endregion
    }
    /// <summary>
    /// 
    /// </summary>
    public class NumericValueLabelPairCollection : BindingListEx<NumericValueLabelPair>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool FindNumber(string label, out int number)
        {
            number = -1;
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].Label.ToLower() == label.ToLower())
                {
                    number = this[i].Number;
                    return true;
                }
            }
            return false; 
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class NumericValueLabelPair
    {
        private string _label;
        private int _number;
        /// <summary>
        /// 
        /// </summary>
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }
    }
}
