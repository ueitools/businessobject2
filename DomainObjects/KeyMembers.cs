using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// _keyPositionList: key identifying number (key matrix number)
    /// for each device
    /// 
    /// e.g.) Cable: 1, 3, 5, 7
    /// </summary>
    [Serializable]
    public class DeviceInfo
    {
        #region Private Fields
        int _deviceKey_RID;
        int _deviceNumber;
        string _modeList = String.Empty;
        string _defaultID = String.Empty;
        StringCollection _keyLabelList = new StringCollection();
        #endregion

        #region Business Properties and Methods
        public int DeviceKey_RID
        {
            get { return _deviceKey_RID; }
            set { _deviceKey_RID = value; }
        }

        public int DeviceNumber
        {
            get { return _deviceNumber; }
            set { _deviceNumber = value; }
        }

        public string ModeList
        {
            get { return _modeList; }
            set { _modeList = value; }
        }

        public string DefaultID
        {
            get { return _defaultID; }
            set { _defaultID = value; }
        }

        public StringCollection KeyLabelList
        {
            get { return _keyLabelList; }
            set { _keyLabelList = value; }
        }
        #endregion

        #region Equals
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            DeviceInfo info = (DeviceInfo)obj;
            if (this.DeviceNumber != info.DeviceNumber)
                return false;
            if (this.ModeList != info.ModeList)
                return false;
            if (this.DefaultID != info.DefaultID)
                return false;
            if (!Object.Equals(this.KeyLabelList, info.KeyLabelList))
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DeviceInfoCollection : BindingListEx<DeviceInfo>
    {
        protected override object AddNewCore()
        {
            DeviceInfo deviceInfo = new DeviceInfo();
            Add(deviceInfo);
            return deviceInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key_RID"></param>
        /// <returns></returns>
        public DeviceInfo Find(int key_RID)
        {
            foreach (DeviceInfo deviceInfo in this)
            {
                if (deviceInfo.DeviceKey_RID == key_RID)
                    return deviceInfo;
            }

            return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class PDLInfo
    {
        int _rid;
        int _key_rid;
        string _mode = String.Empty;
        string _outron = String.Empty;
        int _group = -1;
        StringCollection _intronList = new StringCollection();

        #region Business Properties and Methods
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public int Key_RID
        {
            get { return _key_rid; }
            set { _key_rid = value; }
        }

        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        public string Outron
        {
            get { return _outron; }
            set { _outron = value; }
        }

        public int Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public StringCollection IntronList
        {
            get { return _intronList; }
            set { _intronList = value; }
        }
        #endregion

        #region Equals
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            PDLInfo info = (PDLInfo)obj;
            if (this.Group != info.Group)
                return false;
            if (this.Mode != info.Mode)
                return false;
            if (this.Outron != info.Outron)
                return false;
            if (!Object.Equals(this.IntronList, info.IntronList))
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class PDLInfoCollection : BindingListEx<PDLInfo>
    {
        protected override object AddNewCore()
        {
            PDLInfo pdlInfo = new PDLInfo();
            Add(pdlInfo);
            return pdlInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key_RID"></param>
        /// <returns></returns>
        public PDLInfoCollection Find(int key_RID)
        {
            PDLInfoCollection pdlInfoList = new PDLInfoCollection();

            foreach (PDLInfo pdlInfo in this)
            {
                if (pdlInfo.Key_RID == key_RID)
                    pdlInfoList.Add(pdlInfo);
            }

            return pdlInfoList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key_RID"></param>
        public void SynchKeyRID(int key_RID)
        {
            foreach (PDLInfo pdlInfo in this)
                pdlInfo.Key_RID = key_RID;
        }
    }

    [Serializable]
    public class KeyGroup
    {
        int _device_rid;
        int _order;
        int _key_rid;

        #region Business Properties and Methods
        public int Device_RID
        {
            get { return _device_rid; }
            set { _device_rid = value; }
        }

        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public int Key_RID
        {
            get { return _key_rid; }
            set { _key_rid = value; }
        }
        #endregion
    }

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class OutronIntron
    {
        #region Private Fields
        string _outron;
        int _keyOutron_rid;
        string _intron;
        int _order;
        #endregion

        #region Business Properties and Methods
        public string Outron
        {
            get { return _outron; }
            set { _outron = value; }
        }

        public int KeyOutron_RID
        {
            get { return _keyOutron_rid; }
            set { _keyOutron_rid = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class OutronIntronCollection : BindingListEx<OutronIntron>
    {
        protected override object AddNewCore()
        {
            OutronIntron outronIntron = new OutronIntron();
            Add(outronIntron);
            return outronIntron;
        }

        /// <summary>
        /// For ModePickCollection (Picking)
        /// </summary>
        /// <param name="outron"></param>
        /// <returns></returns>
        public StringCollection GetIntron(string outron)
        {
            StringCollection intronList = new StringCollection();
            foreach (OutronIntron item in this)
            {
                if (item.Outron == outron)
                    intronList.Add(item.Intron);
            }

            return intronList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyOutron_RID"></param>
        /// <returns></returns>
        public StringCollection GetIntron(int keyOutron_RID)
        {
            StringCollection intronList = new StringCollection();
            foreach (OutronIntron item in this)
            {
                if (item.KeyOutron_RID == keyOutron_RID)
                    intronList.Add(item.Intron);
            }

            return intronList;
        }
    }

    [Serializable]
    public class KeyProperty
    {
        #region Private Fields
        int _RID;
        int _Type_Code;
        string _Type_Name;
        #endregion

        #region Business Properties and Methods
        public int RID
        {
            get { return _RID; }
            set { _RID = value; }
        }

        public int Type_Code
        {
            get { return _Type_Code; }
            set { _Type_Code = value; }
        }

        public string Type_Name
        {
            get { return _Type_Name; }
            set { _Type_Name = value; }
        }
        #endregion

        #region Equals
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            KeyProperty info = (KeyProperty)obj;
            if (this.RID != info.RID)
                return false;
            if (this.Type_Code != info.Type_Code)
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class KeyPropertyCollection : BindingListEx<KeyProperty>
    {
        protected override object AddNewCore()
        {
            KeyProperty keyProperty = new KeyProperty();
            Add(keyProperty);
            return keyProperty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key_RID"></param>
        /// <returns></returns>
        public KeyProperty Find(int key_RID)
        {
            foreach (KeyProperty keyProperty in this)
            {
                if (keyProperty.RID == key_RID)
                    return keyProperty;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public KeyProperty Find(string typeName)
        {
            foreach (KeyProperty keyProperty in this)
            {
                if (keyProperty.Type_Name == typeName)
                    return keyProperty;
            }

            return null;
        }
    }

    [Serializable]
    public class KeyType
    {
        #region Private Fields
        int _Code;
        string _Name;
        #endregion

        #region Business Properties and Methods
        public int Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        #endregion

        #region Equals
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            KeyType info = (KeyType)obj;
            if (this._Code != info._Code)
                return false;
            if (this._Name != info._Name)
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class KeyTypeCollection : BindingListEx<KeyType>
    {
        protected override object AddNewCore()
        {
            KeyType keyType = new KeyType();
            Add(keyType);
            return keyType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public KeyType Find(string typeName)
        {
            foreach (KeyType keyType in this)
            {
                if (keyType.Name == typeName)
                    return keyType;
            }

            return null;
        }
    }
}
