using System;
using System.Xml.Serialization;
using System.IO;
using BusinessObject.LockStatus;
using UEIException;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class TN : ITnHelper
    {
        #region Private Fields

        private TNHeader _header = new TNHeader();
        private FunctionCollection _functionList = new FunctionCollection();
        private DeviceCollection _deviceList = new DeviceCollection();

        #endregion

        #region Business Properties and Methods

        public TNHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public FunctionCollection FunctionList
        {
            get { return _functionList; }
            set { _functionList = value; }
        }

        public DeviceCollection DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }

        #endregion

        public bool IsReady()
        {
            return (_header != null && _header.Status == StatusFlag.PASSED_QA);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tn"></param>
        public void Load(int tn)
        {
            _header = DAOFactory.TNHeader().Select(tn);
            _functionList = DAOFactory.Function().Select(tn);
            _deviceList = DAOFactory.Device().Select(tn);

            foreach (Function function in _functionList)
                function.Preprocess();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert()
        {
            DAOFactory.TNHeader().Insert(_header);
            DAOFactory.Function().Insert(_header.TN, _functionList);
            DAOFactory.Device().Insert(_header.TN, _deviceList);
        }

        public void Update()
        {
            DAOFactory.Device().Delete(_header.TN);
            DAOFactory.Function().Delete(_header.TN);

            DAOFactory.TNHeader().Update(_header);
            DAOFactory.Function().Insert(_header.TN, _functionList);
            DAOFactory.Device().Insert(_header.TN, _deviceList);
        }

        public void Save()
        {
            if (DAOFactory.TNHeader().DoesExistNoFilter(_header.TN) == true)
                Update();
            else
                Insert();
        }

        private static TnLock GetLockedTn(int tn) {
            return GetLockedTn(tn, true);
        }

        private static TnLock GetLockedTn(int tn, bool useDomainName) {
            string domainName = string.Empty;
            if (useDomainName) {
                domainName = DBFunctions.GetDBDomain();
            }

            LockStatusService lockStatus = new LockStatusService();
            TnLock[] lockedTNs = lockStatus.GetLockedTNs(domainName);
            TnLock lockedTN = null;
            foreach (TnLock tnLock in lockedTNs) {
                if (tnLock.TN == tn) {
                    lockedTN = tnLock;
                }
            }

            return lockedTN;
        }

        public static bool IsLocked(int tn) {
            TnLock lockedTN = GetLockedTn(tn);
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC) {
                if (lockedTN == null)
                    throw new Exception("TN " + tn + " isn't checked out!");

                TNHeader header = DAOFactory.TNHeader().SelectHeaderOnly(tn) as TNHeader;
                return (header.IsLocked == BooleanFlag.YES);
            }

            return (lockedTN != null);
        }

        public static int Lock(int tn)
        {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC) {
                TnLock lockedTN = GetLockedTn(tn);
                if (lockedTN == null)
                    throw new Exception("TN " + tn + " isn't checked out!");

                TNHeader header = DAOFactory.TNHeader().SelectHeaderOnly(tn) as TNHeader;
                if (header.IsLocked == BooleanFlag.YES)
                    throw new TNLockException("TN " + tn + " is locked");

                header.IsLocked = BooleanFlag.YES;
                DAOFactory.TNHeader().UpdateHeaderOnly(header);

                string userName = Environment.UserName;
                if (lockedTN.UserName != userName) {
                    return 1;
                }
            } else {
                LockStatusService lockStatus = new LockStatusService();
                int lockTNStatus = lockStatus.LockTn(tn, Environment.UserDomainName, Environment.UserName);
                if (lockTNStatus != 0) {
                    throw new TNLockException(string.Format("Error locking TN{0}.\n\n{1}", tn, lockStatus.GetMessage(lockTNStatus)));
                }
            }

            return 0;
        }

        public static void ForceUnLock(int tn) {
            TnLock lockedTN = GetLockedTn(tn, false);
            UnLock(tn, lockedTN.DomainName, "FORCED");
        }

        public static void UnLock(int tn) {
            UnLock(tn, Environment.UserDomainName, Environment.UserName);
        }

        private static void UnLock(int tn, string domainName, string userName) {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC) {
                TNHeader header = DAOFactory.TNHeader().SelectHeaderOnly(tn) as TNHeader;

                //if (header.IsLocked != BooleanFlag.YES)
                //    throw new Exception("Lock has been corrupted for TN: " + tn);

                header.IsLocked = BooleanFlag.NO;
                DAOFactory.TNHeader().UpdateHeaderOnly(header);
            } else {
                LockStatusService lockStatus = new LockStatusService();
                int lockTNStatus = lockStatus.UnlockTn(tn, domainName, userName);
                if (lockTNStatus < 0) {
                    throw new TNLockException(string.Format("Error unlocking TN{0}.\n\n{1}", tn, lockStatus.GetMessage(lockTNStatus)));
                }
            }
        }

        public static void Erase(int tn)
        {
            DAOFactory.Device().Delete(tn);
            DAOFactory.Function().Delete(tn);
            DAOFactory.TNHeader().MakeUnAvailable(tn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void ToXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            Stream s = File.Create(fileName);
            x.Serialize(s, this);
            s.Close();
        }

        public void LoadFromXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            Stream s = File.OpenRead(fileName);
            TN oTN = (TN)x.Deserialize(s);
            s.Close();

            this.Header = oTN.Header;
            this.FunctionList = oTN.FunctionList;
            this.DeviceList = oTN.DeviceList;
        }
    }

    public interface ITnHelper {
        TNHeader Header { get; }
        FunctionCollection FunctionList { get; set; }
        DeviceCollection DeviceList { get; set; }

        bool IsReady();
        void Load(int tn);
    }
}
