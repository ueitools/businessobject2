using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public enum FIRMWARETYPE { SOFTWARE, LIBRARY };

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Firmware
    {
        #region Private Fields
        FirmwareHeader _header = new FirmwareHeader();
        DigitGroupCollection _digitGroupList = new DigitGroupCollection();
        AddressCollection _addressList = new AddressCollection();
        ExecutorLoadCollection _executorList = new ExecutorLoadCollection();
        IDLoadCollection _idList = new IDLoadCollection();
        AlternateIDCollection _alternateIDList = new AlternateIDCollection();
        GoldenImageCollection _goldenImageList = new GoldenImageCollection();
        #endregion

        #region Business Properties and Methods
        public FirmwareHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public DigitGroupCollection DigitGroupList
        {
            get { return _digitGroupList; }
            set { _digitGroupList = value; }
        }

        public AddressCollection AddressList
        {
            get { return _addressList; }
            set { _addressList = value; }
        }

        public ExecutorLoadCollection ExecutorList
        {
            get { return _executorList; }
            set { _executorList = value; }
        }

        public IDLoadCollection IDList
        {
            get { return _idList; }
            set { _idList = value; }
        }

        public AlternateIDCollection AlternateIDList
        {
            get { return _alternateIDList; }
            set { _alternateIDList = value; }
        }

        public GoldenImageCollection GoldenImage
        {
            get { return _goldenImageList; }
            set { _goldenImageList = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null) 
                return false;
            if (this.GetType() != obj.GetType()) 
                return false;

            Firmware firmware = (Firmware)obj;
            if (!Object.Equals(this.Header, firmware.Header)) 
                return false;
            if (!Object.Equals(this.DigitGroupList, firmware.DigitGroupList))
                return false;
            if (!Object.Equals(this.AddressList, firmware.AddressList))
                return false;
            if (!Object.Equals(this.ExecutorList, firmware.ExecutorList))
                return false;
            if (!Object.Equals(this.IDList, firmware.IDList))
                return false;

            return true;
        }
        #endregion

        public void SyncProjectCode(int code)
        {
            this.Header.Project_Code = code;
            this.AddressList.SyncProjectCode(code);
            this.DigitGroupList.SyncProjectCode(code);
            this.IDList.SyncProjectCode(code);
            this.ExecutorList.SyncProjectCode(code);
            this.AlternateIDList.SyncProjectCode(code);
        }
    }

    /// <summary>
    /// default value to 0, empty string and No
    /// </summary>
    [Serializable]
    public class FirmwareHeader
    {
        #region Private Fields
        int _project_code;
        int _maxExecutorSize = 0;
        string _isTwoByteExecutorCode = BooleanFlag.NO;
        string _isCheckSum = BooleanFlag.NO;
        string _includePath = String.Empty;
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_code; }
            set { _project_code = value; }
        }

        public int MaxExecutorSize
        {
            get { return _maxExecutorSize; }
            set { _maxExecutorSize = value; }
        }

        public string IsTwoByteExecutorCode
        {
            get { return _isTwoByteExecutorCode; }
            set { _isTwoByteExecutorCode = value; }
        }

        public string IsCheckSum
        {
            get { return _isCheckSum; }
            set { _isCheckSum = value; }
        }

        public string IncludePath
        {
            get { return _includePath; }
            set { _includePath = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            FirmwareHeader header = (FirmwareHeader)obj;
            if (this.MaxExecutorSize != header.MaxExecutorSize)
                return false;
            if (this.IsTwoByteExecutorCode != header.IsTwoByteExecutorCode)
                return false;
            if (this.IsCheckSum != header.IsCheckSum)
                return false;
            if (this.IncludePath != header.IncludePath)
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Image
    {
        #region Private Fields
        string _id;
        int _executor_code;
        string _version;
        DateTime _imageDate;
        byte[] _data;
        #endregion

        #region Business Properties and Methods
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Executor_Code
        {
            get { return _executor_code; }
            set { _executor_code = value; }
        }

        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public DateTime ImageDate
        {
            get { return _imageDate; }
            set { _imageDate = value; }
        }

        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }
        #endregion
    }

    [Serializable]
    public class ImageCollection : BindingListEx<Image>
    {
        protected override object AddNewCore()
        {
            Image image = new Image();
            Add(image);
            return image;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DigitGroupRow
    {
        int _project_code;
        int _code;
        int _order;
        string _data;

        #region Public Properties (GUI and DAC)
        public int Project_Code
        {
            get { return _project_code; }
            set { _project_code = value; }
        }

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public int Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class GoldenImage
    {
        int projectCode;
        byte[] binaryData;
        int rid;
        DateTime dateTime;
        string description;
        string attribute;

        #region Public Properties
        public int ProjectCode
        {
            get { return projectCode; }
            set { projectCode = value; }    
        }
        public byte[] BinaryData
        {
            get { return binaryData; }
            set { binaryData = value; }
        }
        public int RID
        {
            get { return rid; }
            set { rid = value; }
        }
        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Attribute
        {
            get { return attribute; }
            set { attribute = value; }
        }
        #endregion
    }

    [Serializable]
    public class GoldenImageCollection : BindingListEx<GoldenImage>
    {
        protected override object AddNewCore()
        {
            GoldenImage image = new GoldenImage();
            Add(image);
            return image;
        }
    }
}
