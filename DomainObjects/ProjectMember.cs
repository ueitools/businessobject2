using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Config
    {
        #region Private Fields
        int _project_code;
        string _name;
        string _value;
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_code; }
            set { _project_code = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            Config config = (Config)obj;
            if (this.Name != config.Name)
                return false;
            if (this.Value != config.Value)
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ConfigCollection : BindingListEx<Config>
    {
        protected override object AddNewCore()
        {
            Config config = new Config();
            Add(config);
            return config;
        }

        /// <summary>
        /// don't allow duplicate: overwrite
        /// </summary>
        /// <param name="config"></param>
        public void Add(Config config)
        {
            Config found = Find(config.Name);
            if (found != null)
                found.Value = config.Value;
            else
                base.Add(config);
        }

        /// <summary>
        /// don't allow duplicate: overwrite
        /// </summary>
        /// <param name="configList"></param>
        public void Append(ConfigCollection configList)
        {
            foreach (Config config in configList)
                this.Add(config);
        }

        public Config Find(string name)
        {
            foreach (Config config in this)
            {
                if (config.Name.ToLower() == name.ToLower())
                    return config;
            }

            return null;
        }

        public void SyncProjectCode(int code)
        {
            foreach (Config config in this)
                config.Project_Code = code;
        }
    }

    /// <summary>
    /// Combined Chip and Project_Chip tables.
    /// </summary>
    [Serializable]
    public class Chip
    {
        #region Private Fields
        int _project_code;
        int _chip_code;
        string _number;
        #endregion

        #region Business Properties and Methods
        /// <summary>
        /// Project code number
        /// </summary>
        public int Project_Code
        {
            get { return _project_code; }
            set { _project_code = value; }
        }

        /// <summary>
        /// Chip code number
        /// </summary>
        public int Chip_Code
        {
            get { return _chip_code; }
            set { _chip_code = value; }
        }

        /// <summary>
        /// Chip name string
        /// </summary>
        public string Number
        {
            get { return _number; }
            set { _number = value; }
        }
        #endregion  
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ChipCollection : BindingListEx<Chip>
    {
        protected override object AddNewCore()
        {
            Chip chip = new Chip();
            Add(chip);
            return chip;
        }

        public void SyncProjectCode(int code)
        {
            foreach (Chip chip in this)
                chip.Project_Code = code;
        }
    }
}
