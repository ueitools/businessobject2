using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class Function
    {
        #region Private Fields
        private int _rid;
        private int _tn;
        private string _id;
        private string _isConnection = "N";
        private string _label;

        private string _intron;
        private string _intronPriority;
        private string _data;
        private string _comment;
        private string _legacyLabel;

        private string _filename;
        private string _synth;
        private string _IsInversedData;
        #endregion

        public Function()
        {
            _intron = string.Empty;
            _intronPriority = string.Empty;
            _label = string.Empty;
            _legacyLabel = string.Empty;
            _comment = string.Empty;
            _id = string.Empty;
        }

        #region Business Properties and Methods

        [XmlIgnore]
        public string IsInversedData
        {
            get { return _IsInversedData; }
            set { _IsInversedData = value; }
        }

        [XmlIgnore]
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string IsConnection
        {
            get { return _isConnection; }
            set { _isConnection = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _intronPriority; }
            set { _intronPriority = value; }
        }

        public string FormattedData
        {
            get
            {
                CommonFunctions helper = new CommonFunctions();
                return helper.FormatBinaryData(_data);
            }
            set
            {
                CommonFunctions helper = new CommonFunctions();
                value = helper.StripBinaryFormatting(value);
                if (value.Contains("."))        // frequency data
                    value = CommonFunctions.ValidateFrequencyData(value);
                else                            // binary data - 
                    value = CommonFunctions.ValidateBinaryData(value);

                if (value != null)
                    _data = value;
            }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public string LegacyLabel
        {
            get { return _legacyLabel; }
            set { _legacyLabel = value; }
        }

        public string Filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        [XmlIgnore]
        public string Synth
        {
            get 
            {
                CommonFunctions helper = new CommonFunctions();
                if (_IsInversedData == BooleanFlag.YES)
                {
                    return helper.GetSynthesizer(
                        CommonFunctions.InvertBinaryString(_data));
                }
                else
                    return helper.GetSynthesizer(_data);
            }
            set { _synth = value; }
        }

        #endregion

        public void Preprocess()
        {
            if (_intron == null)
                _intron = string.Empty;

            if (_intronPriority == null)
                _intronPriority = string.Empty;

            if (_legacyLabel == null)
                _legacyLabel = string.Empty;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FunctionCollection : BindingListEx<Function>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            Function function = new Function();
            Add(function);
            return function;
        }
    }
}
