///
/// IList will be replaced with grid-bindable typed collection 
///
using System;
using System.Collections.Generic;

///
///
///
namespace BusinessObject
{
    /// <summary>
    /// General View of ID data
    /// 
    /// IDHeader, IDFunctionCollection and DeviceCollection 
    /// are defined in BusinessObject.dll
    /// 
    /// TODO: seprate DomainObject definition out of BusinessObject
    /// </summary>
    [Serializable]
    public class ID
    {
        #region Private Fields
        IDHeader _header;
        IDFunctionCollection _functionList;
        DeviceCollection _deviceList;
        #endregion

        #region Business Properties and Methods
        public IDHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public IDFunctionCollection FunctionList
        {
            get { return _functionList; }
            set { _functionList = value; }
        }

        public DeviceCollection DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }
        #endregion

        public string SimplePick(IList<string> intronList,
            out string intronPicked, out string intronPriority, int order)
        {
            return FunctionList.SimplePick(intronList, out intronPicked,
                out intronPriority, order);
        }

        public StringCollection GetLabelByData(string data)
        {
            return FunctionList.GetLabelByData(data);
        }
        /// <summary>
        /// 
        /// </summary>
        public void FrequencyDataToBinary()
        {
            uint dec; 
            CommonFunctions helper = new CommonFunctions();

            foreach (IDFunction function in FunctionList)
            {
                dec = helper.FrequencyToDecimal(function.Data);
                function.Data = helper.DecimalToBinary(dec);
            }
        }
    }
}

