using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// Business Key: Project_Code and Label
    /// Surrogate Key: RID 
    /// 
    /// Only one data map for each mode in ModePickList
    /// (Key label is decided already)
    /// </summary>
    [Serializable]
    public class Key
    {
        #region Private Fields
        int _rid;
        int _project_code;
        string _label;
        int _position;
        string _description;
        int _scan_code;
        DeviceInfo _deviceInfo;
        PDLInfoCollection _pdlInfoList = new PDLInfoCollection();
        KeyPropertyCollection _keyPropertyList = new KeyPropertyCollection();

        #endregion

        #region Business Properties and Methods
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public int Project_Code
        {
            get { return _project_code; }
            set { _project_code = value; }
        }

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int Scan_Code
        {
            get { return _scan_code; }
            set { _scan_code = value; }
        }

        public DeviceInfo DeviceInfo
        {
            get { return _deviceInfo; }
            set { _deviceInfo = value; }
        }

        public PDLInfoCollection PDLInfoList
        {
            get { return _pdlInfoList; }
            set { _pdlInfoList = value; }
        }

        public KeyPropertyCollection KeyPropertyList
        {
            get { return _keyPropertyList; }
            set { _keyPropertyList = value; }
        }

        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;

            if (this.GetType() != obj.GetType())
                return false;

            Key key = (Key)obj;
            if (this.Label != key.Label)
                return false;
            if (this.Position != key.Position)
                return false;
            if (this.Description != key.Description)
                return false;
            if (this.Scan_Code != key.Scan_Code)
                return false;
            if (!Object.Equals(this.DeviceInfo, key.DeviceInfo))
                return false;
            if (!Object.Equals(this.PDLInfoList, key.PDLInfoList))
                return false;
            if (!Object.Equals(this.KeyPropertyList, key.KeyPropertyList))
                return false;

            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class KeyCollection : BindingListEx<Key>
    {
        /// <summary>
        /// 
        /// </summary>
        const string DEVICE = "Device";
        const string device = "device";
        const string DEFAULTID = "Default";
        const string LABEL = "Label";
        const string NAME = "DeviceName";
        const int MAXDEVICECOUNT = 16;

        protected override object AddNewCore()
        {
            Key key = new Key();
            Add(key);
            return key;
        }

        public Key Find(int key_RID)
        {
            foreach (Key key in this)
            {
                if (key.RID == key_RID)
                    return key;
            }

            return null;
        }

        public Key Find(string label)
        {
            foreach (Key key in this)
            {
                if (key.Label == label)
                    return key;
            }

            return null;
        }

        /// <summary>
        /// for a mode (Y mode) get all device keys 
        /// (DVD Device, Audio Device)
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public KeyCollection GetDeviceKeyByMode(string mode)
        {
            KeyCollection keyList = new KeyCollection();
            foreach (Key key in this)
            {
                if (key.DeviceInfo != null &&
                    key.DeviceInfo.ModeList.Contains(mode) == true)
                {
                    keyList.Add(key);
                }
            }

            return keyList;
        }

        /// <summary>
        /// Get all device keys
        /// </summary>
        /// <returns></returns>
        public KeyCollection GetDeviceKey()
        {
            KeyCollection keyList = new KeyCollection();
            foreach (Key key in this)
            {
                if (key.DeviceInfo != null)
                    keyList.Add(key);
            }

            return keyList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceKey"></param>
        /// <returns></returns>
        public KeyCollection GetSubKey(Key deviceKey)
        {
            KeyCollection keyList = new KeyCollection();
            if (deviceKey.DeviceInfo == null)
                return keyList;

            foreach (string label in deviceKey.DeviceInfo.KeyLabelList)
            {
                Key key = this.Find(label);
                if (key == null)
                {
                    throw new ApplicationException(String.Format(
                        "Key label {0} can't found in key list",
                        label));
                }

                keyList.Add(key);
            }

            return keyList;
        }

        public void SyncProjectCode(int code)
        {
            foreach (Key key in this)
                key.Project_Code = code;
        }

        /// <summary>
        /// rid starts from 1
        /// position will be -1, -2 ..... for (0, 1, 2 ....)
        /// 
        /// label should be unique in project 
        /// (default is device number: Device 1)
        /// </summary>
        /// <returns></returns>
        public static KeyCollection GetDeviceKey(ConfigCollection configList)
        {
            KeyCollection keyList = new KeyCollection();
            string device = String.Empty;
            string defaultID = String.Empty;
            string label = String.Empty;
            Config config = null;
            char[] delimiter = {';'};

            for (int i = 0; i < MAXDEVICECOUNT; i++)
            {
                config = configList.Find(
                    String.Format("{0}{1}", DEVICE, i));

                if ( config == null )
                    config = configList.Find(
                        String.Format("{0}{1}", device, i));

                if (config != null)
                {
                    device = config.Value.Split(delimiter)[0];
                    device = device.Trim();
                    configList.Remove(config);

                    config = configList.Find(
                        String.Format("{0}{1}", DEFAULTID, i));
                    if (config != null)
                    {
                        defaultID = config.Value;
                        configList.Remove(config);
                    }

                    config = configList.Find(String.Format("{0}{1}", LABEL, i));
                    if (config == null)
                        config = configList.Find(String.Format("{0}{1}", NAME, i));

                    if (config != null)
                    {
                        label = config.Value;
                        configList.Remove(config);

                        config = configList.Find(String.Format("{0}{1}", NAME, i));
                        if (config != null)
                            configList.Remove(config);
                    }
                    else
                        label = String.Format("Device {0}", i);

                    Key key = new Key();
                    key.Position = -1 + -1 * i;
                    key.Description = label;
                    key.Label = label + ".";
                    key.RID = i + 1;

                    key.DeviceInfo = new DeviceInfo();
                    key.DeviceInfo.DefaultID = defaultID;
                    key.DeviceInfo.DeviceNumber = i;
                    key.DeviceInfo.ModeList = device;
                    key.DeviceInfo.DeviceKey_RID = i;

                    if (key.KeyPropertyList == null)
                        key.KeyPropertyList = new KeyPropertyCollection();

                    KeyProperty keyProperty = new KeyProperty();
                    keyProperty.Type_Code = 2;
                    key.KeyPropertyList.Add(keyProperty);

                    keyList.Add(key);
                }
                else
                {
                    break;
                }
            }

            return keyList;
        }
    }
}
