using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class Address
    {
        #region Private Fields
        int _project_Code;
        string _version;
        FIRMWARETYPE _type;
        string _versionAddress;
        string _beginAddress;
        string _endAddress;
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_Code; }
            set { _project_Code = value; }
        }

        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public FIRMWARETYPE Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string VersionAddress
        {
            get { return _versionAddress; }
            set { _versionAddress = value; }
        }

        public string BeginAddress
        {
            get { return _beginAddress; }
            set { _beginAddress = value; }
        }

        public string EndAddress
        {
            get { return _endAddress; }
            set { _endAddress = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            Address address = (Address)obj;
            if (this.Version != address.Version)
                return false;
            if (this.Type != address.Type)
                return false;
            if (this.VersionAddress != address.VersionAddress)
                return false;
            if (this.BeginAddress != address.BeginAddress)
                return false;
            if (this.EndAddress != address.EndAddress)
                return false;

            return true;
        }
        #endregion
    }

    [Serializable]
    public class AddressCollection : BindingListEx<Address>
    {
        protected override object AddNewCore()
        {
            Address address = new Address();
            Add(address);
            return address;
        }

        public void SyncProjectCode(int code)
        {
            foreach (Address address in this)
                address.Project_Code = code;
        }

       /// <summary>
        /// Assumption: sequential data 
        /// version, versionAddress, beginAddress, endAddress
       /// </summary>
       /// <param name="configList"></param>
       /// <returns></returns>
        public static AddressCollection GetAddress(ConfigCollection configList)
        {
            AddressCollection addressList =
                Find(SoftwareAddress.Get(), configList);

            addressList.Append(Find(LibraryAddress.Get(), configList));
            return addressList;
        }
        /// <summary>
        /// remove the address from the config list
        /// </summary>
        /// <param name="configList"></param>
        public static void RemoveAddress(ConfigCollection configList)
        {
            Remove(SoftwareAddress.Get(), configList);
            Remove(LibraryAddress.Get(), configList);
        }

        static void Remove(AddressInfo info, ConfigCollection configList)
        {
            Config config = configList.Find(info.Version);

            if ( config != null)
                configList.Remove(config);

            config = configList.Find(info.VersionAddress);

            if ( config != null)
                configList.Remove(config);

            config = configList.Find(info.BeginAddress);

            if (config != null)
                configList.Remove(config);

            config = configList.Find(info.EndAddress);

            if (config != null)
                configList.Remove(config);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="configList"></param>
        /// <returns></returns>
        static AddressCollection Find(AddressInfo info, 
            ConfigCollection configList)   
        {
            AddressCollection addressList = new AddressCollection();
            Address address = null;

            Config config = configList.Find(info.Version);

            if (config != null)
            {
                address = new Address();
                address.Type = info.Type;
                address.Version = config.Value;
            }
            else
                return addressList;

            config = configList.Find(info.VersionAddress);

            if (config != null)
            {
                address.VersionAddress = config.Value;
            }
            else
                return addressList;

            config = configList.Find(info.BeginAddress);

            if (config != null)
            {
                address.BeginAddress = config.Value;
            }
            else
                return addressList;

            config = configList.Find(info.EndAddress);

            if (config != null)
            {
                address.EndAddress = config.Value;
            }
            else
                return addressList;

            addressList.Add(address);

            return addressList;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    class AddressInfo
    {
        public FIRMWARETYPE Type;
        public string Version;
        public string VersionAddress;
        public string BeginAddress;
        public string EndAddress;
    }

    static class LibraryAddress
    {
        public static AddressInfo Get()
        {
            AddressInfo info = new AddressInfo();
            info.Type = FIRMWARETYPE.LIBRARY;
            info.Version = "Version";
            info.VersionAddress = "VerNoAddr";
            info.BeginAddress = "ExtCodBeg";
            info.EndAddress = "ExtCodEnd";

            return info;
        }
    }

    static class SoftwareAddress
    {
        public static AddressInfo Get()
        {
            AddressInfo info = new AddressInfo();
            info.Type = FIRMWARETYPE.SOFTWARE;
            info.Version = "SoftVersion";
            info.VersionAddress = "SoftVerAddr";
            info.BeginAddress = "RecAreaBeg";
            info.EndAddress = "RecAreaEnd";

            return info;
        }
    }
}
