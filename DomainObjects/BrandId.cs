using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    public class BrandId
    {
        string _brand;
        string _id;

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }
    }
}
