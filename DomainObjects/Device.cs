using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class Device
    {
        const string _DEFAULT_BRANCH = "CA";

        public Device()
        {
            _branch = _DEFAULT_BRANCH;
            _isRetail = BooleanFlag.NO;
            _isPartNumber = BooleanFlag.NO;

            _countryList = new CountryCollection();
            _deviceTypeList = new DeviceTypeCollection();
            _opInfoList = new OpInfoCollection();
        }

        #region Private Fields
        private string _id;
        private int _tn;
        private String _country     = String.Empty;
        private String _component   = String.Empty;
        private int _rid;			
        private string _brand;
        private int _brand_code;
        private string _manufacturer;
        private int _manufacturer_code;
        private string _ueiProduct;
        private int _ueiProduct_code;

        private string _model;
        private string _branch;
        private string _date;
        private string _collector;
        private string _purchaseYear;

        private string _priceInDigit;
        private string _productManual;
        private string _isCodebook;
        private string _isTarget;
        private string _isPartNumber;

        private string _isRetail;
        private string _source_RID;
        private string _text;
        private String _AliasBrand = String.Empty;
        private String _AliasType = String.Empty;

        private CountryCollection _countryList;
        private DeviceTypeCollection _deviceTypeList;
        private OpInfoCollection _opInfoList;
        #endregion

        #region Business Properties and Methods
        public String Country
        {
            get { return _country; }
            set { _country = value; }
        }
        public String Component
        {
            get { return _component; }
            set { _component = value; }
        }
        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        [XmlIgnore]
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }
        public String AliasBrand
        {
            get { return _AliasBrand; }
            set { _AliasBrand = value; }
        }
        public String AliasType
        {
            get { return _AliasType; }
            set { _AliasType = value; }
        }
        [XmlIgnore]
        public int Brand_Code
        {
            get { return _brand_code; }
            set { _brand_code = value; }
        }

        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }

        [XmlIgnore]
        public int Manufacturer_Code
        {
            get { return _manufacturer_code; }
            set { _manufacturer_code = value; }
        }

        public string UEIProduct
        {
            get { return _ueiProduct; }
            set { _ueiProduct = value; }
        }

        [XmlIgnore]
        public int UEIProduct_Code
        {
            get { return _ueiProduct_code; }
            set { _ueiProduct_code = value; }
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }

        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public DateTime Date2
        {
            set { _date = value.ToString(); }
        }

        public string Collector
        {
            get { return _collector; }
            set { _collector = value; }
        }

        public string PurchaseYear
        {
            get { return _purchaseYear; }
            set { _purchaseYear = value; }
        }

        public string PriceInDigit
        {
            get { return _priceInDigit; }
            set { _priceInDigit = value; }
        }

        public string ProductManual
        {
            get { return _productManual; }
            set { _productManual = value; }
        }

        public string IsTarget
        {
            get { return _isTarget; }
            set 
            { 
                _isTarget = value;                
            }
        }

        public string IsCodebook
        {
            get { return _isCodebook; }
            set { _isCodebook = value; }
        }

        public string IsPartNumber
        {
            get { return _isPartNumber; }
            set 
            {
                if (value == null)
                    _isPartNumber = BooleanFlag.NO;
                else _isPartNumber = value;
            }
        }

        public string IsRetail
        {
            get { return _isRetail; }
            set 
            {
                if (value == null)
                    _isRetail = BooleanFlag.NO;
                else _isRetail = value; 
            }
        }

        public string Source_RID
        {
            get { return _source_RID; }
            set { _source_RID = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public CountryCollection CountryList
        {
            get { return _countryList; }
            set { _countryList = value; }
        }
        public DeviceTypeCollection DeviceTypeList
        {
            get { return _deviceTypeList; }
            set { _deviceTypeList = value; }
        }
        public OpInfoCollection OpInfoList
        {
            get { return _opInfoList; }
            set { _opInfoList = value; }
        }
        #endregion
    }

    public class DeviceCollection : BindingListEx<Device>
    {
        public DeviceCollection Clone()
        {
            DeviceCollection clone = new DeviceCollection();

            foreach (Device device in this)
            {
                Device cloneDevice = new Device();

                cloneDevice = device;
                cloneDevice.CountryList = new CountryCollection();
                foreach (Country country in device.CountryList)
                {
                    cloneDevice.CountryList.Add(country);
                }
                cloneDevice.DeviceTypeList = new DeviceTypeCollection();
                foreach (DeviceType deviceType in device.DeviceTypeList)
                {
                    cloneDevice.DeviceTypeList.Add(deviceType);
                }
                cloneDevice.OpInfoList = new OpInfoCollection();
                foreach (OpInfo opInfo in device.OpInfoList)
                {
                    cloneDevice.OpInfoList.Add(opInfo);
                }
            }

            return clone;
        }

        protected override object AddNewCore()
        {
            Device device = new Device();
            Add(device);
            return device;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Relation
    {
        int _parent;
        int _child;

        public int Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        public int Child
        {
            get { return _child; }
            set { _child = value; }
        }
    }
}
