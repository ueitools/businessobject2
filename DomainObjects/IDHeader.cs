using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class IDHeader : INotifyPropertyChanged
    {
        [field:NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(info));
            }
        }

        #region Private Fields

        private string _id;
        private string _parent_ID;
        private int _executor_Code = -1;
        private string _status = StatusFlag.MODIFIED;
        private string _isInversedData = BooleanFlag.NO;

        private string _isExternalPrefix = BooleanFlag.NO;
        private string _isRestricted = BooleanFlag.NO;
        private string _isFrequencyData = BooleanFlag.NO;
        private string _isInversedPrefix = BooleanFlag.NO;
        private string _isHexFormat = BooleanFlag.NO;

        private string _text;
        private string _isLocked = BooleanFlag.NO;

        private PrefixCollection _prefixList = new PrefixCollection();
        private LogCollection _logList = new LogCollection();

        #endregion

        #region Business Properties and Methods
        public string ID
        {
            get { return _id; }
            set { _id = value; 
                OnPropertyChanged("ID"); }
        }

        public string Parent_ID
        {
            get { return _parent_ID; }
            set { _parent_ID = value; 
                OnPropertyChanged("Parent_ID"); }
        }

        public int Executor_Code
        {
            get { return _executor_Code; }
            set { _executor_Code = value; 
                OnPropertyChanged("Executor_Code"); }
        }

        [XmlIgnore]
        public string Status
        {
            get { return _status; }
            set { _status = value; 
                OnPropertyChanged("Status"); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string IsInversedData
        {
            get { return _isInversedData; }
            set { _isInversedData = value;
                OnPropertyChanged("IsInversedData");}
        }

        public string IsExternalPrefix
        {
            get { return _isExternalPrefix; }
            set { _isExternalPrefix = value;
                OnPropertyChanged("IsExternalPrefix");}
        }

        public string IsRestricted
        {
            get { return _isRestricted; }
            set { _isRestricted = value; 
                OnPropertyChanged("IsRestricted"); }
        }

        public string IsFrequencyData
        {
            get { return _isFrequencyData; }
            set { _isFrequencyData = value; 
                OnPropertyChanged("IsFrequencyData"); }
        }

        public string IsInversedPrefix
        {
            get { return _isInversedPrefix; }
            set { _isInversedPrefix = value; 
                OnPropertyChanged("IsInversedPrefix"); }
        }

        public string IsHexFormat
        {
            get { return _isHexFormat; }
            set { _isHexFormat = value; 
                OnPropertyChanged("IsHexFormat"); }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; 
                OnPropertyChanged("Text"); }
        }

        [XmlIgnore]
        public string IsLocked
        {
            get { return _isLocked; }
            set
            {
                _isLocked = value;
                OnPropertyChanged("IsLocked");
            }
        }

        public PrefixCollection PrefixList
        {
            get { return _prefixList; }
            set { _prefixList = value; }
        }

        public LogCollection LogList
        {
            get { return _logList; }
            set { _logList = value; }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class Prefix
    {
        #region Private Fields
        private Guid _rid;
        private string _id;
        private string _data;
        private string _description;
        #endregion

        #region Business Properties and Methods

        [XmlIgnore]
        public Guid RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Data
        {
            get 
            {
                CommonFunctions helper = new CommonFunctions();
                return helper.FormatBinaryData(_data);
            }
            set
            {
                CommonFunctions helper = new CommonFunctions();
                value = helper.StripBinaryFormatting(value);
                if (value == null)
                {
                    //                    throw new ApplicationException("Data of Prefix can't be NULL.");
                    _data = "";                     // Null is not allowed, set it to "empty"
                }
                else
                {
                    if (value.Contains("."))        // frequency data
                        value = CommonFunctions.ValidateFrequencyData(value);
                    else                            // binary data - 
                        value = CommonFunctions.ValidateBinaryData(value);
                    _data = value;
                }   
            }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class PrefixCollection : BindingList<Prefix>
    {
        public PrefixCollection Clone()
        {
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            f.Serialize(s, this);
            s.Position = 0;
            object clone = f.Deserialize(s);
            return clone as PrefixCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            Prefix prefix = new Prefix();
            Add(prefix);
            return prefix;
        }
    }
}
