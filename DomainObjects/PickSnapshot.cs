using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    public class PickIDSnapshotCollection : BindingListEx<PickIDSnapshot>
    {
    }

    public class PickIDSnapshot
    {
        private string _isinternalpick;
        private string _istemporary;
        private string _id;
        private int _version;
        private DateTime _time;
        private string _name;
        private string _comment;
        private int _executor_Code;
        private string _isInversedData;
        private string _isExternalPrefix;
        private string _isInversedPrefix;
        private string _isFrequencyData;
        private string _isHexFormat;
        private string _pickFromID;
        private string _projectName;
        private int _rid;

        private PrefixCollection _prefixList = new PrefixCollection();
        private LogCollection _logList = new LogCollection();
        private IDFunctionCollection _functionList = new IDFunctionCollection();
        private DataMapCollection _dataMapList = new DataMapCollection();
        private DeviceCollection _deviceList = new DeviceCollection();

        public DeviceCollection DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }

        public DataMapCollection DataMapList
        {
            get { return _dataMapList; }
            set { _dataMapList = value; }
        }

        public IDFunctionCollection FunctionList
        {
            get { return _functionList; }
            set { _functionList = value; }
        }

        public PrefixCollection PrefixList
        {
            get { return _prefixList; }
            set { _prefixList = value; }
        }

        public LogCollection LogList
        {
            get { return _logList; }
            set { _logList = value; }
        }

        public string IsInternalPick
        {
            get { return _isinternalpick; }
            set
            {
                _isinternalpick = value;
                //OnPropertyChanged("IsInternalPick");
            }
        }

        public string IsTemporary
        {
            get { return _istemporary; }
            set
            {
                _istemporary = value;
                //OnPropertyChanged("IsTemporary");
            }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public int Executor_Code
        {
            get { return _executor_Code; }
            set { _executor_Code = value; }
        }

        public string IsInversedData
        {
            get { return _isInversedData; }
            set { _isInversedData = value; }
        }
    
        public string IsExternalPrefix
        {
            get { return _isExternalPrefix; }
            set { _isExternalPrefix = value; }
        }

        public string IsFrequencyData
        {
            get { return _isFrequencyData; }
            set { _isFrequencyData = value; }
        }

        public string IsInversedPrefix
        {
            get { return _isInversedPrefix; }
            set { _isInversedPrefix = value; }
        }

        public string IsHexFormat
        {
            get { return _isHexFormat; }
            set { _isHexFormat = value; }
        }

        public string PickFromID
        {
            get { return _pickFromID; }
            set { _pickFromID = value; }
        }

        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }

        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public static PickID AdaptToPickID(PickIDSnapshot snapshot)
        {
            PickID result = new PickID();
            result.Header = new PickIDHeader();
            result.OID = new ID();
            result.OID.Header = new IDHeader();

            result.Header.ID = snapshot.ID;
            result.Header.Version = snapshot.Version;
            result.Header.Name = snapshot.Name;
            result.OID.Header.Executor_Code = snapshot.Executor_Code;
            result.OID.Header.IsInversedData = snapshot.IsInversedData;
            result.OID.Header.IsExternalPrefix = snapshot.IsExternalPrefix;
            result.OID.Header.IsFrequencyData = snapshot.IsFrequencyData;
            result.OID.Header.IsInversedPrefix = snapshot.IsInversedPrefix;
            result.OID.Header.IsHexFormat = snapshot.IsHexFormat;
            result.OID.Header.ID = snapshot.PickFromID;
            result.OID.Header.PrefixList = snapshot.PrefixList.Clone();
            result.DataMapList = snapshot.DataMapList.Clone();
            result.OID.Header.LogList = snapshot.LogList.Clone();
            result.OID.DeviceList = snapshot.DeviceList.Clone();
            result.OID.FunctionList = snapshot.FunctionList.Clone();            

            return result;
        }

        public static PickIDSnapshot Adapt(PickID pickID, bool tempPick)
        {
            PickIDSnapshot result = new PickIDSnapshot();

            result.ID = pickID.Header.ID;
            result.Version = pickID.Header.Version;
            result.Time = DateTime.Now;
            result.Name = pickID.Header.Name;
            result.Comment = pickID.Header.Comment;
            result.Executor_Code = pickID.OID.Header.Executor_Code;
            result.IsInversedData = pickID.OID.Header.IsInversedData;
            result.IsExternalPrefix = pickID.OID.Header.IsExternalPrefix;
            result.IsFrequencyData = pickID.OID.Header.IsFrequencyData;
            result.IsInversedPrefix = pickID.OID.Header.IsInversedPrefix;
            result.IsHexFormat = pickID.OID.Header.IsHexFormat;
            result.PickFromID = pickID.OID.Header.ID;
            result.PrefixList = pickID.OID.Header.PrefixList.Clone();
            result.DataMapList = pickID.DataMapList.Clone();
            result.LogList = pickID.OID.Header.LogList.Clone();
            result.DeviceList = pickID.OID.DeviceList.Clone();
            result.FunctionList = pickID.OID.FunctionList.Clone();
            result.IsTemporary = AdaptYN(tempPick);
            result.IsInternalPick = "N";
            result.ProjectName = pickID.ProjectName;

            return result;
        }

        private static string AdaptYN(bool b)
        {
            if (b)
                return "Y";
            else
                return "N";
        }
    }
}
