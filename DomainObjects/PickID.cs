///
/// IList will be replaced with grid-bindable typed collection 
///
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UEI;

///
/// PickID wilk
///
namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPickService
    {
        void Pick();
        IDFunctionCollection GetNotPicked();
    }

    /// <summary>
    /// Product Specific View of ID
    /// 
    /// inversed prefix will be taken care of (no flag)
    /// labelList can be obtained from ID object
    /// 
    /// </summary>
    [Serializable]
    public class PickID : IPickService
    {
        const string DEFAULT_NOPDATA = "00000000";

        #region Private Fields
        string _projectName;
        string _nopData;
        ID _oID = new ID();
        DataMapCollection _dataMapList = new DataMapCollection();
        PickIDHeader header;

        #endregion

        #region Business Properties and Methods
        public string ProjectName
        {
            get { return _projectName; }
            set { _projectName = value; }
        }

        public string NopData
        {
            get { return _nopData; }
            set 
            { 
                _nopData = value;
                if (String.IsNullOrEmpty(_nopData) == true)
                    _nopData = DEFAULT_NOPDATA;
            }
        }

        public ID OID
        {
            get { return _oID; }
            set { _oID = value; }
        }

        public DataMapCollection DataMapList
        {
            get { return _dataMapList; }
            set { _dataMapList = value; }
        }

        public PickIDHeader Header
        {
            get { return header; }
            set { header = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        void IPickService.Pick()
        {
            string intronPicked = String.Empty;
            string intronPriority = String.Empty;

            if ( OID.Header.IsFrequencyData == BooleanFlag.YES)
                OID.FrequencyDataToBinary();

            int order = 0;
            bool[] dataFound = new bool[DataMapList.Count];

            for (int i = 0; i < dataFound.Length; i++)
                dataFound[i] = false;

            while (true)
            {
                int index = 0;
                bool pass = true;

                foreach (DataMap dataMap in DataMapList)
                {
                    if (dataFound[index++])
                        continue;

                    if (dataMap.IntronList == null)
                    {
                        dataMap.Data = String.Empty;
                        dataFound[index - 1] = true;
                        continue;
                    }

                    if (order >= dataMap.IntronList.Count)
                    {
                        dataMap.Data = String.Empty;
                        dataFound[index - 1] = true;
                        continue;
                    }

                    pass = false;

                    dataMap.Data = OID.SimplePick(dataMap.IntronList,
                        out intronPicked, out intronPriority, order);
                    if (dataMap.Data != String.Empty)
                    {
                        dataMap.Intron = intronPicked;
                        dataMap.IntronPriority = intronPriority;
                        dataFound[index - 1] = true;
                    }
                }

                order++;

                if (pass)
                    break;
            }

            EnforceGroup();

            string data;

            foreach (DataMap dataMap in DataMapList)
            {
                if (dataMap.Data != String.Empty)
                {
                    if (OID.Header.IsInversedData == BooleanFlag.YES)
                        data = CommonFunctions.InvertBinaryString(dataMap.Data);
                    else data = dataMap.Data;

                    dataMap.LabelList = OID.GetLabelByData(dataMap.Data);
                    dataMap.Synth = Synthesizer.getCode(data);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IDFunctionCollection IPickService.GetNotPicked()
        {
            IDFunctionCollection list = new IDFunctionCollection();
            foreach (IDFunction function in OID.FunctionList)
            {
                if (DataMapList.IsDataPicked(function.Data) == false)
                    list.Add(function);
            }

            return list;
        }

        /// <summary>
        /// grouped key will be picked as all or nothing manner
        /// fill with previous data
        /// </summary>
        void EnforceGroup()
        {
            DataMapList.EnforceGroup(NopData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void ToXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            using (Stream s = File.Create(fileName))
            {
                x.Serialize(s, this);
                s.Flush();
                s.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public void LoadFromXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            Stream s = File.OpenRead(fileName);
            PickID pickID = (PickID)x.Deserialize(s);
            s.Close();

            this.NopData = pickID.NopData;
            this.OID = pickID.OID;
            this.DataMapList = pickID.DataMapList;
        }
    }
}

