using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace BusinessObject
{
    [Serializable]
    public class HashtableCollection : BindingListEx<Hashtable>
    {
        protected override object AddNewCore()
        {
            Hashtable table = new Hashtable();
            Add(table);
            return table;
        }

        /// <summary>
        /// hard coded xml serialization
        /// 
        /// TODO: change to automated using 
        /// non-hashtable based data strucuture
        /// </summary>
        /// <param name="fileName"></param>
        public void ToXML(string fileName)
        {
            StreamWriter sw = File.CreateText(fileName);
            sw.WriteLine("<?xml version=\"1.0\"?>");

            sw.WriteLine("<HashtableCollection>");
            foreach (Hashtable table in this)
            {
                sw.WriteLine("\t<Hashtable>");
                sw.Write(ToText(table));
                sw.WriteLine("\t</Hashtable>");
            }

            sw.WriteLine("</HashtableCollection>");
            sw.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private string ToText(Hashtable table)
        {
            string node = "";
            string value = "";

            foreach (string key in table.Keys)
            {
                if (table[key] != null)
                    value = table[key].ToString();
                else
                    value = "";

                node += String.Format("\t\t<{0}>{1}</{0}>\r\n",
                    key, value);
            }

            return node;
        }
    }

    [Serializable]
    public class StringCollection : BindingListEx<string>
    {
        protected override object AddNewCore()
        {
            Add("");
            return "";
        }
    }

    [Serializable]
    public class IntegerCollection : BindingListEx<int>
    {
        protected override object AddNewCore()
        {
            Add(0);
            return 0;
        }

        #region Comparison
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            IntegerCollection list = (IntegerCollection)obj;
            if (Count != list.Count)
                return false;

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i] != list[i])
                    return false;
            }

            return true;
        }
        #endregion
    }

    /// <summary>
    /// for combobox
    /// </summary>
    public class ComboBoxItem
    {
        string _display;
        string _value;

        public string Display
        {
            get { return _display; }
            set { _display = value; }
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class ComboBoxItemCollection : BindingListEx<ComboBoxItem>
    {
        protected override object AddNewCore()
        {
            ComboBoxItem item = new ComboBoxItem();
            Add(item);
            return 0;
        }
    }
}
