using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    [Serializable()]
    public class IDSearchParameter
    {
        private String m_SearchFlag = "0";
        /// <summary>
        ///<remarks>
        /// -Used for ModelInfoReport generation from 27July2011.
        /// </remarks>
        /// </summary>
        public String SearchFlag
        {
            get { return m_SearchFlag; }
            set { m_SearchFlag = value; }
        }
        private String m_startDate = String.Empty;
        public String StartDate
        {
            get { return m_startDate; }
            set { m_startDate = value; }
        }
        private String m_endDate = String.Empty;
        public String EndDate
        {
            get { return m_endDate; }
            set { m_endDate = value; }
        }
        private String ids = String.Empty;
        public String Ids
        {
            get { return ids; }
            set { ids = value; }
        }
        private String deviceAliasRegion = String.Empty;
        public String DeviceAliasRegion
        {
            get { return deviceAliasRegion; }
            set { deviceAliasRegion = value; }
        }
        private String xmlLocations = String.Empty;
        public String XMLLocations
        {
            get { return xmlLocations; }
            set { xmlLocations = value; }
        }
        private String xmlDevices = String.Empty;
        public String XMLDevices
        {
            get { return xmlDevices; }
            set { xmlDevices = value; }
        }
        private int emptyDeviceTypeFilter = 0;
        public int EmptyDeviceTypeFilter
        {
            get { return emptyDeviceTypeFilter; }
            set { emptyDeviceTypeFilter = value; }
        }
        private String regions = String.Empty;
        public String Regions{
            get { return regions; }
            set { regions = value; }
        }
        private String subregions = String.Empty;
        public String SubRegions
        {
            get { return subregions; }
            set { subregions = value; }
        }
        private String countries = String.Empty;
        public String Countries{
            get{return countries;}
            set{countries=value;}
        }
        private Int32 regionType = 0;
        public Int32 RegionType
        {
            get { return regionType; }
            set { regionType = value; }
        }
        private Int32 idModeFilter = 0;
        public Int32 IDModeFilter
        {
            get { return idModeFilter; }
            set { idModeFilter = value; }
        }
        private String devicetypes = String.Empty;
        public String DeviceTypes{
            get { return devicetypes; }
            set { devicetypes = value; }
        }
        private String subdevicetypes = String.Empty;
        public String SubDeviceTypes
        {
            get { return subdevicetypes; }
            set { subdevicetypes = value; }
        }
        private String devicetypeComponents = String.Empty;
        public String DeviceComponents
        {
            get { return devicetypeComponents; }
            set { devicetypeComponents = value; }
        }
        private String dataSources = String.Empty;
        public String DataSources{
            get { return dataSources; }
            set { dataSources = value; }
        }
        private Int32 includeEmptyDeviceType = 0;
        public Int32 IncludeEmptyDeviceType
        {
            get { return includeEmptyDeviceType; }
            set { includeEmptyDeviceType = value; }
        }
        private Int32 removeidsforSelectedDeviceTypes = 0;
        public Int32 RemoveidsforSelectedDeviceTypes
        {
            get { return removeidsforSelectedDeviceTypes; }
            set { removeidsforSelectedDeviceTypes = value; }
        }
        private Int32 includeAliasBrand = 0;
        public Int32 IncludeAliasBrand
        {
            get { return includeAliasBrand; }
            set { includeAliasBrand = value; }
        }
        private Int32 includeUknownLocation = 0;
        public Int32 IncludeUnknownLocation
        {
            get { return includeUknownLocation; }
            set { includeUknownLocation = value; }
        }
        private String _IncludeRestrictedIDs;
        public String IncludeRestrictedIDs
        {
            get { return _IncludeRestrictedIDs.ToUpper(); }
            set { _IncludeRestrictedIDs = value.ToUpper(); }
        }
        private bool _displayMajorBrandsOnly;
        public bool DisplayMajorBrandsOnly
        {
            get { return _displayMajorBrandsOnly; }
            set { _displayMajorBrandsOnly = value; }
        }
        private int _ModelType;
        /// <summary>
        ///Gets or sets flag to denote type of remote.0 for all,
        /// 1 for target model and 2 for remote model.
        /// <remarks>
        /// IDSelection Report.
        /// </remarks>
        /// </summary>
        public int ModelType
        {
            get { return _ModelType; }
            set { _ModelType = value; }
        }
	
        #region GetModeIDList paramsection
        private String _FirstIDOrMode;
        public String FirstIDOrMode
        {
            get { return _FirstIDOrMode; }
            set { _FirstIDOrMode = value; }
        }
        private String _LastIDOrMode;
        public String LastIDOrMode
        {
            get { return _LastIDOrMode; }
            set { _LastIDOrMode = value; }
        }        
        #endregion

        #region ModelInfoReportSpecificParams

        private int _IncludeRecordWithoutBrandNumber;
        public int IncludeRecordWithoutBrandNumber
        {
            get { return _IncludeRecordWithoutBrandNumber; }
            set { _IncludeRecordWithoutBrandNumber = value; }
        }
        private int _ExcludeSelectedBrands;
        public int ExcludeSelectedBrands
        {
            get { return _ExcludeSelectedBrands; }
            set { _ExcludeSelectedBrands = value; }
        }
        private string _Brands;
        public string Brands
        {
            get { return _Brands; }
            set { _Brands = value; }
        }

        private int _IncludeRecordWithoutModelName;
        public int IncudeRecordWithoutModelName
        {
            get { return _IncludeRecordWithoutModelName; }
            set { _IncludeRecordWithoutModelName = value; }
        }

        private int _RemoveNonCodeBookModels;
        public int RemoveNonCodeBookModels
        {
            get { return _RemoveNonCodeBookModels; }
            set { _RemoveNonCodeBookModels = value; }
        }
        private int _ExcludeSelectedModels;
        public int ExcludeSelectedModels
        {
            get { return _ExcludeSelectedModels; }
            set { _ExcludeSelectedModels = value; }
        }
        private string _Models;
        public string Models
        {
            get { return _Models; }
            set { _Models = value; }
        }

        private bool _DuplicatedOutputs;
        public bool DuplicatedOutputs
        {
            get { return _DuplicatedOutputs; }
            set { _DuplicatedOutputs = value; }
        }

        private int _TNLink = 0;
        public int TNLink
        {
            get { return this._TNLink; }
            set { this._TNLink = value; }
        }	
        #endregion
        #region EmptyNonEmptyIDParams
        private int returnEmptyID;
        public int ReturnEmptyID
        {
            get { return returnEmptyID; }
            set { returnEmptyID = value; }
        }
        #endregion
        #region LabelAndIntronSearch
        private int _CompleteMatchFlag = 0;

        public int CompleteLabelIntronMatch
        {
            get { return _CompleteMatchFlag; }
            set { _CompleteMatchFlag = value; }
        }


        private int _LabelIntronSearchFlag = 0;
        public int LabelIntronSearchFlag
        {
            get { return _LabelIntronSearchFlag; }
            set { _LabelIntronSearchFlag = value; }
        }
        private string _LabelOrIntronPattern;
        public string LabelOrIntronPattern
        {
            get { return _LabelOrIntronPattern; }
            set { _LabelOrIntronPattern = value; }
        }
        #endregion
    }
}
