using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Security.Principal;

namespace BusinessObject
{
    public class Log
    {
        #region Private Fields

        int _rid;
        int _tn;
        string _id;
        DateTime _time;
        string _operator;
        string _content;

        #endregion

        #region Business Properties and Methods

        [XmlIgnore]
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        //public DateTime Time
        //{
        //    get { return _time.ToUniversalTime(); }
        //    set { _time = value.ToLocalTime(); }
        //}

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        #endregion

        public static string DATETIME_FORMAT = "yyyy-MM-ddTHH:mm:ss";

        public static Log Create(string id, string content)
        {
            Log log = new Log();
            log.ID = id;
            log.Time = GetCurrentDateTime();
            log.Operator = GetCurrentUserName();
            log.Content = content;

            return log;
        }

        public static Log Create(int tn, string content)
        {
            Log log = new Log();
            log.TN = tn;
            log.Time = GetCurrentDateTime();
            log.Operator = GetCurrentUserName();
            log.Content = content;

            return log;
        }

        #region Private
        private static DateTime GetCurrentDateTime()
        {
            string now = DateTime.Now.ToString(DATETIME_FORMAT);
            return DateTime.Parse(now);
        }

        private static string GetCurrentUserName()
        {
            string name = WindowsIdentity.GetCurrent().Name;
            if (name.Contains("\\"))
                name = name.Substring(name.LastIndexOf("\\") + 1);

            return name;
        }
        #endregion
    }

    public class LogCollection : BindingListEx<Log>
    {
        protected override object AddNewCore()
        {
            Log log = new Log();
            Add(log);
            return log;
        }

        public LogCollection Clone()
        {
            LogCollection clone = new LogCollection();

            foreach (Log log in this)
            {
                clone.Add(log);
            }
            return clone;
        }
    }
}
