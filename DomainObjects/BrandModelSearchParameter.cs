using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class BrandModelSearchParameter
    {
        private string _brand;
        private string _model;
        private int? _deviceTypeCode;
        private StringCollection _regionList;
        private StringCollection _countryList;
        private string _type;
        private string _branch;

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        public int? DeviceTypeCode
        {
            get { return _deviceTypeCode; }
            set { _deviceTypeCode = value; }
        }
        public StringCollection RegionList
        {
            get { return _regionList; }
            set { _regionList = value; }
        }
        public StringCollection CountryList
        {
            get { return _countryList; }
            set { _countryList = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }
        public string EmptyMarketFlag
        {
            get 
            {
                if (RegionList.Count == 0 && CountryList.Count == 0)
                    return "Y";
                else
                    return "N";
            }
        }
    }
}
