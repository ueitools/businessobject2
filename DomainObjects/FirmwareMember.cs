using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 0
    /// 00000001, 00000010, and so on
    /// </summary>
    [Serializable]
    public class DigitGroup
    {
        #region Private Fields
        int _project_Code;
        int _groupNumber;
        StringCollection _dataList = new StringCollection();
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_Code; }
            set { _project_Code = value; }
        }

        public int GroupNumber
        {
            get { return _groupNumber; }
            set { _groupNumber = value; }
        }

        public StringCollection DataList
        {
            get { return _dataList; }
            set { _dataList = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            DigitGroup group = (DigitGroup)obj;
            if (this.GroupNumber != group.GroupNumber)
                return false;
            if (!Object.Equals(this.DataList, group.DataList))
                return false;

            return true;
        }
        #endregion
    }

    [Serializable]
    public class DigitGroupCollection : BindingListEx<DigitGroup>
    {
        protected override object AddNewCore()
        {
            DigitGroup digitGroup = new DigitGroup();
            Add(digitGroup);
            return digitGroup;
        }

        public void SyncProjectCode(int code)
        {
            foreach (DigitGroup group in this)
                group.Project_Code = code;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class IDLoad
    {
        #region Private Fields
        int _project_Code;
        string _id;
        string _doUpdate = BooleanFlag.NO;
        string _fromID;
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_Code; }
            set { _project_Code = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string DoUpdate
        {
            get { return _doUpdate; }
            set { _doUpdate = value; }
        }

        public string FromID
        {
            get { return _fromID; }
            set { _fromID = value; }
        }

        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            IDLoad idLoad = (IDLoad)obj;
            if (this.ID != idLoad.ID)
                return false;
            if (this.DoUpdate != idLoad.DoUpdate)
                return false;

            if (this.FromID != idLoad.FromID)
                return false;

            return true;
        }
        #endregion
    }

    [Serializable]
    public class IDLoadCollection : BindingListEx<IDLoad>
    {
        protected override object AddNewCore()
        {
            IDLoad idLoad = new IDLoad();
            Add(idLoad);
            return idLoad;
        }

        public void SyncProjectCode(int code)
        {
            foreach (IDLoad idLoad in this)
                idLoad.Project_Code = code;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ExecutorLoad
    {
        #region Private Fields
        int _project_Code;
        int _executor_code;
        string _doUpdate = BooleanFlag.NO;
        #endregion

        #region Business Properties and Methods
        public int Project_Code
        {
            get { return _project_Code; }
            set { _project_Code = value; }
        }

        public int Executor_Code
        {
            get { return _executor_code; }
            set { _executor_code = value; }
        }

        public string DoUpdate
        {
            get { return _doUpdate; }
            set { _doUpdate = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            ExecutorLoad executorLoad = (ExecutorLoad)obj;
            if (this.Executor_Code != executorLoad.Executor_Code)
                return false;
            if (this.DoUpdate != executorLoad.DoUpdate)
                return false;

            return true;
        }
        #endregion
    }

    [Serializable]
    public class ExecutorLoadCollection : BindingListEx<ExecutorLoad>
    {
        protected override object AddNewCore()
        {
            ExecutorLoad executorLoad = new ExecutorLoad();
            Add(executorLoad);
            return executorLoad;
        }

        public void SyncProjectCode(int code)
        {
            foreach (ExecutorLoad executorLoad in this)
                executorLoad.Project_Code = code;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class AlternateID
    {
        #region Private Fields
        int _project_Code;
        string _alternateID;
        string _externalID;
        int _numCriticalOutron;
        int _matchCount;
        private DateTime _creationDate;
        private DateTime _modificationDate;
        #endregion

        #region Business Properties and Methods
        /// <summary>
        /// 
        /// </summary>
        public int Project_Code
        {
            get { return _project_Code; }
            set { _project_Code = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string InternalID
        {
            get { return _alternateID; }
            set { _alternateID = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ExternalID
        {
            get { return _externalID; }
            set { _externalID = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int NumCriticalOutron
        {
            get { return _numCriticalOutron; }
            set { _numCriticalOutron = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int MatchCount
        {
            get { return _matchCount; }
            set { _matchCount = value; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        public DateTime ModificationDate
        {
            get { return _modificationDate; }
            set { _modificationDate = value; }
        }

        #endregion

        #region Operator
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            AlternateID altID= (AlternateID)obj;
            if (this.InternalID != altID.InternalID)
                return false;
            if (this.ExternalID != altID.ExternalID)
                return false;
            if (this.NumCriticalOutron != altID.NumCriticalOutron)
                return false;
            if (this.MatchCount != altID.MatchCount)
                return false;
            return true;
        }

        public override string ToString()
        {
            return "External:" + ExternalID + "," +
                "Internal:" + InternalID + "," +
                "Match Count:" + MatchCount + "," +
                "Num Critical:" + NumCriticalOutron + "," +
                "Project:" + Project_Code;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class AlternateIDCollection : BindingListEx<AlternateID>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            AlternateID altID = new AlternateID();
            Add(altID);
            return altID;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SyncProjectCode(int code)
        {
            foreach (AlternateID altID in this)
                altID.Project_Code = code;
        }

        public bool ContainsExternalID(string id)
        {
            bool found = false;

            foreach (AlternateID alt in this)
            {
                if (alt.ExternalID == id)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }
    }

    [Serializable]
    public class AliasCode
    {
        private int projectCode;
        private string globalCode;
        private string localCode;

        public string GlobalCode
        {
            get { return globalCode; }
            set { globalCode = value; }
        }

        public string LocalCode
        {
            get { return localCode; }
            set { localCode = value; }
        }

        public int ProjectCode
        {
            get { return projectCode; }
            set { projectCode = value; }
        }

        public int FK_ProjectCode
        {
            get { return projectCode; }
            set { projectCode = value; }
        }

        public string FK_Local_Code
        {
            get { return localCode; }
            set { localCode = value; }
        }

        public string Global_Code
        {
            get { return globalCode; }
            set { globalCode = value; }
        }

        public Guid InsertedRID { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class AliasCodeCollection : BindingListEx<AliasCode>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            AliasCode alias = new AliasCode();
            Add(alias);
            return alias;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SyncProjectCode(int code)
        {
            foreach (AliasCode alias in this)
                alias.ProjectCode = code;
        }
    }
}
