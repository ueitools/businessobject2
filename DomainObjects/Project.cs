///
/// IList will be replaced with grid-bindable typed collection 
///
using System;
using System.Collections.Generic;
using System.IO;

///
/// Please don't use classes directly since it will change.
/// Instead create Wrapper Function in turn get data from class.
/// 
/// int GetExecutorSize(string project)
/// {
///     Project project = new Project();
///     return Int32.Parse(project.config["ExecutorSize"]);
/// }
/// 
/// TODO:
/// Location for Building image routine 
/// Domain Modling of
/// 
///     Project
///     Library
///     Image 
///
namespace BusinessObject
{
    /// <summary>
    /// project
    /// config: Encrypt, TwoByteBlasterFileReocrdLength 
    /// </summary>
    [Serializable]
    public class Project
    {
        #region Private Fields
        ProjectHeader _header = new ProjectHeader();
        KeyCollection _keyList = new KeyCollection();
        Firmware _firmware = new Firmware();
        ConfigCollection _configList = new ConfigCollection();
        ChipCollection _chipList = new ChipCollection();
        #endregion

        #region Business Properties and Methods
        public ProjectHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public KeyCollection KeyList
        {
            get { return _keyList; }
            set { _keyList = value; }
        }

        public Firmware Firmware
        {
            get { return _firmware; }
            set { _firmware = value; }
        }

        public ConfigCollection ConfigList
        {
            get { return _configList; }
            set { _configList = value; }
        }

        public ChipCollection ChipList
        {
            get { return _chipList; }
            set { _chipList = value; }
        }
        #endregion 

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            Project project = (Project)obj;
            if (!Object.Equals(this.Header, project.Header))
                return false;
            if (!Object.Equals(this.KeyList, project.KeyList))
                return false;
            if (!Object.Equals(this.Firmware, project.Firmware))
                return false;
            if (!Object.Equals(this.ConfigList, project.ConfigList))
                return false;
            if (!Object.Equals(this.ChipList, project.ChipList))
                return false;
            return true;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public void SyncProjectCode(int code)
        {
            this.Header.Code = code;
            this.KeyList.SyncProjectCode(code);
            this.Firmware.SyncProjectCode(code);
            this.ConfigList.SyncProjectCode(code);
            this.ChipList.SyncProjectCode(code);
        }
    }

    /// <summary>
    /// chipName from ID.INC (SAMSUNGCH)
    /// memoryType from Modes.db (EEPROM)
    /// </summary>
    [Serializable]
    public class ProjectHeader
    {
        public static string E2PROM = "E2PROM";
        public static string FLASH = "FLASH";
        public static string BLASTER = "BLASTER";
        public static string ELCC = "ELCC";
        public static string FDRA_GENII = "FDRA_GENII";
        public static string ZIP_IR = "ZIP_IR";

        #region Private Fields
        int _code;
        string _name;
        string _memoryType;
        int _sectorSize;
        string _manufacturer_name;
        string _manufacturer_code;
        string _description;
        int _executorCode;
        string _doUpdate;
        #endregion

        #region Business Properties and Methods
        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string MemoryType
        {
            get { return _memoryType; }
            set { _memoryType = value; }
        }

        public int SectorSize
        {
            get { return _sectorSize; }
            set { _sectorSize = value; }
        }

        public string Manufacturer_Name
        {
            get { return _manufacturer_name; }
            set { _manufacturer_name = value; }
        }

        public string Manufacturer_Code
        {
            get { return _manufacturer_code; }
            set { _manufacturer_code = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int ExecutorCode
        {
            get { return _executorCode; }
            set { _executorCode = value; }
        }
        public string DoUpdate
        {
            get { return _doUpdate; }
            set { _doUpdate = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            ProjectHeader header = (ProjectHeader)obj;
            if (this.Name != header.Name)
                return false;
            if (this.MemoryType != header.MemoryType)
                return false;
            if (this.SectorSize != header.SectorSize)
                return false;
            if (this.Manufacturer_Name != header.Manufacturer_Name)
                return false;
            if (this.Manufacturer_Code != header.Manufacturer_Code)
                return false;
            if (this.Description != header.Description)
                return false;
            
            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ProjectHeaderCollection : BindingListEx<ProjectHeader>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            ProjectHeader header = new ProjectHeader();
            Add(header);
            return header;
        }
    }

}

