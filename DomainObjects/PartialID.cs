using System;
using System.Xml.Serialization;
using System.IO;
using BusinessObject.LockStatus;
using UEIException;

namespace BusinessObject
{
    public class PartialID
    {
        #region Private Fields

        private IDHeader _header = new IDHeader();
        private FunctionCollection _functionList = new FunctionCollection();
        private DeviceCollection _deviceList = new DeviceCollection();

        #endregion

        #region Business Properties and Methods

        public IDHeader Header
        {
            get { return _header; }
            set { _header = value; }
        }

        public FunctionCollection FunctionList
        {
            get { return _functionList; }
            set { _functionList = value; }
        }

        public DeviceCollection DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        //public void Load(string id)
        //{

        //    System.Text.StringBuilder sBuilder = new System.Text.StringBuilder();
        //    DateTime methodStartTime = DateTime.Now;

        //    sBuilder.AppendLine("---------------------------------------------------");
        //    sBuilder.AppendLine(String.Format("== Load Method({0}) {1} ", id, methodStartTime));
        //    sBuilder.AppendLine(" ");

        //    DateTime startTime = DateTime.Now;
        //    _header = DAOFactory.IDHeader().Select(id);
        //    DateTime endTime = DateTime.Now;
        //    TimeSpan headerLoadSpan = endTime - startTime;
        //    sBuilder.AppendLine(String.Format("===> Select IDHeader :{0}", headerLoadSpan.ToString()));
           
            

        //    startTime = DateTime.Now;
        //    _functionList = DAOFactory.Function().SelectIDOnly(id);
        //    endTime = DateTime.Now;
        //    TimeSpan funcLoadSpan = endTime - startTime;
        //    sBuilder.AppendLine(String.Format("===> Select Function List :{0}", funcLoadSpan.ToString()));
            

        //        //startTime = DateTime.Now;
        //   _deviceList = DAOFactory.Device().SelectIDOnly_PGS(id,sBuilder);
        //        //endTime = DateTime.Now;
        //        //TimeSpan deviceLoadSpan = endTime - startTime;
               
                
        //        //sWriter.WriteLine(String.Format("Device Load:{0}", deviceLoadSpan.ToString()));


        //   startTime = DateTime.Now;
        //    foreach (Function function in _functionList)
        //        function.Preprocess();

        //    endTime = DateTime.Now;
        //    TimeSpan funcPreprocessSpan = endTime - startTime;
        //    sBuilder.AppendLine(String.Format("===> FuncPreprocessSpan :{0}", funcPreprocessSpan.ToString()));

        //    sBuilder.AppendLine(" ");
        //    sBuilder.AppendLine("== Load Method End");
        //    sBuilder.AppendLine(" ");

        //    using (System.IO.StreamWriter sWriter = new System.IO.StreamWriter(@"D:\WorkingDirForPGS\LogsForT0251\IdLoadLog.txt", true))
        //    {
        //        sWriter.Write(sBuilder.ToString());
        //    }
        //}

        public void Load(string id)
        {

            _header = DAOFactory.IDHeader().Select(id);

            _functionList = DAOFactory.Function().SelectIDOnly(id);
            _deviceList = DAOFactory.Device().SelectIDOnly_PGS(id);


            foreach (Function function in _functionList)
                function.Preprocess();

        }
        public bool IsReady()
        {
            return (_header != null && _header.Status == StatusFlag.PASSED_QA);
        }

        private static IdLock GetLockedId(string id) {
            return GetLockedId(id, true);
        }

        private static IdLock GetLockedId(string id, bool useDomainName) {
            string domainName = string.Empty;
            if (useDomainName) {
                domainName = DBFunctions.GetDBDomain();
            }

            LockStatusService lockStatus = new LockStatusService();
            IdLock[] lockedIDs = lockStatus.GetLockedIDs(domainName);
            IdLock lockedID = null;
            foreach (IdLock idLock in lockedIDs) {
                if (idLock.ID.ToUpper() == id.ToUpper()) {
                    lockedID = idLock;
                }
            }

            return lockedID;
        }


        public static bool IsLocked(string id) {
            IdLock lockedID = GetLockedId(id);
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC) {
                if (lockedID == null)
                    throw new Exception(id + " isn't checked out!");

                IDHeader header = DAOFactory.IDHeader().SelectHeaderOnly(id);
                return (header.IsLocked == BooleanFlag.YES);
            }

            return (lockedID != null);
        }

        public static int Lock(string id) {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC) {
                IdLock lockedID = GetLockedId(id);
                if (lockedID == null) {
                    throw new Exception("ID " + id + " isn't checked out!");
                }

                IDHeader header = DAOFactory.IDHeader().SelectHeaderOnly(id) as IDHeader;

                if (header.IsLocked == BooleanFlag.YES) {
                    throw new IDLockException("ID " + id + " is locked");
                }

                header.IsLocked = BooleanFlag.YES;
                DAOFactory.IDHeader().UpdateHeaderOnly(header);

                string userName = Environment.UserName;
                if (lockedID.UserName != userName) {
                    return 1;
                }
            } else {
                LockStatusService lockStatus = new LockStatusService();
                int lockIDStatus = lockStatus.LockId(id, Environment.UserDomainName, Environment.UserName);
                if (lockIDStatus != 0) {
                    throw new IDLockException(string.Format("Error locking {0}.\n\n{1}", id, lockStatus.GetMessage(lockIDStatus)));
                }
            }

            return 0;
        }

        public static void ForceUnLock(string id) {
            IdLock lockedID = GetLockedId(id, false);
            UnLock(id, lockedID.DomainName, "FORCED");
        }

        public static void UnLock(string id) {
            UnLock(id, Environment.UserDomainName, Environment.UserName);
        }

        private static void UnLock(string id, string domainName, string userName)
        {
            if (DAOFactory.GetDBConnectionString() != DBConnectionString.UEIPUBLIC)
            {
                IDHeader header = DAOFactory.IDHeader().SelectHeaderOnly(id) as IDHeader;

                header.IsLocked = BooleanFlag.NO;
                DAOFactory.IDHeader().UpdateHeaderOnly(header);
            }else{
                LockStatusService lockStatus = new LockStatusService();
                int lockIDStatus = lockStatus.UnlockId(id, domainName, userName);
                if (lockIDStatus < 0) {
                    throw new IDLockException(string.Format("Error unlocking {0}.\n\n{1}", id, lockStatus.GetMessage(lockIDStatus)));
                }
            }
        }

        /// <summary>
        /// Erase device and function. Changed header into a shadow
        /// </summary>
        /// <param name="id"></param>
        public static void Erase(string id)
        {
            DAOFactory.Device().DeleteIDOnly(id);
            DAOFactory.Function().DeleteIDOnly(id);
            DAOFactory.IDHeader().MakeUnAvailable(id);
        }

        /// <summary>
        /// Erase device, function and header.  Do not leave a shadow.
        /// </summary>
        /// <param name="id"></param>
        public static void EraseIDAndShadow(string id)
        {
            DAOFactory.Device().DeleteIDOnly(id);
            DAOFactory.Function().DeleteIDOnly(id);
            DAOFactory.Function().DropProxy(id);
            DAOFactory.IDHeader().Delete(id);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert()
        {
            DAOFactory.IDHeader().Insert(_header);
            DAOFactory.Function().Insert(_header.ID, _functionList);
            DAOFactory.Device().Insert(_header.ID, _deviceList);
        }

        public void Update()
        {
            DAOFactory.Device().DeleteIDOnly(_header.ID);
            DAOFactory.Function().DeleteIDOnly(_header.ID);

            DAOFactory.IDHeader().Update(_header);
            DAOFactory.Function().Insert(_header.ID, _functionList);
            DAOFactory.Device().Insert(_header.ID, _deviceList);
        }

        public void Save()
        {
            if (DAOFactory.IDHeader().DoesExistNoFilter(_header.ID) == true)
                Update();
            else
                Insert();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newID">must not exist</param>
        /// <param name="baseID">must exist</param>
        /// <returns></returns>
        public void Initialize(string newID, string baseID, string exec)
        {
            if (baseID != "")
            {
                _header = DAOFactory.IDHeader().Select(baseID);
                _header.LogList.Clear();
                _header.Status = StatusFlag.MODIFIED;
            }

            _header.ID = newID;
            _header.Executor_Code = Int32.Parse(exec);
            _header.IsLocked = BooleanFlag.NO;
        }

        public void Initialize(string newID, IDHeader baseID, string exec)
        {
            if (baseID != null)
            {
                _header = baseID;
                _header.LogList.Clear();
                _header.Status = StatusFlag.MODIFIED;
            }

            _header.ID = newID;
            _header.Executor_Code = Int32.Parse(exec);
            _header.IsLocked = BooleanFlag.NO;
        }

        /// <summary>
        /// XML Serialization Routines
        /// </summary>
        /// <param name="fileName"></param>
        public void ToXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            Stream s = File.Create(fileName);
            x.Serialize(s, this);
            s.Close();
        }

        public void LoadFromXML(string fileName)
        {
            XmlSerializer x = new XmlSerializer(this.GetType());
            Stream s = File.OpenRead(fileName);
            PartialID oID = (PartialID)x.Deserialize(s);
            s.Close();

            this.Header = oID.Header;
            this.FunctionList = oID.FunctionList;
            this.DeviceList = oID.DeviceList;
        }
    }
}
