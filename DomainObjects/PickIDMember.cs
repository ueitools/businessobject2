using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    public class ModePick
    {
        #region Private Fields
        string _mode;
        DataMapCollection _dataMapList = new DataMapCollection();
        #endregion

        #region Business Properties and Methods
        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        public DataMapCollection DataMapList
        {
            get { return _dataMapList; }
            set { _dataMapList = value; }
        }
        #endregion
    }

    public class ModePickCollection : BindingListEx<ModePick>
    {
        protected override object AddNewCore()
        {
            ModePick modePick = new ModePick();
            Add(modePick);
            return modePick;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public ModePick Find(string mode)
        {
            foreach (ModePick pick in this)
            {
                if (pick.Mode.Contains(mode) == true)
                    return pick;
            }

            return null;
        }
    }

    /// <summary>
    /// Mapping Key (Emain02) to Label (Sony)
    /// outronDescription (key label) -- outron -- data -- label (intron)
    /// 
    /// currently outron description will replace actual key description
    /// outron description usually handles multiple names 
    /// (sometimes key position)
    /// 
    /// </summary>
    /// 
    [Serializable]
    public class DataMap
    {
        #region Private Fields
        //// input
        string _synth;
        string _outron;  // X01, X02 for not assigned data
        string _keyLabel;
        int _outronGroup;
        StringCollection _intronList = new StringCollection();

        //// output
        string _data;
        string _intron;
        string _intronPriority;
        StringCollection _labelList = new StringCollection();

        int _position;

        private int _rid;

        #endregion

        #region Business Properties and Methods
        public string Synth
        {
            get { return _synth; }
            set { _synth = value; }
        }

        public string Outron
        {
            get { return _outron; }
            set { _outron = value; }
        }

        public string KeyLabel
        {
            get { return _keyLabel; }
            set { _keyLabel = value; }
        }

        public int OutronGroup
        {
            get { return _outronGroup; }
            set { _outronGroup = value; }
        }

        public StringCollection IntronList
        {
            get { return _intronList; }
            set { _intronList = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Intron
        {
            get { return _intron; }
            set { _intron = value; }
        }

        public string IntronPriority
        {
            get { return _intronPriority; }
            set { _intronPriority = value; }
        }
        
        public StringCollection LabelList
        {
            get { return _labelList; }
            set { _labelList = value; }
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        [XmlIgnore]
        public int IntronPriorityInt
        {
            get { return int.Parse(IntronPriority); }
            set { IntronPriority = value.ToString(); }
        }

        [XmlIgnore]
        public int RID
        {
            get { return _rid; }
            set { _rid = value; }
        }

        #endregion
    }
    
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DataMapCollection : BindingListEx<DataMap>
    {
        public DataMapCollection Clone()
        {
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            f.Serialize(s, this);
            s.Position = 0;
            object clone = f.Deserialize(s);
            return clone as DataMapCollection;
        }

        protected override object AddNewCore()
        {
            DataMap dataMap = new DataMap();
            Add(dataMap);
            return dataMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool IsDataPicked(string data)
        {
            foreach (DataMap map in this)
            {
                if (map.Data == data)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nopData"></param>
        public void EnforceGroup(string nopData)
        {
            foreach (DataMap map in this)
            {
                if (String.IsNullOrEmpty(map.Data) == true &&
                    map.OutronGroup != -1 &&
                    IsGroupPicked(map.OutronGroup) == true)
                {
                    map.Data = nopData;
                }
            }
        }

        bool IsGroupPicked(int number)
        {
            foreach (DataMap map in this)
            {
                if (map.OutronGroup == number &&
                    String.IsNullOrEmpty(map.Data) == false)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyLabel"></param>
        /// <returns></returns>
        public DataMapCollection Find(string keyLabel)
        {
            DataMapCollection dataMapList = new DataMapCollection();
            foreach (DataMap map in this)
            {
                if (map.KeyLabel == keyLabel)
                    dataMapList.Add(map);
            }

            return dataMapList;
        }
    }

    [Serializable]
    public class PickIDHeader : INotifyPropertyChanged
    {
        #region Private Fields
        //// input
        private int version;
        private string time;
        private string name;
        private string comment;
        private string id;
        //// output

        #endregion

        #region Business Properties and Methods

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(info));
            }
        }

        public int Version
        {
            get { return version; }
            set { version = value; }
        }

        public string Time
        {
            get { return time; }
            set { time = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        
        #endregion
    }   
}
