using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// 
    /// </summary>
    public class Product
    {
        #region Private Fields
        int _urc;
        string _revision;
        int _chip_code;
        string _description;
        string _chip_number;
        #endregion

        #region Business Properties and Methods
        public int URC
        {
            get { return _urc; }
            set { _urc = value; }
        }

        public string Revision
        {
            get { return _revision; }
            set { _revision = value; }
        }

        public int Chip_Code
        {
            get { return _chip_code; }
            set { _chip_code = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Chip_Number
        {
            get { return _chip_number; }
            set { _chip_number = value; }
        }
        #endregion

        #region Operator
        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;
            if (this.GetType() != obj.GetType())
                return false;

            Product product = (Product)obj;
            if (this.URC != product.URC)
                return false;
            if (this.Revision != product.Revision)
                return false;
            if (this.Chip_Code != product.Chip_Code)
                return false;
            if (this.Description != product.Description)
                return false;
            if (this.Chip_Number != product.Chip_Number)
                return false;
            return true;
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ProductCollection : BindingListEx<Product>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override object AddNewCore()
        {
            Product product = new Product();
            Add(product);
            return product;
        }
    }
}
