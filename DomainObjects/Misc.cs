using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable()]
    public class Brand
    {
        int _code;
        int _standard_code;
        string _name;

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public int Standard_Code
        {
            get { return _standard_code; }
            set { _standard_code = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }

    /// <summary>
    /// default IComparable interface to be implememted
    /// </summary>
    [Serializable()]
    public class Country : IComparable<Country>
    {
        #region Private Fields
        int _dr_rid;
        int _code;
        private string _region = String.Empty;
        private String _subregion = String.Empty;
        private string _name = String.Empty;
        private String _alternateregion = String.Empty;        
        #endregion

        #region Business Properties and Methods
        [XmlIgnore]
        public int DR_RID
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public String Region
        {
            get { return _region; }
            set { _region = value; }
        }        
        public String SubRegion
        {
            get { return _subregion; }
            set { _subregion = value; }
        }
        public String AlternateRegion
        {
            get { return _alternateregion; }
            set { _alternateregion = value; }
        }
        
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion

        public int CompareTo(Country rhs)
        {
            int res = this.Region.CompareTo(rhs.Region);
            if (res != 0)
                return res;

            return this.Name.CompareTo(rhs.Name);
        }
    }

    [Serializable()]
    public class OpInfo
    {
        int _dr_rid;
        int _code;
        private string _operation;
        private string _information;

        [XmlIgnore]
        public int DR_RID
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Operation
        {
            get { return _operation; }
            set { _operation = value; }
        }

        public string Information
        {
            get { return _information; }
            set { _information = value; }
        }
    }

    [Serializable()]
    public class DeviceType
    {
        int _dr_rid;
        int _code;
        private string _name;
        private String _maindevicetype = String.Empty;
        private Int32 _devicetypeID = 0;
        private String _devicetype = String.Empty;
        private Int32 _subdevicetypeID = 0;
        private String _subdevicetype = String.Empty;
        private Int32 _componentID = 0;
        private String _component = String.Empty;
        private String _mode = String.Empty;
        private string _ComponentStatus = String.Empty;
        [XmlIgnore]
        public int DR_RID
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }
        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string MainDevice
        {
            get { return _maindevicetype; }
            set { _maindevicetype = value; }
        }
        public String Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
        public Int32 DeviceID
        {
            get { return _devicetypeID; }
            set { _devicetypeID = value; }
        }
        public String Device
        {
            get { return _devicetype; }
            set { _devicetype = value; }
        }
        public Int32 SubDeviceID
        {
            get { return _subdevicetypeID; }
            set { _subdevicetypeID = value; }
        }
        public String SubDevice
        {
            get { return _subdevicetype; }
            set { _subdevicetype = value; }
        }
        public Int32 ComponentID
        {
            get { return _componentID; }
            set { _componentID = value; }
        }
        public String Component
        {
            get { return _component; }
            set { _component = value; }
        }
       

        public string ComponentStatus
        {
            get { return _ComponentStatus; }
            set { _ComponentStatus = value; }
        }
	
    }

    [Serializable()]
    public class Labels
    {
        private string _keylabel;
        private long  _RID;

        public Labels()
        {
            _keylabel = string.Empty;
            _RID = 0;
        }
        public string Keylabel
        {
            get { return _keylabel; }
            set { _keylabel = value; }
        }
        [XmlIgnore]
        public long RID
        {
            get { return _RID; }
            set { _RID = value; }
        }

       
    }

  

    public class LabelCollection : BindingListEx<Labels>
    {
        protected override object AddNewCore()
        {
            Labels labels = new Labels();
            Add(labels);
            return labels;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class BrandCollection : BindingListEx<Brand>
    {
        protected override object AddNewCore()
        {
            Brand brand = new Brand();
            Add(brand);
            return brand;
        }
    }

    public class CountryCollection : BindingListEx<Country>
    {

        protected override object AddNewCore()
        {
            Country country = new Country();
            Add(country);
            return country;
        }
    }

    public class OpInfoCollection : BindingListEx<OpInfo>
    {
        protected override object AddNewCore()
        {
            OpInfo opInfo = new OpInfo();
            Add(opInfo);
            return opInfo;
        }
    }

    public class DeviceTypeCollection : BindingListEx<DeviceType>
    {
        protected override object AddNewCore()
        {
            DeviceType deviceType = new DeviceType();
            Add(deviceType);
            return deviceType;
        }
    }
    
    #region [DBMIntegrations - 11/18/2010]

    /// <summary>
    /// Holds ID, Brand, Brand Number, Device Type, Model Count
    /// Contains ID,Brand Name and Model Count.
    ///<remarks></remarks>
    /// </summary>
    [Serializable()]
    public class IDBrandResult
    {
        public IDBrandResult()
        {
        }
        private String _DeviceTypeFlag = String.Empty;
        public String DeviceTypeFlag
        {
            get { return _DeviceTypeFlag; }
            set { _DeviceTypeFlag = value; }
        }
        private String mode = String.Empty;
        public string Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        private String _ID = String.Empty;
        public String ID
        {
            get { return _ID; }
            set { _ID = value; }
        }
        private String _Brand = String.Empty;
        public String Brand
        {
            get { return _Brand; }
            set { _Brand = value; }
        }
        private string _AliasBrand;

        public string AliasBrand
        {
            get { return _AliasBrand; }
            set { _AliasBrand = value; }
        }
        private string _AliasType;

        public string AliasType
        {
            get { return _AliasType; }
            set { _AliasType = value; }
        }

        private String _Model = String.Empty;
        public String Model
        {
            get { return _Model; }
            set { _Model = value; }
        }
        private String _IsTargetModel = String.Empty;
        public String TRModel
        {
            get { return _IsTargetModel; }
            set { _IsTargetModel = value; }
        }
        private String _TNLink = String.Empty;
        public String TN
        {
            get { return _TNLink; }
            set { _TNLink = value; }
        }
        private Int32 brandNumber = 0;
        public Int32 BrandNumber
        {
            get { return brandNumber; }
            set { brandNumber = value; }
        }
        private String deviceType = String.Empty;
        /// <summary>
        /// Obsolete. Not used in unification version
        /// </summary>
        public String DeviceType
        {
            get { return deviceType; }
            set { deviceType = value; }
        }
        private String _MainDevice = String.Empty;
        public String MainDevice
        {
            get { return _MainDevice; }
            set { _MainDevice = value; }
        }
        private String _SubDevice = String.Empty;
        public String SubDevice
        {
            get { return _SubDevice; }
            set { _SubDevice = value; }
        }
        private String _Component  = String.Empty;
        public String Component
        {
            get { return _Component; }
            set { _Component = value; }
        }
        private int _ComponentStatus;
        public int ComponentStatus
        {
            get { return _ComponentStatus; }
            set { _ComponentStatus = value; }
        }
        private String _RegionalName = String.Empty;
        public String RegionalName
        {
            get { return _RegionalName; }
            set { _RegionalName = value; }
        }
        private Int32 _ModelCount = 0;
        public Int32 ModelCount
        {
            get { return _ModelCount; }
            set { _ModelCount = value; }
        }
        private Int32 _Count = 0;
        public Int32 Count
        {
            get { return _Count; }
            set { _Count = value; }
        }
        private String _DataSource;
        public String DataSource
        {
            get { return _DataSource; }
            set { _DataSource = value; }
        }
        private String country;
        public String Country
        {
            get { return country; }
            set { country = value; }
        }
        private String _SubRegion;

        public String SubRegion
        {
            get { return _SubRegion; }
            set { _SubRegion = value; }
        }

        private String region;
        public String Region
        {
            get { return region; }
            set { region = value; }
        }
        private String comment = String.Empty;
        public String Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        private Int32 m_Availability = 0;
        public Int32 Availability
        {
            get { return m_Availability; }
            set { m_Availability = value; }
        }

        private String _TrustLevel;

        public String TrustLevel
        {
            get { return _TrustLevel; }
            set { _TrustLevel = value; }
        }
        private String _LowPriorityID;
        public String LowPriorityID
        {
            get { return _LowPriorityID; }
            set { _LowPriorityID = value; }
        }

    }   

    /// <summary>
    /// Collection of IDBrandResult object
    /// <remarks>created on 9/15/2010 by binil for DBM.</remarks>
    /// </summary>
    public class IDBrandResultCollection : BindingListEx<IDBrandResult>
    {
        protected override object AddNewCore()
        {
            IDBrandResult idBrandResult = new IDBrandResult();
            Add(idBrandResult);
            return idBrandResult;
        }
        /// <summary>
        /// Returns the list of distinct IDs from the collection
        /// </summary>
        /// <returns></returns>
        public List<string> GetIDList()
        {
            List<string> idList = new List<string>();
            foreach (IDBrandResult result in this.Items)
            {
                if (!idList.Contains(result.ID))
                {
                    idList.Add(result.ID);
                }
            }

            return idList;
        }

        public IList<String> GetBrandList()
        {
            IList<String> brandList = new List<String>();
            foreach (IDBrandResult result in this.Items)
            {
                if(!brandList.Contains(result.Brand))
                {
                    brandList.Add(result.Brand);
                }
            }
            return brandList;
        }
      

       

         public IList<IDBrandResult> GetMatchedBrandIDList(List<String> refinedidList)
         {
             List<IDBrandResult> idbrandResultList = new List<IDBrandResult>();
             if (refinedidList != null)
             {
                 foreach (IDBrandResult result in this.Items)
                 {
                     if (refinedidList.Contains(result.ID))
                     {
                         idbrandResultList.Add(result);
                     }
                 }
             }
             return idbrandResultList;
         }
     }

    #endregion
 }
