using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BusinessObject
{/// <summary>
    /// Remote Business Entity
    /// </summary>
    [Serializable]
    public class TNHeader
    {
        #region Private Fields
        private int _tn;
        private int _parent_TN;
        private string _dbSearchLog = "";
        private string _image = "";
        private int _legacyMarketFlag;

        private string _legacyBrand = "";
        private string _box = "";
        private string _legacyDeviceType = "";
        private string _legacyRemoteModel = "";
        private string _legacyTargetModel = "";

        private string _sender_Name = "";
        private string _sender_Company = "";
        private string _sender_Address = "";
        private string _sender_Phone = "";
        private string _homer = "";

        private string _text = "";
        private string _status = StatusFlag.MODIFIED;
        private string _isLocked = BooleanFlag.NO;

        private LogCollection _logList = new LogCollection();
        
        #endregion

        #region Properties
        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public int Parent_TN
        {
            get { return _parent_TN; }
            set { _parent_TN = value; }
        }

        public string DBSearchLog
        {
            get { return _dbSearchLog; }
            set { _dbSearchLog = value; }
        }

        public string Image
        {
            get { return _image; }
            set { _image = value; }
        }

        public int LegacyMarketFlag
        {
            get { return _legacyMarketFlag; }
            set { _legacyMarketFlag = value; }
        }

        public string LegacyBrand
        {
            get { return _legacyBrand; }
            set { _legacyBrand = value; }
        }

        public string Box
        {
            get { return _box; }
            set { _box = value; }
        }

        public string LegacyDeviceType
        {
            get { return _legacyDeviceType; }
            set { _legacyDeviceType = value; }
        }

        public string LegacyRemoteModel
        {
            get { return _legacyRemoteModel; }
            set { _legacyRemoteModel = value; }
        }

        public string LegacyTargetModel
        {
            get { return _legacyTargetModel; }
            set { _legacyTargetModel = value; }
        }

        public string Sender_Name
        {
            get { return _sender_Name; }
            set { _sender_Name = value; }
        }

        public string Sender_Company
        {
            get { return _sender_Company; }
            set { _sender_Company = value; }
        }

        public string Sender_Address
        {
            get { return _sender_Address; }
            set { _sender_Address = value; }
        }

        public string Sender_Phone
        {
            get { return _sender_Phone; }
            set { _sender_Phone = value; }
        }

        public string Homer
        {
            get { return _homer; }
            set { _homer = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        [XmlIgnore]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        [XmlIgnore]
        public string IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }

        public LogCollection LogList
        {
            get { return _logList; }
            set { _logList = value; }
        }

        #endregion
    }
}
