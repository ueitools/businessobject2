using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessObject
{
    /// <summary>
    /// repliaction (synchronization tools)
    /// use dbName.dbo.tableName format (only for SQL server)
    /// </summary>
    public static class ReplicationFunctions
    {
        public static HashtableCollection SelectAll(string tableName, string dbName)
        {
            return DAOFactory.Misc().SelectAll(tableName, dbName);
        }

        public static HashtableCollection SelectAll(string tableName)
        {
            return DAOFactory.Misc().SelectAll(tableName);
        }

        public static void DeleteAll(string tableName, string dbName)
        {
            DAOFactory.Misc().DeleteAll(tableName, dbName);
        }

        public static void Transfer(string tableName,
            string destDBName, string srcDBName)
        {
            DAOFactory.Misc().Transfer(tableName, destDBName, srcDBName);
        }
    }
}
